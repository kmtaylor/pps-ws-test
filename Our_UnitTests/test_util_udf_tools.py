"""
    unit tests for http tools
"""
from unittest import TestCase
from unittest.mock import patch, Mock
import requests
from Utils.util_udfTools import udfTools

from Utils.http_request_tools import HttpTools

class TestUDFTools(TestCase):
    """
        unit tests for util_udfTools
    """

    def setUp(self):
        """setup"""
        self.my_udf = udfTools()
        self.test_lib = 'ABBBB'
        self.test_lib_limsid = '2-3002473'

    def test_get_library_url(self):
        """ example usage """
        uri = self.my_udf.get_first_clarity_url_by_artifact_name(self.test_lib)
        self.assertIn(self.test_lib_limsid, uri)
