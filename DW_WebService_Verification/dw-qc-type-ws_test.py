
# created 1/13/21

# the WS URLs are :
# https://ws-access-dev.jgi.doe.gov
# https://ws-access-int.jgi.doe.gov
# https://ws-access.jgi.doe.gov


#this test runs the qcType WS and verifies that the data return is as specified
# see  https://issues.jgi.doe.gov/browse/QATEST-998
# specification: https://docs.jgi.doe.gov/display/DW/Qc+Type%3A+API+Contract




# The tests included here are:
#
#     GET dw-pc-qc-type status=200; response contains json object representing an array of resource attributes
#     GET dw-pc-qc-type?organization-name=<organization-name> status=200; query parameter is ignored
#     Get dw-pc-qc-type with invalid/incorrect URL; status =404 “error”:“Resource Not Found”


import requests
import json

from Utils.open_yaml import open_yaml
from Utils.util_jsonInput import jsonInput
from Utils.http_request_tools import HttpTools


class  qcTypeWST():
    def __init__(self):
        self.printOn = True
        self.errorCnt = 0
        dict_config = open_yaml()
        self.wsURL = dict_config['ws_url'] + "dw-pc-qc-types-cv"
        self.myJson = jsonInput()
        self.myRequests = HttpTools()



    # -------------------------------------------------------------------------------------------
    # runWS
    #
    def runWS(self,parm="",parmvalue=""):
        wsURL = self.wsURL
        if parm:
            wsURL = self.wsURL + "/?" + parm +"=" + parmvalue

        if self.printOn:
            print("using WS url: ", wsURL)
        status,responseJson = self.myRequests.http_get_json_w_expectedStatus(wsURL,200)

        return responseJson

    # -------------------------------------------------------------------------------------------
    # checkForRequiredParameters
    #

    def checkForRequiredParameters(self,qcTypeRecord):
        verificationErrors = 0
        print(json.dumps(qcTypeRecord, indent=4, sort_keys=True))
        errorMsg = ""
        qcTypeId = qcTypeRecord["qc-type-id"]
        if not (isinstance(qcTypeId,int)):
            print(qcTypeId)
            print("data type= ", type(qcTypeId))
            verificationErrors +=1
            errorMsg += "qc-type-id  inconsistency,"

        qcType = qcTypeRecord["qc-type"]
        if not(qcType and  isinstance(qcType,str)):
            verificationErrors +=1
            print (qcType)
            print ("data type= ", type(qcType))
            errorMsg += "qc-type inconsistency,"

        active = qcTypeRecord["active"]
        if active:
            if not (isinstance(active, bool)):
                verificationErrors += 1
                errorMsg += "active inconsistency,"


        if verificationErrors > 0:
            print("***DB errors for qcType: ",qcType, ", " , qcTypeId, ", required parameter(s) inconsistency = ", verificationErrors, " errors")
        return verificationErrors,qcType,errorMsg



    #------------------------------------------------------------------------------------
    # getAll_qcTypes_and_verifyData
    # run the web service without parameters. It verifies the data in response to be
    # consistent with the contract. Prints a report including errors found
    # inputs: none
    # outputs: none
    def getAll_qcTypes_and_verifyData(self):
        qcTypeData = self.runWS()
        #print(qcTypeData)
        activeCount =0
        totalErrors = 0
        qcTypeDict={}
        #qcTypeRecord = qcTypeData[0]
        for record in qcTypeData:
            #print(json.dumps(record, indent=4, sort_keys=True))
            if record["active"] == True :
                activeCount +=1
            qcTypeErrors,qcType,errMsg = self.checkForRequiredParameters(record)
            totalErrors = qcTypeErrors + totalErrors
            if qcTypeErrors > 0:
                qcTypeDict[qcType] =errMsg  #save errors for later reporting
        print ("***** QC Type DB attribute verification Report ! *****")
        print("number of active QC Types = ", activeCount)
        print("number of total QC Type records = ",len(qcTypeData))

        #print report of this test
        if totalErrors >0 :
            print("total errors found with DB/contract inconsistency= ", totalErrors)
            print("****************************************************************")
            print("QC Type  :      ERRORS FOUND:")
            for qcType in qcTypeDict:
                print (qcType, ":", qcTypeDict[qcType])



  #------------------------------------------------------------------------------------
    # getAllqcTypes_with_parameter
    # run the web service with parameters.
    # inputs: none
    # outputs: none
    def get_qcTypes_with_query_parameter(self,parm,value):
        qcTypeData = self.runWS(parm,value)

        print("number of total QC Type records = ",len(qcTypeData))





#------this is how to run the tests--------------------------

myTest = qcTypeWST()
print ("----------------------------------------------------")
print ("TESTING: QC Type WS (without parameter) and verify data ")
myTest.getAll_qcTypes_and_verifyData()

print ("----------------------------------------------------")
print ("----Error testing---invalid query parameter -- expecting all records -------")
print ("TESTING: QC Type WS with query parameter active=false, expecting all records, query parameter not checked")
myTest.get_qcTypes_with_query_parameter("active","false")




print ("----------------------------------------------------")
print ("----Error testing---using id as parameter --expecting status = 404 -------")
print ("TESTING: QC Type WS with parameter id")
saveURL =  myTest.wsURL
myTest.wsURL = saveURL + "/231"
qcTypeData = myTest.runWS()
myTest.wsURL = saveURL
print(qcTypeData)

print ("----------------------------------------------------")

print ("----Error testing---incorrect URL -- expecting status 404 -------")
print ("TESTING: QC Type WS incorrect URL")
saveURL =  myTest.wsURL
myTest.wsURL = saveURL + "/JGI"
qcTypeData = myTest.runWS()
myTest.wsURL = saveURL
print(qcTypeData)

print ("----------------------------------------------------")
print("---end---")







