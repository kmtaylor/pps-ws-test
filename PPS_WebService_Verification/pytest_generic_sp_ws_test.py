# created 4/13/22

# URL: https://ws-access-int.jgi.doe.gov/pps-sequencing-projects/<xxxxxxx>
# these tests, create new sp without sample submission via data generator
# the edit test checks that the sample can be updated by a PUT to the PPS Clarity Generic SP service
# verify database and udf updates
# to see SP in WIP : https://projects-genint.jgi.doe.gov/pmo/sequencing_projects/xxxx

# requirements for the Clarity Generic SP service are here:
# https://docs.google.com/document/d/15ANSoeMku86UkllU5fizCseGIYHpc_Jg7wgexsuP48A/edit#heading=h.6rg9uf20u7t4

from assertpy import assert_that
import pytest
from datetime import datetime, timedelta

from Utils.clarity_constants import Container, ClarityQueues

ws_endpoint = "pps-sequencing-projects"


@pytest.fixture(scope="class")
def ws_url(ws_base_url):
    return ws_base_url + ws_endpoint


def test_can_delete_sp_when_it_is_awaiting_metadata(ws_url, data_gen, http_tools, pps_uss_db, udf_tools):
    # create a new SP for which sample metadata has not been submitted
    sp_ids = data_gen.create_sequencing_project(1, Container.TUBE, 1)
    sp_id = sp_ids[0]

    # get the info about the SP from the ws
    ws_sp_url = ws_url + '/' + str(sp_id)
    sp_json = http_tools.http_get_json(ws_sp_url)

    original_sp_name = sp_json['sequencing-project-name']
    original_status = sp_json['current-status']

    # must use today's date for status date, status dates that are before the current status are not allowed
    new_status_date = datetime.strftime(datetime.now(), '%Y-%m-%d')
    edits_to_sp_name = ' *** Deleted via pps-ws-test'
    new_sp_name = original_sp_name + edits_to_sp_name
    new_status = 'Deleted'

    submission_json = {
        "sequencing-project-name": new_sp_name,
        "current-status": new_status,
        "status-date": new_status_date,
        "last-updated-by": 19463
    }

    # verify that our new values are different from the original values, because we can't tell if things have
    # really been updated if we are not sending new values
    assert_that(original_status).is_not_equal_to(new_status)
    assert_that(original_sp_name).is_not_equal_to(new_sp_name)

    # edit the SP using the service we are testing
    ws_resp = http_tools.http_put_json_w_expectedStatus(ws_sp_url, submission_json, 200)

    # verify that the web service response shows the new values
    for key in submission_json.keys():
        # the web service doesn't return last-updated-by, so skip it
        if key == 'last-updated-by':
            continue
        assert_that(ws_resp[key]).is_equal_to(submission_json[key])

    # verify that the Clarity udfs have been updated
    # TODO - make below a loop through the submission_json, and check each value we sent,
    #      - requires some kind of mapping between submission_keys and udf names???
    #      -  and not all values submitted to webservice are recorded in udfs
    #      -  and we'll need a mapping between contact ids (which is what the ws accepts) and names,
    #      -  which is what is displayed by the udfs
    udf_dict = udf_tools.get_project_udfs_by_spid(sp_id)
    error_msg = 'Sequencing Project Name was not updated in Clarity'
    assert_that(udf_dict['Sequencing Project Name'], error_msg).is_equal_to(new_sp_name)
    
    # verify that the database entries have been updated
    db_query = f"SELECT sp.sequencing_project_name, to_char(sp.status_date, 'YYYY-MM-DD')as status_date, " \
               f"sp.aud_last_modified_by as last_updated_by, spcv.status as current_status " \
               f"FROM uss.dt_sequencing_project sp " \
               f"LEFT JOIN uss.DT_SEQ_PROJECT_STATUS_CV spcv ON spcv.STATUS_ID = sp.CURRENT_STATUS_ID " \
               f"where sequencing_project_id = ({sp_id})"
    db_list = pps_uss_db.do_query_get_rows_as_json(db_query)

    # TODO - add check for sample and sow status = 'Deleted' also this will need to be separate
    #      - sample/sow status is not returned by this ws

    # compare the info in the database to the info in the edit attributes json
    for db_info in db_list:
        for key in submission_json.keys():
            # the web service doesn't return last-updated-by, so skip it
            if key == 'last-updated-by':
                continue

            # the query is written so that the names returned match the keys in the
            # submission json except that they have underscores rather than hyphens
            # and the database columns are all upper case
            db_column = key.replace("-", "_")
            assert_that(db_info[db_column]).is_equal_to(submission_json[key])

# TODO - change more entities when doing the update, like comments, embargo days, etc.
def test_can_update_sp_when_sample_created(ws_url, data_gen, http_tools, pps_uss_db, udf_tools):
    # create a new SP and sample in Clarity
    artifacts = data_gen.create_samples(1, Container.TUBE, 1, ClarityQueues.APPROVE_SHIPPING)
    sp_ids = artifacts[0].spids
    sp_id = sp_ids[0]
   # sp_id = 1419659

    # get the info about the SP from the generic SP ws
    ws_sp_url = ws_url + '/' + str(sp_id)
    sp_json = http_tools.http_get_json(ws_sp_url)

    original_sp_name = sp_json['sequencing-project-name']
    original_auto_schedule = sp_json['auto-schedule-sow-items']

    new_sp_name = original_sp_name + " *** edited"
    new_sp_pi = 77969              # Sandaa, Ruth-Anne
    new_sample_contact_id = 11381  # Talag, Jayson
    new_sp_pm = 116                # Barry, Kerrie
    new_auto_schedule = 'N'
    if original_auto_schedule == 'false':
        new_auto_schedule = 'Y'

    # verify that our new values are different from our old values (the old values are set by defaults
    # in data generator, so they may change in the future, and our test is not effective if the values are the same
    error_message = 'test setup is invalid'
    assert_that(new_sp_name, error_message).is_not_equal_to(original_sp_name)

    submission_json = {
        "sequencing-project-name": new_sp_name,
        "project-contact-cid": new_sp_pi,
        "project-manager-cid": new_sp_pm,
        "auto-schedule-sow-items": new_auto_schedule,
        "last-updated-by": 19463
    }

    # edit the SP using the service we are testing
    ws_resp = http_tools.http_put_json_w_expectedStatus(ws_sp_url, submission_json, 200)

    # verify that the web service response shows the new values
    for key in submission_json.keys():
        # the web service doesn't return last-updated-by, so skip it
        if key == 'last-updated-by':
            continue
        assert_that(ws_resp[key]).is_equal_to(submission_json[key])

    # verify that the Clarity udfs have been updated
    udf_dict = udf_tools.get_project_udfs_by_spid(sp_id)

    error_msg = 'Sequencing Project Name was not updated in Clarity'
    assert_that(udf_dict['Sequencing Project Name'], error_msg).is_equal_to(new_sp_name)

    # error_msg = 'Sequencing Project PI Contact Id was not updated in Clarity'
    # assert_that(int(udf_dict['Sequencing Project PI Contact Id']), error_msg).is_equal_to(new_sp_pi)
    # error_msg = 'Sequencing Project PI name was not updated in Clarity'
    # assert_that(udf_dict['Sequencing Project PI'], error_msg).is_equal_to('Sandaa, Ruth-Anne')
    #
    # error_msg = 'Sample Contact Id was not updated in Clarity'
    # assert_that(int(udf_dict['Sample Contact Id']), error_msg).is_equal_to(new_sample_contact_id)
    # error_msg = 'Sample Contact name was not updated in Clarity'
    # assert_that(udf_dict['Sample Contact'], error_msg).is_equal_to('Talag, Jayson')
    #
    # error_msg = 'Sequencing Project Manager Id was not updated in Clarity'
    # assert_that(int(udf_dict['Sequencing Project Manager Id']), error_msg).is_equal_to(new_sp_pm)
    # error_msg = 'Sequencing Project Manager name was not updated in Clarity'
    # assert_that(udf_dict['Sequencing Project Manager'], error_msg).is_equal_to('Barry, Kerrie')

    error_msg = 'AutoSchedule Sow Items was not updated'
    if submission_json["auto-schedule-sow-items"] == 'Y':
        assert_that(udf_dict['AutoSchedule Sow Items'], error_msg).is_equal_to('true')
    else:
        assert_that(udf_dict['AutoSchedule Sow Items'], error_msg).is_equal_to('false')

    # verify that the database entries have been updated
    db_query = f"SELECT sequencing_project_name, sequencing_project_contact_id, " \
               f"sequencing_project_manager_id, " \
               f"auto_schedule_sow_items " \
               f"FROM uss.dt_sequencing_project " \
               f"where sequencing_project_id = {sp_id}"
    db_list = pps_uss_db.do_query_get_rows_as_json(db_query)
    db_info = db_list[0]   # this query returns only one row

    # compare the info in the database to the info in the edit attributes json
    assert_that(db_info['sequencing_project_name']).is_equal_to(submission_json['sequencing-project-name'])
    assert_that(db_info['sequencing_project_contact_id']).is_equal_to(submission_json['project-contact-cid'])
    assert_that(db_info['sequencing_project_manager_id']).is_equal_to(submission_json['project-manager-cid'])
    assert_that(db_info['auto_schedule_sow_items']).is_equal_to(submission_json['auto-schedule-sow-items'])
