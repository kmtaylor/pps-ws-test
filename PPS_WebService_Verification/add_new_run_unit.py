"""
test new run units ticket: pc-19
todo: better way to handle http response errors
todo: use init err_count
todo: use yaml for variables
"""
import json
import Utils.open_logger as ol
from Utils import data_gen_tools
from Utils.clarity_constants import Container
from Utils.http_request_tools import HttpTools
from Utils.util_dbTools import DataBaseTools
from Utils.util_dbTools_Postgres import DataBaseToolsPostgres
from Utils.util_jsonInput import jsonInput
from Utils.util_udfTools import udfTools
from Utils.open_yaml import open_yaml

class  AddRunUnit():
    """
    add run unit
    """
    def __init__(self):
        """
        Init
        """
        self.logger = ol.open_logger()
        self.logger.info('start add run unit tests')
        self.my_db = DataBaseToolsPostgres(db='pps')
        self.my_db.connect()
        self.error_cnt = 0

        dict_config = open_yaml()
        self.ws_url = dict_config['ws_url'] + dict_config['add_sow_url']
        self.clarity_server = dict_config['server_name']

        self.my_udf = udfTools()
        self.data_gen = data_gen_tools.DataGenTools()
        self.my_json = jsonInput()
        self.my_http = HttpTools()


    def add_new_run_units(self):
        """
        test new run units by adding them to a new sow
        process:
            create sample and sow(s) with the new run units
            verify the run units are correct by
                - convert run unit id of the sow item to run unit name
                - compare the two run unit names
        :return: 0 if test passes, error count if failure
        """

        # error count
        err_count = 0

        # create sample data
        artifacts = self.data_gen.create_samples(1, Container.TUBE, 1)
        sample_id = artifacts[0].sample_ids[0]
        sp_id = artifacts[0].spids[0]

        # set up json submission
        self.my_json.updateJson(self.my_json.addSowToNewSamJson, "sequencing-project-id", sp_id)
        self.my_json.updateJson(self.my_json.addSowToNewSamJson, "sample-id", sample_id)
        self.my_json.updateJson(self.my_json.addSowToNewSamJson, "created-by-cid", 13)

        # pc-19 - add new run unit to a sow and verify the sow contains the correct run unit id
        run_units = ['Gb_CCS', 'M reads_CCS', 'X_CCS']
        for unit in run_units:
            # add run unit to json submission
            self.my_json.updateJson(self.my_json.addSowToNewSamJson, "logical-amount-units", unit)

            # prepare json submission
            submission_json = self.my_json.addSowToNewSamJson
            mydata = json.dumps(submission_json)

            # post to add sow item
            status_code, response_json = self.my_http.http_post_json_w_status(self.ws_url, mydata)
            if status_code != 201:
                return status_code

            # get new sow item and its logical amt id and canonical name of the id
            sow_item_id = str(response_json[0]['sow-item-id'])
            logical_amt_id = self.my_db.get_database_attribute_list('logical_amount_units_id',
                                                                    'uss.dt_sow_item',
                                                                    'SOW_ITEM_ID',
                                                                    sow_item_id)
            log_unit_name = self.my_db.get_database_attribute_list('logical_amount_units',
                                                                   'uss.dt_logical_amount_units_cv',
                                                                   'LOGICAL_AMOUNT_UNITS_ID',
                                                                   logical_amt_id[0])
            # if logical unit name does not match submitted name, increment error count
            if unit != log_unit_name[0]:
                err_count += 1
                err_msg = ("ERR: expected run mode / actual run mode / sow item",
                           unit, log_unit_name[0], sow_item_id)
                self.logger.error(err_msg)

        return err_count


MY_RUN = AddRunUnit()
MY_RUN.add_new_run_units()
