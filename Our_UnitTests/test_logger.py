"""
    unit tests for logger
"""
from unittest import TestCase
import Utils.open_logger

class TestLoggerTools(TestCase):
    """
        unit tests for logger
    """
    def setUp(self):
        """setup"""

    def test_write_to_log_file(self):
        """ test error message goes to test log file """
        test_log_file = '/Users/KMTaylor/git2/pps-ws-test/test.log'
        log_file_entry = 'ooo nnoooo3'
        log_file_type = 'test'

        # these two lines are what you need to use logger
        # log_file_type should probably be 'dev' instead of 'test'
        # log_file_entry is your error message
        logger = Utils.open_logger.open_logger(log_file_type)
        logger.warning(log_file_entry)

        with open(test_log_file, "r") as log_file:
            for last in log_file:
                pass  # Read all lines, keep final value.

        self.assertRegex(last, log_file_entry)

    def test_write_to_log_file_dont_specify_logger(self):
        """ test error message goes to test log file """
        test_log_file = '/Users/KMTaylor/git2/pps-ws-test/test.log'
        log_file_entry = 'logger unit test message: ooo nnoooo4'

        # these two lines are what you need to use logger
        # log_file_type should probably be 'dev' instead of 'test'
        # log_file_entry is your error message
        logger = Utils.open_logger.open_logger()
        logger.warning(log_file_entry)

        with open(test_log_file, "r") as log_file:
            for last in log_file:
                pass  # Read all lines, keep final value.

        self.assertRegex(last, log_file_entry)
