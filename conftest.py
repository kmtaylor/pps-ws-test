import pytest
from _pytest.monkeypatch import MonkeyPatch

from Utils.Analyte import Analyte
from Utils.clarity_constants import Container, ClarityQueues
from Utils.open_yaml import open_yaml
from Utils import data_gen_tools
from Utils.http_request_tools import HttpTools
from Utils.sow_status import SowStatus
from Utils.util_dbTools import DataBaseTools
from Utils.util_dbTools_Postgres import DataBaseToolsPostgres
from Utils.util_udfTools import udfTools


@pytest.fixture(scope="module")
def ws_base_url():
    dict_config = open_yaml()
    return dict_config['ws_url']


@pytest.fixture(scope="module")
def user_id():
    dict_config = open_yaml()
    return dict_config['user_id']


@pytest.fixture(scope="module")
def data_gen():
    return data_gen_tools.DataGenTools()


@pytest.fixture(scope="module")
def http_tools():
    return HttpTools()


@pytest.fixture(scope="module")
def oracle_db():
    db_tools = DataBaseTools()
    db_tools.connect(db_tools.int_db)
    return db_tools


@pytest.fixture(scope="module")
def pps_uss_db():
    db_tools = DataBaseToolsPostgres(db='pps')
    db_tools.connect()
    return db_tools


@pytest.fixture(scope="module")
def udf_tools():
    return udfTools()


@pytest.fixture(scope="module")
def json_inputs():
    return json_inputs()


# added the monkeysession() code to fix scope error when trying to use monkey patch,
# per comments here: https://github.com/pytest-dev/pytest/issues/363
@pytest.fixture(scope="class")
def monkeysession():
    monkey_patch = MonkeyPatch()
    yield monkey_patch
    monkey_patch.undo()


@pytest.fixture(scope="class")
def mock_illumina_dop1_prus(monkeysession):
    def mock_return(*args, **kwargs):
        artifact_1 = Analyte("2-4846150", "HUNGY", [1439838], [296786], ['HUNGY'])
        artifact_2 = Analyte("2-4846201", "HUNGZ", [1439838], [296787], ['HUNGZ'])
        artifact_3 = Analyte("2-4846208", "HUNGU", [1439838], [296783], ['HUNGU'])
        artifact_4 = Analyte("2-4846212", "HUNGW", [1439838], [296784], ['HUNGW'])
        artifact_5 = Analyte("2-4846216", "HUNGX", [1439838], [296785], ['HUNGX'])
        analytes = [artifact_1, artifact_2, artifact_3, artifact_4, artifact_5]
        return analytes

    monkeysession.setattr(data_gen_tools.DataGenTools, "create_samples", mock_return)


# to test with pre-made prus (possibly made by hand when data gen won't work all the way through RQC)
# add mock_pool_pru to the illumina_pool_pru_artifact fixture, as shown below:
# def illumina_pool_pru_artifact(data_gen, mock_illumina_pool_pru):
# THIS FEATURE IS MEANT TO BE USED WHEN WORKING ON THE TEST, NOT WHEN ACTUALLY RUNNING IT!!!
@pytest.fixture(scope="class")
def mock_pool_pru(monkeysession):
    def mockreturn(*args, **kwargs):
        # the spids are used in pool_with_status_awaiting_qa_qc fixture, so they should be set correctly
        spids = [1456950, 1456950, 1456950, 1456950, 1456950, 1456950, 1456950, 1456950]
        sampleids = [339381, 339380, 339382, 339375, 339377, 339376, 339379, 339378]
        library_names = ['NYXNB', 'NYXNC', 'NYXHU', 'NYXHW', 'NYXHX', 'NYXHY', 'NYXHZ', 'NYXNA']
        analyte = Analyte("2-5670117", "lb NYXNG", spids, sampleids, library_names)
        analytes = [analyte]
        return analytes

    monkeysession.setattr(data_gen_tools.DataGenTools, "create_samples", mockreturn)


# use with mock_illumina_dop1_prus fixture when you want to mock the results from data generator
@pytest.fixture(scope="class")
def illumina_libraries_dop_1_pru_artifacts(data_gen):
    # create samples and run them through Sequencing in Clarity
    # we only need 6 for the test, but create 8 so that if something fails in data gen
    # then we have extras we can push through manually and put their data into
    # mock_illumina_dop1_prus, so we can mock to continue testing
    artifacts = data_gen.create_pru_illumina_dop1(8, Container.TUBE)
    return artifacts


@pytest.fixture(scope="class")
def illumina_libraries_with_status_awaiting_qa_qc(illumina_libraries_dop_1_pru_artifacts, pps_uss_db):
    # change the status of all sow items Awaiting QA/QC Analysis
    spids = Analyte.get_all_sp_ids(illumina_libraries_dop_1_pru_artifacts)
    # update sow status to Awaiting QA/QC
    pps_uss_db.update_sow_status_by_spid(spids, SowStatus.AWAITING_QA_QC.value)
    return illumina_libraries_dop_1_pru_artifacts


@pytest.fixture(scope="class")
def illumina_pool_pru_artifact(data_gen):
    # create the prus
    pool_artifact = data_gen.create_samples(8, Container.TUBE, 1, ClarityQueues.RQC)
    return pool_artifact


# TODO figure out what spid will be used to create Revio multiplexed pools
# TODO - better yet, quit relying on specific spids, and make use of the DGUI specify run mode option
@pytest.fixture(scope="class")
def revio_pool_pru_artifact(data_gen):
    # create the prus
    pool_artifact = data_gen.create_samples(8, Container.TUBE, 3, ClarityQueues.RQC)
    return pool_artifact


# As of 9/7/2023 on INT spid 42 creates artifacts with
# run mode = PacBio Sequel IIe 1 X 900
# library creation queue = PacBio Multiplexed 3kb, Tubes
# because the library creation queue is a multiplexed queue, these libraries are assigned
# indexes and therefore go through the pooling process
@pytest.fixture(scope="class")
def sequel_pool_pru_artifact(data_gen):
    # create the prus
    pool_artifact = data_gen.create_samples(8, Container.TUBE, 42, ClarityQueues.RQC)
    return pool_artifact


@pytest.fixture(scope="class")
def pool_with_status_awaiting_qa_qc(illumina_pool_pru_artifact, pps_uss_db):
    # change the status of all sow items Awaiting QA/QC Analysis
    spids = Analyte.get_all_sp_ids(illumina_pool_pru_artifact)
    # update sow status to Awaiting QA/QC
    pps_uss_db.update_sow_status_by_spid(spids, SowStatus.AWAITING_QA_QC.value)
    return illumina_pool_pru_artifact


@pytest.fixture(scope="class")
def sequel_pool_with_status_awaiting_qa_qc(sequel_pool_pru_artifact, pps_uss_db):
    # change the status of all sow items Awaiting QA/QC Analysis
    spids = Analyte.get_all_sp_ids(sequel_pool_pru_artifact)
    # update sow status to Awaiting QA/QC
    pps_uss_db.update_sow_status_by_spid(spids, SowStatus.AWAITING_QA_QC.value)
    return sequel_pool_pru_artifact


@pytest.fixture(scope="class")
def revio_pool_with_status_awaiting_qa_qc(revio_pool_pru_artifact, pps_uss_db):
    # change the status of all sow items Awaiting QA/QC Analysis
    spids = Analyte.get_all_sp_ids(revio_pool_pru_artifact)
    # update sow status to Awaiting QA/QC
    pps_uss_db.update_sow_status_by_spid(spids, SowStatus.AWAITING_QA_QC.value)
    return revio_pool_pru_artifact


@pytest.fixture()
def edit_sows_json(user_id):
    return {  # ok
            "submitted-by": user_id,
            "sow-item-ids": [],
            }
