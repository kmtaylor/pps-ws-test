# created 12/13/21

# to see sows in WIP : https://projects-genint.jgi.doe.gov/pmo/sequencing_projects/xxxx
# this test, creates new sp and sample, runs it through aliquot creation, edits the sow created,
# verify Database updates and correct UDFs

# requirements for the edit sow service are here:
# https://docs.google.com/document/d/1oF6VsLEyOJjIoQwj_6LeLQeawdTh5sGlbFkMds-n3Q0/edit#


from assertpy import assert_that
import pytest

from Utils.clarity_constants import ClarityQueues, Container

printOn = True
ws_endpoint = "pps-sow-items-edit"

# at the time of test writing, samples created with SPID = 1 have attributes:
# dop = 384, TLA = 200, units = X, run-mode = Illumina NovaSeq S4 2 X 150
edit_attributes = {
    "degree-of-pooling": 1,
    "target-logical-amount": 80,
    "logical-amount-units": "Gb",
    "run-mode": "Illumina MiSeq 2 X 150",
    "sm-comments": "please create 10ug aliquots",
    "lc-comments": "please create 270kb library not 250kb",
    "sq-comments": "please run on a pink flowcell"
}


@pytest.fixture(scope="class")
def ws_url(ws_base_url):
    return ws_base_url + ws_endpoint


def test_can_edit_sow_of_sample(ws_url, data_gen, http_tools, pps_uss_db, udf_tools, edit_sows_json):
    # create a new sow item associated with a sample in the Approve for Shipping queue
    analytes = data_gen.create_samples(1, Container.TUBE, 1, ClarityQueues.APPROVE_SHIPPING)
    sp_ids = analytes[0].spids
    sp_id = sp_ids[0]
    sample_ids = analytes[0].sample_ids

    # get sow items (List) of newly created SP
    sow_items = get_sows_via_db_query(sp_id, pps_uss_db)
    print("New SP: ", sp_id, " ,New Sample(s): ", sample_ids, "New Sows: ", sow_items)

    # edit sow of sample by calling WS,
    json = edit_sows_json
    url = ws_url
    edit_sows(sow_items, edit_attributes, http_tools, json, url)

    # convert python list of sow item ids into a comma separated string
    sow_id_string = ','.join(map(str, sow_items))

    # verify that database has been updated
    db_query = f"select sow.SOW_ITEM_ID, sow.QC_TYPE_ID, " \
               f"dopcv.DEGREE_OF_POOLING, runcv.RUN_MODE, sow.TARGET_LOGICAL_AMOUNT, " \
               f"laucv.LOGICAL_AMOUNT_UNITS, sow.TOTAL_LOGICAL_AMOUNT_COMPLETED, " \
               f"sow.SM_INSTRUCTIONS as sm_comments, " \
               f"sow.LC_INSTRUCTIONS as lc_comments, " \
               f"sow.SQ_INSTRUCTIONS as sq_comments " \
               f"from uss.dt_sow_item sow " \
               f"left join USS.DT_RUN_MODE_CV runcv on sow.RUN_MODE_ID = runcv.RUN_MODE_ID " \
               f"left join USS.DT_DEGREE_OF_POOLING_CV dopcv on " \
               f"dopcv.DEGREE_OF_POOLING_ID = sow.DEGREE_OF_POOLING_ID " \
               f"left join USS.dt_logical_amount_units_cv laucv on " \
               f"laucv.logical_amount_units_id = sow.logical_amount_units_id " \
               f"where sow.sow_item_id in ({sow_id_string})"
    db_list = pps_uss_db.do_query_get_rows_as_json(db_query)

    # compare the sow info in the database to the info in the edit attributes json
    for db_info in db_list:
        for key in edit_attributes.keys():
            # the query is written so that the names returned match the keys in the
            # attributes json except that they have underscores rather than hyphens
            # and the database columns are all upper case
            db_column = key.replace("-", "_")
            assert_that(db_info[db_column]).is_equal_to(edit_attributes[key])

    # TODO verify that the udfs are correct
    # TODO - add code to remove the entities from the Clarity queues

    # commenting out the delete code below for now because there is no code yet that removes
    # the artifacts that have been created from the Clarity queues.  The delete code below
    # changes the status of the SPs and samples to Deleted, so then the entities that are left
    # in the queues cause PPV errors if you try to run the process on them - YUCK!
    # myDB.deleteSPandChildren(sp_id)


# editSows  - updates the json for submission with the correct values that are being edited
# input:  sow_items  - list of sows to be edited
#  attribute_dict - a dictionary containing the attributes (with new values) to be edited for sow item(s)
#  i.e  (only attributes that are to be updated should be included in dictionary
# {
#         "submitted-by": 5981,
#         "sow-item-purpose": "Anticipated Planned Work",
#          "degree-of-pooling" : 4,
#          "target-logical-amount": 80,
#          "logical-amount-units": "Gb",
#          "run-mode": "Illumina NextSeq-HO 2 X 150",
#          "exome-capture-probe-set": "",
#          "itag-primer-set": "",
#          "sequencing-optimization-factor": 0,
#         }

def edit_sows(sow_items, attribute_dict, http_tools, json, url):
    # update json to be used for post
    json["sow-item-ids"] = sow_items
    for attr, value in attribute_dict.items():
        json[attr] = value
    if printOn:
        print(json)

    # post request
    status, ws_resp = http_tools.http_post_json_w_expectedStatus(url, json, 200)

    # verify that the response reports success
    assert_that(status, description='Post to edit sow item service did not succeed').is_equal_to(200)
    print("WS succeeded! sows edited: ", sow_items, " attributes updated,  = ", edit_attributes)


# ------------------------------------------------------------------------------------
# get_sows_via_db_query
#
# get sows associated with SP via DB query
# inputs: none
# outputs: none


def get_sows_via_db_query(spid, db):
    # verify database has been updated with sow item
    sow_list = db.get_database_attribute_list('*', 'uss.dt_sow_item',
                                              'SEQUENCING_PROJECT_ID', str(spid))
    # convert list from string to list of numbers
    for i in range(0, len(sow_list)):
        sow_list[i] = int(sow_list[i])

    return sow_list
