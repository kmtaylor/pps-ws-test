"""
   Tools to aid with gets and posts of urls
"""
import json
import requests
import sys
from assertpy import assert_that

class HttpTools():
    """
       Tools to aid with gets and posts of urls
    """


    def http_get_json(self, request_url):
        """
            execute a get request on the supplied URL
            :param request_url: url to do a get on
            :return:  an empty string if request fails
               return: if json returned , return the json
            :exception: if get fails return
        """

        # preset return json to empty string
        json_value = ""

        # do a get on url ; throw exception if fails (and return)
        try:
            url_response = requests.get(request_url, timeout=15)
        except ValueError as error:
            print("http get request failed", error.args)
            return ""

        # if json is in the response, return the json
        # can also do this w. exception handling
        if url_response.status_code == 200:
            if 'json' in url_response.headers['Content-Type']:
                json_value = url_response.json()

        return json_value

    # ----------------------------------------------------------------------------------------------
    #  GET
    # returns json string if request status= expected status, returns status and message if not
    #
    def http_get_json_w_expectedStatus(self, requestURL, expectedStatus):
        jsonstring = {}
        try:
            r = requests.get(requestURL, timeout=36000)  # timeout is 10 minutes
        except:
            # status = r.status_code
            print("*** Error ! Opps! Can't get the request, Time out")
            print("Unexpected error:", sys.exc_info()[0])
            return {}
        status = r.status_code
        if status != expectedStatus:
            print("*** Error, Unexpected Returned Status = ", status, " expected status = ", expectedStatus)

            prettyJson = json.loads(r.text)
            #print(json.dumps(prettyJson, indent=4, sort_keys=True))
            #message = prettyJson["errors"][0]["message"]
            return status, prettyJson
        else:
            # Print Response
            prettyJson = json.loads(r.text)
            #print (json.dumps(prettyJson, indent=4, sort_keys=True))
            return status, prettyJson






    # ----------------------------------------------------------------------------------------------
    # returns json string if request status= 200, returns {} if request is bad or times out
    #
    def http_post_json(self, request_url, json_submission):
        """

        :param request_url: url to post to
        :param json_submission: json to submit in post
        :return: json returned by call
        :exception: if url post fails
        """

        # set up header content type
        content_type = {"Content-Type": "application/json"}

        # post url; throw exception if fails
        try:
            url_response = requests.post(request_url,
                                         data=json_submission,
                                         headers=content_type)
        except ValueError as error:
            print("http post request failed", error.args)
            return ""

        # clean up json and return
        return json.loads(url_response.text)

    def http_post_json_w_status(self, request_url, json_submission):
        """
        get json and return code
        :param request_url: url to post to
        :param json_submission: json to post
        :return: status code and return json
        :exception: if call to post fails exception is thrown
        """
        # set up content type in header
        content_type = {"Content-Type": "application/json"}

        # post with json to url; throw exception if all goes wrong
        try:
            url_response = requests.post(request_url,
                                         data=json_submission,
                                         headers=content_type)
        except ValueError as error:
            print("http post request failed", error.args)
            return ""

        # clean up json and return status code and json
        # check to see if response text is empty, because rqc-rework-action returns
        # an empty string response body for successful actions, and json.loads() throws
        # an exception for an empty string
        response_text = ""
        if url_response.text != "":
            response_text = json.loads(url_response.text)
        return (url_response.status_code, response_text)


    def http_post_json_w_expectedStatus(self, requestURL, requestJson, expectedStatus):
        mydata = json.dumps(requestJson)
        headers = {"Content-Type": "application/json"}
        # Call REST API
        response = requests.post(requestURL, data=mydata, headers=headers)  #POST  request
        status = response.status_code
        if status != expectedStatus:
            print("*** Error, Unexpected Returned Status = ", status)

            prettyJson = json.loads(response.text)
            if "errors" in prettyJson:
                message = prettyJson["errors"][0]["message"]
            else:
                message = response.text
            return status, message
        else:
            # check to see if response text is empty, because rqc-rework-action returns
            # an empty string response body for successful actions, and json.loads() throws
            # an exception for an empty string
            prettyJson = ""
            if response.text != "":
                prettyJson = json.loads(response.text)

            #print (json.dumps(prettyJson, indent=4, sort_keys=True))
            return status,prettyJson

    def http_put_json_w_expectedStatus(self, requestURL, requestJson, expectedStatus):
        mydata = json.dumps(requestJson)
        headers = {"Content-Type": "application/json"}
        # Call REST API
        response = requests.put(requestURL, data=mydata, headers=headers)  # PUT request
        status = response.status_code

        assert_that(status, response.text).is_equal_to(expectedStatus)

        # check to see if response text is empty, because json.loads() throws
        # an exception for an empty string
        prettyJson = ""
        if response.text != "":
            prettyJson = json.loads(response.text)

        #print (json.dumps(prettyJson, indent=4, sort_keys=True))
        return prettyJson