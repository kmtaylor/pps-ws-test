
# created 1/13/21

# the WS URLs are :
# https://ws-access-dev.jgi.doe.gov
# https://ws-access-int.jgi.doe.gov
# https://ws-access.jgi.doe.gov

# to see new sow in WIP : https://projects-genint.jgi.doe.gov/pmo/sequencing_projects/xxxx
#this test, creates new sp and sample, runs it through aliquot creation, adds a sow to the  existing sample,
# new sample and library, verify  Database
# and  UDF

import requests

from Utils.clarity_constants import ClarityQueues, Container
from Utils import data_gen_tools
import json
from Utils.util_dbTools import DataBaseTools
from Utils.util_dbTools_Postgres import DataBaseToolsPostgres
from Utils.util_udfTools import udfTools

from Utils.util_jsonInput import jsonInput
from Utils.Analyte import Analyte
from Utils.http_request_tools import HttpTools
from Utils.open_yaml import open_yaml

#name:  addSowWST  -  add sow Web Service Test,
# per requirements doc: https://docs.google.com/document/d/1DRIKtA7KZGBGONjaJKWe_jT8pp7LAaHI7uc8c215IHU/edit

class  addSowWST():
    def __init__(self):
        self.printOn = False
        self.myDB = DataBaseToolsPostgres(db='pps')
        self.myDB.connect()
        self.errorCnt = 0

        dict_config = open_yaml()
        self.wsURL = dict_config['ws_url'] + dict_config['add_sow_url']
        self.clarityServer = dict_config['server_name']

        self.myUDF = udfTools()
        self.dataGen = data_gen_tools.DataGenTools()
        self.myJson = jsonInput()
        self.myRequests = HttpTools()



    # -------------------------------------------------------------------------------------------
    # addSowToExistingSample
    # given an spid and a sample id, this will add a sow to that existing sample
    def addSowToExistingSample(self,spId,sampleID):
        if self.printOn:
            print("using WS url: ", self.wsURL)
        self.myJson.updateJson(self.myJson.addSowToExistSamJson, "sequencing-project-id",spId)
        self.myJson.updateJson(self.myJson.addSowToExistSamJson, "sample-id", sampleID)
        submissionJson = self.myJson.addSowToExistSamJson
        status, wsResp = self.myRequests.http_post_json_w_expectedStatus(self.wsURL, submissionJson, 201)
        if status == 201:
            sowItemId = wsResp[0]["sow-item-id"]
            return sowItemId
        return None   #if post did not work return null

    # -------------------------------------------------------------------------------------------
    # addSowToNewSample
    # given an spid and a sample id, this will add a sow to that new sample
    # inputs: spid
    # outputs: newSow, newSample

    def addSowToNewSample(self, spId):
        if self.printOn:
            print("using WS url: ", self.wsURL)
        self.myJson.updateJson(self.myJson.addSowToNewSamJson, "sequencing-project-id", spId)
        submissionJson = self.myJson.addSowToNewSamJson
        status, wsResp = self.myRequests.http_post_json_w_expectedStatus(self.wsURL, submissionJson, 201)
        if status == 201:
            sowItemId = wsResp[0]["sow-item-id"]
            sampleId =  wsResp[0]["sample-id"]
            return sowItemId,sampleId
        return None,None     #if post did not work return null


    # -------------------------------------------------------------------------------------------
    # addSowToLibrary
    # given an spid and library lims id, this will add a sow to that library

    def addSowToLibrary(self, spId, libraryLims):
        if self.printOn:
            print("using WS url: ", self.wsURL)
        self.myJson.updateJson(self.myJson.addSowToLibJson, "sequencing-project-id", spId)
        self.myJson.updateJson(self.myJson.addSowToLibJson, "library-stock-id", libraryLims)
        submissionJson = self.myJson.addSowToLibJson
        sowItemId = ""
        sowAdded=False
        while sowAdded == False:
            status, wsResp = self.myRequests.http_post_json_w_expectedStatus(self.wsURL, submissionJson, 201)
            if status == 201:
                sowItemId = wsResp[0]["sow-item-id"]
                sowAdded=True
            else:
                if status ==422:
                    if wsResp.find ("invalid status") != -1:
                        # get sow item and change status to abandon, then rerun
                        lbracket = wsResp.find('[')
                        rbracket = wsResp.find(']')
                        sowItemId = wsResp[lbracket+1:rbracket]
                        print("The sow status for adding sow to Library is unacceptable, must change to abandoned. "
                              "Updating sowitem id = ", sowItemId,  " status to abandoned")
                        if sowItemId:
                            # change status of sow item
                            statement = "update uss.dt_sow_item set current_status_id =7, " \
                                        "status_comments = 'updated by add_sow_ws_test.py' ," \
                                        "status_date = CURRENT_TIMESTAMP where sow_item_id in (" + sowItemId + ")"
                            self.myDB.do_update(statement)
                            # post again
                            sowAdded =False
                    else:
                        print ("something wrong!  add sow WS post an error")
                        exit()
        return sowItemId

    #------------------------------------------------------------------------------------
    # verifySowUDFinScheduledSample
    # inputs: sampleId, server,
    # outputs: UDFs as a dict  (will return the latest sample record)
    def verifySowUDFinScheduledSample(self,sampleId,newSowItem):
        sampleUDFs = {}
        url = self.myUDF.setURLsample(sampleId,self.clarityServer)
        print("sample base URL = ", url)
        if url:
            sampleUrl = self.myUDF.findLatestClaritySampleUrlBySampleName(url) #gets latest record created of created sample
            print("scheduled sample  URL = ", sampleUrl)
            if sampleUrl:
                   sampleUDFs = self.myUDF.getSampleUDFs(sampleUrl)


        apiSowRetrieved = sampleUDFs["SOW Item ID"]
        print("SOW Item ID UDF = ", apiSowRetrieved)
        if str(newSowItem) in apiSowRetrieved:
            print("Success! verified sow UDF found in Clarity scheduled sample")
        else:
            print("Error! something wrong. Sow not found in Clarity API!")

    # ------------------------------------------------------------------------------------
    # verifyDB_sowAssociatedWithSP
    # create new SP and sample and library using data generator
    # then add sow to library
    # must change status of sow to abandon to add to library
    # inputs: none
    # outputs: none

    def verifyDB_sowAssociatedWithSP(self, spid, newSowItem):
        # verify database has been updated with sow item
        sowList = self.myDB.get_database_attribute_list('*', 'uss.dt_sow_item',
                                                        'SEQUENCING_PROJECT_ID', str(spid))
        # verify database entry ------
        if str(newSowItem) in str(sowList):
            print("Success! verified database entry of sow item")
        else:
            print("Error! something wrong. Sow not found in DB!")

    #------------------------------------------------------------------------------------
    # createSample_and_addSowToLibrary
    # create new SP and sample and library using data generator
    # then add sow to library
    # must change status of sow to abandon to add to library
    # inputs: none
    # outputs: none
    def createSample_and_addSowToLibrary(self):
        # create new SP and sample and library to work with
        artifacts = self.dataGen.create_illumina_libraries(1, Container.TUBE, 1)
        libraryName = artifacts[0].name
        libraryLims = artifacts[0].lims_id
        spid = artifacts[0].spids[0]
        print("New SP: ", spid, "New Library :", libraryName, " (", libraryLims, ")")

        # add new sow to library by calling WS
        newSowItem = self.addSowToLibrary(spid, libraryLims)

        print("new sow added: ", newSowItem, " to library  ", libraryName)

        # verify database has been updated with sow item
        self.verifyDB_sowAssociatedWithSP(spid, newSowItem)

        # delete SP and children
        #self.myDB.deleteSPandChildren(spid)

    #------------------------------------------------------------------------------------
    # createSample_and_addSowToExistingSample
    # create new SP and sample using data generator
    # then add sow to sample using ws
    # verify that the sow can be found in db
    # verify that a scheduled sample record was created that has new sow UDF
    # inputs: none
    # outputs: none
    def createSample_and_addSowToExistingSample(self):
        # create new SP and sample to work with using datagenerator
        artifacts = self.dataGen.create_samples(1, Container.TUBE, 1, ClarityQueues.ALIQUOT_CREATION)
        spid = artifacts[0].spids[0]
        sample_id = artifacts[0].sample_ids[0]
        sample_id_list = Analyte.get_all_sample_ids(artifacts)
        # add new sow to sample by calling WS
        newSowItem = self.addSowToExistingSample(spid, sample_id)
        print("new sow added: ", newSowItem, " to sample ", sample_id)

        # verify database has been updated with sow item
        self.verifyDB_sowAssociatedWithSP(spid, newSowItem)

        # verify UDFs updates/creations
        self.verifySowUDFinScheduledSample(sample_id,newSowItem)

        # delete SP and children
       # self.myDB.deleteSPandChildren(spid)

  #------------------------------------------------------------------------------------
    # createSample_and_addSowToNewSample
    # create new SP and sample using data generator
    # then add sow to a new sample associated with SP using ws
    # verify that the sow can be found in db
    # verify that a scheduled sample record was created that has new sow UDF
    # inputs: none
    # outputs: none
    def createSample_and_addSowToNewSample(self):
        # create new SP and sample to work with using datagenerator
        artifacts = self.dataGen.create_samples(1, Container.TUBE, 1, ClarityQueues.APPROVE_SHIPPING)
        spid = artifacts[0].spids[0]
        sample_id = artifacts[0].sample_ids[0]
        sample_id_list = Analyte.get_all_sample_ids(artifacts)
        # add new sow to sample by calling WS
        newSowItem,newSample = self.addSowToNewSample(spid)
        print("new sow added: ", newSowItem, " to new sample ", newSample)

        # verify database has been updated with sow item
        self.verifyDB_sowAssociatedWithSP(spid, newSowItem)

        # delete SP and children
        # this changes SP and Sows to deleted, but leaves samples in the queues, which is annoing
        # self.myDB.deleteSPandChildren(spid)


#------this is how to run the tests--------------------------

myTest = addSowWST()

print ("----------------------------------------------------")
print ("End to end testing add sow WS - add sow to existing sample ")
myTest.createSample_and_addSowToExistingSample()

print ("----------------------------------------------------")
print ("End to end testing add sow WS - add sow to library ")
myTest.createSample_and_addSowToLibrary()

print ("----------------------------------------------------")
print ("End to end testing add sow WS - add sow to new sample")
myTest.createSample_and_addSowToNewSample()

#---- tests to do --------------------

# myTest.createSample_and_addSowToPlate()
#
# myTest.createSample_and_addSowToSingleCellSample()











