# created 7/27/22

import pytest

from PPS_WebService_Verification.pytest_rqc_super_class import TestRqcSuper, RqcActions

class TestRqcReworkIlluminaDop1(TestRqcSuper):
    """
    tests for RQC Rework functionality for Illumina libraries with DOP =  1
    """

# TODO - test this again.  Last time the data generator was failing in Sequencing - PERCENT-121
    # I ran the data generator again with 8 inputs, one got stuck, so there are 7 in sequencing
    # I could use 6 of these in mock_illumina_dop1_prus and see if the tests themselves work the
    # or I could wait till PERCENT-121 is fixed and retry without mocks
    # the libraries to use for mocks are:
    # HUWAG
    # HUWAN
    # HUWAO
    # HUWAP
    # HUWAS
    # HUWAT
    # HUWAU

    @pytest.fixture
    def allowed_lab_actions(self):
        return (RqcActions.MAKE_NEW_LIBRARY.value,
                RqcActions.QPCR_EXISTING_LIBRARY.value,
                RqcActions.SEQUENCE_EXISTING_LIBRARY.value)

    # For libraries dop = 1 individual prus are created for each library, so each test uses
    # a different library like illumina_libraries_with_status_awaiting_qa_qc[<index>] with the
    # first library name from the list of library names
    def test_inquiry_response(self, illumina_libraries_with_status_awaiting_qa_qc, allowed_lab_actions):
        pru_lims_id = illumina_libraries_with_status_awaiting_qa_qc[0].lims_id
        library_name = illumina_libraries_with_status_awaiting_qa_qc[0].library_names[0]
        self.check_inquiry_response(pru_lims_id, library_name, allowed_lab_actions)

    def test_no_action_complete(self, pps_uss_db, illumina_libraries_with_status_awaiting_qa_qc, allowed_lab_actions):
        pru_lims_id = illumina_libraries_with_status_awaiting_qa_qc[1].lims_id
        library_name = illumina_libraries_with_status_awaiting_qa_qc[1].library_names[0]
        self.check_no_action_complete(pps_uss_db, pru_lims_id, library_name)

    def test_no_action_needs_attention_and_abandon(self, pps_uss_db, illumina_libraries_with_status_awaiting_qa_qc):
        pru_lims_id = illumina_libraries_with_status_awaiting_qa_qc[2].lims_id
        library_name = illumina_libraries_with_status_awaiting_qa_qc[2].library_names[0]
        self.check_no_action_needs_attention_and_abandon(pps_uss_db, pru_lims_id, library_name)

    def test_make_new_library(self, illumina_libraries_with_status_awaiting_qa_qc):
        pru_lims_id = illumina_libraries_with_status_awaiting_qa_qc[3].lims_id
        library_name = illumina_libraries_with_status_awaiting_qa_qc[3].library_names[0]
        self.check_make_new_library(pru_lims_id, library_name)

    def test_qpcr_existing_library(self, illumina_libraries_with_status_awaiting_qa_qc):
        pru_lims_id = illumina_libraries_with_status_awaiting_qa_qc[4].lims_id
        library_name = illumina_libraries_with_status_awaiting_qa_qc[4].library_names[0]
        self.check_qpcr_existing_library(pru_lims_id, library_name)

# Todo - make check_sequence_existing_library function in super class
    def test_sequence_existing_library(self, illumina_libraries_with_status_awaiting_qa_qc):
        pru_lims_id = illumina_libraries_with_status_awaiting_qa_qc[5].lims_id
        library_name = illumina_libraries_with_status_awaiting_qa_qc[5].library_names[0]
        self.check_sequence_existing_library(pru_lims_id, library_name)