# created 11/10/21

# the WS URLs are :
# https://ws-access-dev.jgi.doe.gov
# https://ws-access-int.jgi.doe.gov
# https://ws-access.jgi.doe.gov


# this is a super class for the "simple" DW web services
# these services are for the CV tables used by PMOS and PPS and also some
# simplified versions of the more complex services, like for FD, SP, etc -
# these simplified versions just return ids, names, and active values
# https://docs.jgi.doe.gov/display/DW/Controlled+Vocabulary+Web+Services


import json

from Utils.open_yaml import open_yaml
from Utils.util_jsonInput import jsonInput
from Utils.http_request_tools import HttpTools
from Utils.util_dbTools_Postgres import DataBaseToolsPostgres
from Utils.wsTools import wsTools


class SimpleWsSuperClass:
    def __init__(self):
        self.printOn = True
        self.errorCnt = 0
        dict_config = open_yaml()
        self.wsUrlbase = dict_config['ws_url']
        self.myJson = jsonInput()
        self.myRequests = HttpTools()
        self.myDB = DataBaseToolsPostgres()
        self.myDB.connect()
        self.myWsTools = wsTools()
        self.errorMsgs = []

        # child classes need to initialize these values
        self.wsUrl = ''
        self.id_key = ''
        self.name_key = ''
        self.db_id_field = ''
        self.db_fields = ''
        self.db_table = ''

    # -------------------------------------------------------------------------------------------
    # check_for_required_parameters
    #
    def check_for_required_parameters(self, record):
        # check the base parameters
        self.myWsTools.checkKey(self.errorMsgs, record, self.id_key, True, int)
        self.myWsTools.checkKey(self.errorMsgs, record, self.name_key, True, str)
        self.myWsTools.checkKey(self.errorMsgs, record, 'active', True, bool)
        self.myWsTools.checkKey(self.errorMsgs, record, 'aud-date-created', True, str, null_ok=True)
        self.myWsTools.checkKey(self.errorMsgs, record, 'aud-last-modified', True, str)

    # ------------------------------------------------------------------------------------
    # get_records_with_parameter(self,parm):
    # run the web service with parameters.
    # expects one records returned in response by WS
    # inputs: parameter  (ie.  lc spec id)
    # outputs: prints error report
    def get_records_with_parameter_base(self, parm):
        record = self.myWsTools.runWS(self.wsUrl, parm)

        # get the info sequencing product info from the db for this id
        # the db fields are the same as the json tags but with underscores instead of dashes, so do a str replace
        # to get the db key from the json id key
        db_info = self.myDB.get_columns_from_single_row_single_table(self.db_fields,
                                                                     self.db_table,
                                                                     self.db_id_field,
                                                                     str(parm))

        # create a list of the items in the db fields string
        db_field_list = self.db_fields.replace(" ", "").split(",")

        # compare values from the ws with values from the db
        for field in db_field_list:
            json_key = field.replace("_", "-")
            if 'final-deliv' in json_key:
                json_key = json_key.replace('final-deliv', 'final-deliverable')
            self.myWsTools.compare_ws_to_db(self.errorMsgs, self.id_key, record, json_key,
                                            db_info, field)

    def test_active_records(self):
        self.myWsTools.test_active_records(self.errorMsgs, self.wsURL)
