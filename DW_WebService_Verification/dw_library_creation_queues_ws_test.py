
# created 10/13/21

# the WS URLs are :
# https://ws-access-dev.jgi.doe.gov
# https://ws-access-int.jgi.doe.gov
# https://ws-access.jgi.doe.gov


# this test runs the libraryCreationQueues WS and verifies that the data return is as specified
# see
# specification:

# The tests included here are:
#
#     GET dw-pc-library-creation-queues status=200; response contains an array of json objects of resource attributes
#     GET dw-pc-library-creation-queues/id# status=200; response contains json object of resource attributes
#     GET dw-pc-library-creation-queues?active=true status=200; response contains only the active queues
#
#     Get dw-pc-library-creation-specs with invalid/incorrect URL; status =404 “error”:“Resource Not Found”
#     POST,PUT,DELETE status=405

from Utils.open_yaml import open_yaml
from Utils.util_dbTools_Postgres import DataBaseToolsPostgres
from Utils.util_jsonInput import jsonInput
from Utils.http_request_tools import HttpTools
from Utils.wsTools import wsTools


class LibraryCreationQueuesWST:
    def __init__(self):
        self.printOn = True
        self.errorCnt = 0
        dict_config = open_yaml()
        self.wsURL = dict_config['ws_url'] + "dw-pc-library-creation-queues"
        self.myJson = jsonInput()
        self.myRequests = HttpTools()
        self.myDB = DataBaseToolsPostgres()
        self.myDB.connect()
        self.myWsTools = wsTools()
        self.errorMsgs = []

    # -------------------------------------------------------------------------------------------
    # check_db_field_for_active
    # returns True if the record is active

    def check_db_field_for_active(self, table_name, field_name, field_value):
        active_value = self.myDB.get_database_attribute("active", table_name, field_name, str(field_value))
        if active_value == "Y":
            return True
        else:
            return False

    # -------------------------------------------------------------------------------------------
    # check_for_required_parameters
    #
    def check_for_required_parameters(self, record):

        self.myWsTools.checkKey(self.errorMsgs, record, 'lc-queue-id', True, int)
        self.myWsTools.checkKey(self.errorMsgs, record, 'lc-queue', True, str)
        self.myWsTools.checkKey(self.errorMsgs, record, 'active', True, bool)
        self.myWsTools.checkKey(self.errorMsgs, record, 'plate-target-mass-lib-trial-ng', True, float, True)
        self.myWsTools.checkKey(self.errorMsgs, record, 'plate-target-vol-lib-trial-ul', True, float, True)
        self.myWsTools.checkKey(self.errorMsgs, record, 'tube-target-mass-lib-trial-ng', True, float, True)
        self.myWsTools.checkKey(self.errorMsgs, record, 'tube-target-vol-lib-trial-ul', True, float, True)
        self.myWsTools.checkKey(self.errorMsgs, record, 'aud-last-modified', True, str)
        self.myWsTools.checkKey(self.errorMsgs, record, 'aud-last-modified-by', True, str)

    # ------------------------------------------------------------------------------------
    # getAllLibraryCreationSpecs_and_verifyData
    # run the web service without parameters. It verifies the data in response to be
    # consistent with the contract. Prints a report including errors found
    # inputs: none
    # outputs: none
    def get_all_records_and_verify_schema(self):
        response_data = self.myWsTools.runWS(self.wsURL)

        for record in response_data:
            # print(json.dumps(record, indent=4, sort_keys=True))
            self.check_for_required_parameters(record)


    # ------------------------------------------------------------------------------------
    # get_records_with_parameter(self,parm):
    # run the web service with parameters.
    # expects one records returned in response by WS
    # inputs: parameter  (ie.  lc spec id)
    # outputs: prints error report
    def get_records_with_parameter(self, parm):
        record = self.myWsTools.runWS(self.wsURL, parm)

        # get the info from the db for this id
        db_fields = "LIBRARY_CREATION_QUEUE_ID, LIBRARY_CREATION_QUEUE, ACTIVE, " \
            "PLATE_TARGET_VOL_LIB_TRIAL_UL, PLATE_TARGET_MASS_LIB_TRIAL_NG, " \
            "TUBE_TARGET_MASS_LIB_TRIAL_NG, TUBE_TARGET_VOL_LIB_TRIAL_UL"
        db_info = self.myDB.get_columns_from_single_row_single_table(db_fields,
                                                        'pc.dt_library_creation_queue_cv',
                                                        'LIBRARY_CREATION_QUEUE_ID',
                                                        str(parm))

        self.myWsTools.compare_ws_to_db(self.errorMsgs, 'lc-queue-id', record, 'lc-queue', db_info, 'LIBRARY_CREATION_QUEUE')
        self.myWsTools.compare_ws_to_db(self.errorMsgs, 'lc-queue-id', record, 'active', db_info, 'ACTIVE')
        self.myWsTools.compare_ws_to_db(self.errorMsgs, 'lc-queue-id', record, 'plate-target-mass-lib-trial-ng', db_info, 'PLATE_TARGET_VOL_LIB_TRIAL_UL')
        self.myWsTools.compare_ws_to_db(self.errorMsgs, 'lc-queue-id', record, 'plate-target-vol-lib-trial-ul', db_info, 'PLATE_TARGET_MASS_LIB_TRIAL_NG')
        self.myWsTools.compare_ws_to_db(self.errorMsgs, 'lc-queue-id', record, 'tube-target-mass-lib-trial-ng', db_info, 'TUBE_TARGET_MASS_LIB_TRIAL_NG')
        self.myWsTools.compare_ws_to_db(self.errorMsgs, 'lc-queue-id', record, 'tube-target-vol-lib-trial-ul', db_info, 'TUBE_TARGET_VOL_LIB_TRIAL_UL')

    def test_active_records(self):
        self.myWsTools.test_active_records(self.errorMsgs, self.wsURL)


# ------this is how to run the tests--------------------------
myTest = LibraryCreationQueuesWST()
print("----------------------------------------------------")
print("TESTING: get all records and verify schema")
myTest.get_all_records_and_verify_schema()

print("----------------------------------------------------")
print("TESTING: get all active records")
myTest.test_active_records()
print("----------------------------------------------------")

print("----------------------------------------------------")
print("TESTING: with valid <id> parameter.")
myTest.get_records_with_parameter(5)

# TODO invalid parameter test
# print("----------------------------------------------------")
# print("TESTING: url with invalid <id> parameter.")

# TODO PUT not allowed
# TODO POST not allowed
# TODO DELETE not allowed

print("----------------------------------------------------")
print("---end---")

if len(myTest.errorMsgs) == 0:
    print("No Errors")
else:
    print("Errors occurred!")
    print("Error messages:")
    print(myTest.errorMsgs)
