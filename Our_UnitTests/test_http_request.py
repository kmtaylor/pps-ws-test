"""
    unit tests for http tools
"""
from unittest import TestCase
from unittest.mock import patch, Mock
import requests

from Utils.http_request_tools import HttpTools
from Utils.util_jsonInput import jsonInput

class TestHttpTools(TestCase):
    """
        unit tests for http tools
    """

    SOW_1 = '{"logical-amount-units":[' \
            '{"index":0,"sow-item-id":1,"library-name":null,"from-logical-amount-units":null,' \
            '"from-logical-amount":null,"to-logical-amount-units":null,"to-logical-amount":null,' \
            '"target-logical-amount":200,"logical-amount-units":"X","run-mode":"Illumina HiSeq-HO 2 X 150",' \
            '"read-total":2,"read-length-bp":150,"number-of-reads-bp":2000000,"genome-size-estimated-mb":3,' \
            '"mappable-yield-rate":null}]}'

    SOW_41 = '{"errors": [{"index": 0,"key": "sow-item-id","message": "sow item not found for ID [41]"}]}'

    def setUp(self):
        """setup"""
        self.my_http = HttpTools()


    def test_http_get_json_exception(self):
        """ test for url that isn't found s/b exception """
        self.assertRaises(Exception, self.my_http.http_get_json("www.python.org"))

    def test_http_get_json_no_json(self):
        """ no json returned s/b empty string """
        msg = self.my_http.http_get_json("http://www.python.org")
        self.assertEqual(msg, "")

    def test_http_get_json(self):
        """ test json returned by verifying content """
        msg = self.my_http.http_get_json("http://ip.jsontest.com/")
        self.assertIn('ip', msg)

    def test_http_post_json(self):
        """ test post w. json returned """
        json_str = '{"logical-amount-units": [{"sow-item-id": 1}]}'
        url = 'https://ws-access-int.jgi.doe.gov/pps-logical-amount-units-converter'
        response_json = self.my_http.http_post_json(url, json_str)
        self.assertIn('logical-amount-units', response_json)

    @patch.object(
        requests,  # object whose attribute I want to patch
        'post',  # the attribute name I want to patch
        return_value=(Mock(status_code=200, text=SOW_1)) # replacement for the patched attribute
    )
    def test_http_post_json_w_status(self, mock_requests):
        """ test post w. json status code and json returned ; verify status """
        json_str = '{"logical-amount-units": [{"sow-item-id": 1}]}'
        url = 'https://ws-access-int.jgi.doe.gov/pps-logical-amount-units-converter'
        status_code, response_json = self.my_http.http_post_json_w_status(url, json_str)
        self.assertIn('logical-amount-units', response_json)
        self.assertEqual(200, status_code)

    @patch.object(
        requests,  # object whose attribute I want to patch
        'post',  # the attribute name I want to patch
        #side_effect=ValueError()
        return_value=(Mock(status_code=422, text=SOW_41))  # replacement for the patched attribute
    )
    def test_http_post_json_w_status_error(self, requests_mock):
        """ test post w. json status code and json returned ; verify status """
        json_str = '{"logical-amount-units": [{"sow-item-id": 41}]}'
        url = 'https://ws-access-int.jgi.doe.gov/pps-logical-amount-units-converter'
        status_code, response_json = self.my_http.http_post_json_w_status(url, json_str)
        self.assertIn('errors', response_json)
        self.assertEqual(422, status_code)

    @patch.object(
        requests,  # object whose attribute I want to patch
        'post',  # the attribute name I want to patch
        return_value=(Mock(status_code=204, text="")) # replacement for the patched attribute
    )
    def test_http_post_json_w_status_w_empty_string_response(self, mock_requests):
        """ test post w. json status code and json returned ; verify status """
        json_str = jsonInput().rqc_rework_info
        url = 'https://ws-access-int.jgi.doe.gov/pps-rqc-rework-action'
        status_code, response_json = self.my_http.http_post_json_w_status(url, json_str)
        self.assertEqual('', response_json)
        self.assertEqual(204, status_code)

    @patch.object(
        requests,  # object whose attribute I want to patch
        'post',  # the attribute name I want to patch
        return_value=(Mock(status_code=204, text="")) # replacement for the patched attribute
    )
    def test_http_http_post_json_w_expectedStatus_w_empty_string_response(self, mock_requests):
        """ test post w. json returned """
        json_str = jsonInput().rqc_rework_info
        url = 'https://ws-access-int.jgi.doe.gov/pps-rqc-rework-action'
        status, response_json = self.my_http.http_post_json_w_expectedStatus(url, json_str, 204)
        self.assertEqual('', response_json)