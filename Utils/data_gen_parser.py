"""
   Tools to aid with parsing the json response from data generator service
"""

from Utils.Analyte import Analyte


class DataGenParser():

    def __init__(self, data):
        self.data = data

    def get_spids(self):
        """
        parse the data gen ui response and return the list of spids that were created
        :return: list of spids
        """
        print('SP IDs created: ', self.data['sp-ids'])
        return self.data['sp-ids']

    def get_sample_ids(self):
        """
        parse the data gen ui response and return the pmo sample ids
        :return: list of pmo (uss) sample ids
        """
        sample_ids = []
        for analyte in self.data['analytes']:
            ids = analyte['ancestry']['sample-ids'];
            sample_ids += ids

        print('Sample IDs created : ', sample_ids)
        return sample_ids

    def get_analytes(self):
        analytes = []
        for analyte in self.data['analytes']:
            lims_id = analyte['final-analyte-lims-id'];
            name = analyte['final-analyte-name'];
            spids = analyte['ancestry']['sp-ids']
            sample_ids = analyte['ancestry']['sample-ids']

            if 'library-names' in analyte['ancestry']:
                library_names = analyte['ancestry']['library-names']
            else:
                library_names = []

            if 'library-stocks-lims-ids' in analyte['ancestry']:
                library_lims_ids = analyte['ancestry']['library-stocks-lims-ids']
            else:
                library_lims_ids = []

            if 'pool-name' in analyte['ancestry']:
                pool_name = analyte['ancestry']['pool-name']
            else:
                pool_name = []

            analyte = Analyte(lims_id, name, spids, sample_ids, library_names, library_lims_ids, pool_name)
            analytes.append(analyte)

        print('Artifacts created: ')
        for analyte in analytes:
            print('    ', analyte)

        return analytes  # return a list of Analyte objects