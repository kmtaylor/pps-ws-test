from enum import Enum


class ClarityQueues(Enum):
    SAMPLE_SUBMISSION = 'Sample Submission'
    APPROVE_SHIPPING = 'SM Approve For Shipping'
    SAMPLE_RECEIPT = 'Receive Samples From Barcode Association'
    SAMPLE_QC = 'SM Sample QC'
    SOW_QC = 'SM SOW Item QC'
    METABOLOMICS = 'SM Sample QC Metabolomics'
    FRACTIONATION = 'SM Sample Fractionation'
    ALIQUOT_CREATION = 'AC Sample Aliquot Creation'
    LIBRARY_CREATION = 'LC Library Creation'
    QPCR = 'LQ Library qPCR'
    POOLING = 'LP Pool Creation'
    SIZE_SELECTION = 'Size Selection'
    ILLUMINA_SEQUENCING = 'SQ Sequencing'
    PB_BINDING = 'PacBio Library Binding'
    PB_ANNEALING = 'PacBio Library Annealing'
    PB_SEQ_PLATE_CREATION = 'PacBio Sequencing Plate Creation'
    PB_SEQ_PLATE_COMPLETE = 'PacBio Sequencing Complete'
    RQC = 'RQC'


class ClarityReQueues(Enum):
    QC_REQUEUE_FOR_LIBRARY_CREATION = 'Automatic Sequence QC Requeue for Library Creation'
    QC_REQUEUE_FOR_POOL_CREATION = 'Automatic Sequence QC Requeue for Pool Creation'
    QC_REQUEUE_FOR_QPCR = 'Automatic Sequence QC Requeue for qPCR'
    QC_REQUEUE_FOR_SEQUENCING = 'Automatic Sequence QC Requeue for Sequencing'


class Container(Enum):
    TUBE = 'Tube'
    PLATE = '96 well plate'
