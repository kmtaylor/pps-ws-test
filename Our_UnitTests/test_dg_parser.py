import unittest
import json

from Utils.data_gen_parser import DataGenParser
import Our_UnitTests.data_gen_mock_data as mock_data


class TestDataGenParser(unittest.TestCase):
    """
        unit tests for the data generator json parser
    """

    def test_get_spids(self):
        """ test that spids are read from response json """
        data = json.loads(mock_data.JSON_THREE_TUBE_SAMPLES)
        parser = DataGenParser(data)
        spids = parser.get_spids()
        self.assertEqual(spids, [1665292, 1665293, 1665294])

    def test_get_sample_ids(self):
        """ test that spids are read from response json """
        data = json.loads(mock_data.JSON_THREE_TUBE_SAMPLES)
        parser = DataGenParser(data)
        sample_ids = parser.get_sample_ids()
        self.assertEqual(sorted(sample_ids), sorted([344208, 344209, 344210]))

    def test_get_artifacts_libraries(self):
        data = json.loads(mock_data.JSON_THREE_TUBE_LIBRARIES)
        parser = DataGenParser(data)
        artifacts = parser.get_analytes()
        self.assertTrue(len(artifacts) == 3)

    def test_get_artifacts_pool(self):
        data = json.loads(mock_data.JSON_POOL_10)
        parser = DataGenParser(data)
        artifacts = parser.get_analytes()
        self.assertTrue(len(artifacts) == 1)
        (self.assertTrue(len(artifacts[0].spids) == 10))

    def test_get_artifact_names(self):
        data = json.loads(mock_data.JSON_THREE_TUBE_LIBRARIES)
        parser = DataGenParser(data)
        artifacts = parser.get_analytes()
        for artifact in artifacts:
            name = artifact.name
            self.assertTrue(name in mock_data.THREE_TUBE_LIBRARY_NAMES)
            mock_data.THREE_TUBE_LIBRARY_NAMES.remove(name)


if __name__ == '__main__':
    unittest.main()
