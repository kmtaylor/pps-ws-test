
# created 1/13/21

# the WS URLs are :
# https://ws-access-dev.jgi.doe.gov
# https://ws-access-int.jgi.doe.gov
# https://ws-access.jgi.doe.gov


#this test runs the freezerLoc WS and verifies that the data return is as specified
# see  https://issues.jgi.doe.gov/browse/QATEST-1002
# specification: https://docs.jgi.doe.gov/display/DW/Container+Freezer+Location




# The tests included here are:
#
#     Post dw-container-freezer-locations with array of barcodes, return status=200;
#     response contains List of JSON objects. Each JSON object contains the resource attributes: barcode and freezer location strings
#       If the container can not be found for a given barcode, do not return an error and set the freezer location to "Location Unavailable".
#       If the container can be found in multiple freezers, do not return an error and return multiple records for the same barcode.
#       if the request has duplicate items (e.g., ["str123","str123"]), do not return an error and return only one JSON object per duplicate item. GET dw-pc-qc-type?organization-name=<organization-name> status=200; query parameter is ignored
#     post dw-container-freezer-locations with empty object; status =400 “error”:“Bad Request”
#     post dw-container-freezer-locations with incorrect object; status =400 “error”:“Bad Request”
#     post dw-container-freezer-locations with invalid  URL; status =404 “error”:“Resource Not Found”


import requests
import json

from Utils.open_yaml import open_yaml
from Utils.util_jsonInput import jsonInput
from Utils.http_request_tools import HttpTools


class  freezerLocWST():
    def __init__(self):
        self.printOn = True
        self.errorCnt = 0
        self.dict_config = open_yaml()
        self.wsURL = self.dict_config['ws_url'] + "dw-container-freezer-locations"
        self.myJson = jsonInput()
        self.myRequests = HttpTools()



    # -------------------------------------------------------------------------------------------
    # runWS
    #
    def runWS(self,parm="",parmvalue=""):
        wsURL = self.wsURL
        if parm:
            wsURL = self.wsURL + "/?" + parm +"=" + parmvalue

        if self.printOn:
            print("using WS url: ", wsURL)
        status,responseJson = self.myRequests.http_get_json_w_expectedStatus(wsURL,200)

        return responseJson

    # -------------------------------------------------------------------------------------------
    # checkForRequiredParameters
    #

    def checkForRequiredParameters(self,freezerLocRecord):
        verificationErrors = 0
        print(json.dumps(freezerLocRecord, indent=4, sort_keys=True))
        errorMsg = ""
        freezerLocId = freezerLocRecord["qc-type-id"]
        if not (isinstance(freezerLocId,int)):
            print(freezerLocId)
            print("data type= ", type(freezerLocId))
            verificationErrors +=1
            errorMsg += "qc-type-id  inconsistency,"

        freezerLoc = freezerLocRecord["qc-type"]
        if not(freezerLoc and  isinstance(freezerLoc,str)):
            verificationErrors +=1
            print (freezerLoc)
            print ("data type= ", type(freezerLoc))
            errorMsg += "qc-type inconsistency,"

        active = freezerLocRecord["active"]
        if active:
            if not (isinstance(active, bool)):
                verificationErrors += 1
                errorMsg += "active inconsistency,"


        if verificationErrors > 0:
            print("***DB errors for freezerLoc: ",freezerLoc, ", " , freezerLocId, ", required parameter(s) inconsistency = ", verificationErrors, " errors")
        return verificationErrors,freezerLoc,errorMsg



    #------------------------------------------------------------------------------------
    # getAll_freezerLocs_and_verifyData
    # run the web service without parameters. It verifies the data in response to be
    # consistent with the contract. Prints a report including errors found
    # inputs: none
    # outputs: none
    def getAll_freezerLocs_and_verifyData(self):
        freezerLocData = self.runWS()
        #print(freezerLocData)
        activeCount =0
        totalErrors = 0
        freezerLocDict={}
        #freezerLocRecord = freezerLocData[0]
        for record in freezerLocData:
            #print(json.dumps(record, indent=4, sort_keys=True))
            if record["active"] == True :
                activeCount +=1
            freezerLocErrors,freezerLoc,errMsg = self.checkForRequiredParameters(record)
            totalErrors = freezerLocErrors + totalErrors
            if freezerLocErrors > 0:
                freezerLocDict[freezerLoc] =errMsg  #save errors for later reporting
        print ("***** Freezer Location DB attribute verification Report ! *****")
        print("number of active Freezer Locations = ", activeCount)
        print("number of total Freezer Location records = ",len(freezerLocData))

        #print report of this test
        if totalErrors >0 :
            print("total errors found with DB/contract inconsistency= ", totalErrors)
            print("****************************************************************")
            print("Freezer Location  :      ERRORS FOUND:")
            for freezerLoc in freezerLocDict:
                print (freezerLoc, ":", freezerLocDict[freezerLoc])



  #------------------------------------------------------------------------------------
    # getAllfreezerLocs_with_parameter
    # run the web service with parameters.
    # inputs: none
    # outputs: none
    def get_freezerLocs_with_query_parameter(self,parm,value):
        freezerLocData = self.runWS(parm,value)

        print("number of total Freezer Location records = ",len(freezerLocData))





#------this is how to run the tests--------------------------

# comment from Angie - None of these tests work.
# They are trying to do a GET when that's not allowed by this service per its docs here -
# https://prospero-int.jgi.doe.gov/ws/container-freezer-locations/api-docs/index.html
# Also, I think these may have been a work in progress by Becky that never got completed
# it looks to me like all the tests below actually do the same thing???



# myTest = freezerLocWST()
# print ("----------------------------------------------------")
# print ("TESTING: Freezer Location WS and verify data ")
# myTest.getAll_freezerLocs_and_verifyData()
#
# myTest = freezerLocWST()
# print ("----------------------------------------------------")
# print ("TESTING: Freezer Location WS  with barcodes that can not be found and verify data ")
# myTest.getAll_freezerLocs_and_verifyData()
#
# myTest = freezerLocWST()
# print ("----------------------------------------------------")
# print ("TESTING: Freezer Location WS  using the same barcode in request multiple times and verify data ")
# myTest.getAll_freezerLocs_and_verifyData()
#

# myTest = freezerLocWST()
# print ("----------------------------------------------------")
# print ("TESTING: Freezer Location WS and verify data ")
# myTest.getAll_freezerLocs_and_verifyData()
#
# myTest = freezerLocWST()
# print ("----------------------------------------------------")
# print ("TESTING: Freezer Location WS  with barcodes that can not be found and verify data ")
# myTest.getAll_freezerLocs_and_verifyData()
#
# myTest = freezerLocWST()
# print ("----------------------------------------------------")
# print ("TESTING: Freezer Location WS  using the same barcode in request multiple times and verify data ")
# myTest.getAll_freezerLocs_and_verifyData()
#

