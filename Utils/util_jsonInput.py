#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Becky'
# created 8/16/17
# name:  jsonInput  -  storage for the json requests for WS,

class  jsonInput():

    def __init__(self):

        #-------use these for add sow WS -------------------------------------------
        #these are all the attributes that can be updated on put command. Use this or subset
        self.addSowToExistSamJson = {  # ok
                                       "created-by-cid": 5981,
                                       "sequencing-project-id": 1651794,
                                       "sow-item-purpose": "RnD",
                                       "sample-id": 340631,
                                       "sow-number": 342,
                                       "target-logical-amount": 4,
                                       "logical-amount-units": "Gb",
                                       "degree-of-pooling": 10,
                                       "run-mode": "PacBio Revio 1 X 1440",
                                       "sm-comments": "random sm comments",
                                       "lc-comments": "random lc comment",
                                       "sq-comments": "random sq comment"
                                       }

        self.addSowToNewSamJson =  {  #ok
                                       "created-by-cid": 5981,
                                       "sequencing-project-id": 1651794,
                                       "sow-item-purpose": "Unanticipated Additional Work",
                                       "sample-id": None,
                                       "sow-number": 258,
                                       "target-logical-amount": 200,
                                       "logical-amount-units": "Gb",
                                       "degree-of-pooling": 2,
                                       "run-mode": "Illumina NovaSeq S4 2 X 150"
                                               }

        self.addSowToLibJson = {  #works but need to make sure all sows are in terminal state, this will create new sow in progress
                                       "created-by-cid": 5981,
                                       "sequencing-project-id": 1660472,
                                       "sow-item-purpose": "Validation",
                                       "library-stock-id": "2-5352339",
                                       "sow-number": 258,
                                       "target-logical-amount": 200,
                                       "logical-amount-units": "Gb",
                                       "degree-of-pooling": 10,
                                       "run-mode": "Illumina NovaSeq S4 2 X 150"
                                       #"qc-type":"Purity"
                                        }
        self.addSowToPlate = {  #ok
                                       "created-by-cid": 5981,
                                       "sequencing-project-id": 1259375,
                                       "sow-item-purpose": "Unanticipated Additional Work",
                                       "sample-id": 240595,
                                       "sow-number": 280,
                                       "target-logical-amount": 55,
                                       "logical-amount-units": "M mappable reads",
                                       "degree-of-pooling": 48,
                                       #"run-mode": "Illumina NextSeq-HO 2 X 150",
                                       "qc-type": "Quantity+Quality+Purity",
                                       "run-mode": "PacBio Sequel 1 X 120",
                                       "sm-comments": "77777777y",
                                       "lc-comments": "yyyyyyyyyyyyyyh",
                                       "sq-comments": "hhhhhhhhhh"
                                       }


        self.addSowToSingleCellSam = {  #
                                        "created-by-cid": 5981,
                                        "sequencing-project-id": 1168895,
                                        "sow-item-purpose": "Anticipated Planned Work",
                                        "sample-id": 165011,
                                        "sow-number": 319,
                                        "target-logical-amount": 4,
                                        "logical-amount-units": "reads per barcode",
                                        "degree-of-pooling": 10,
                                        "run-mode": "Illumina NextSeq-HO 2 X 150",
                                        "sm-comments": "random sm comments",
                                        "lc-comments": "random lc comment",
                                        "sq-comments": "random sq comment"
        }



        # -------use these for delete sow WS -------------------------------------------
        # these are all the attributes that can be updated on put command. Use this or subset
        self.deleteSowJson = {  # ok
            "submitted-by": 5981,
            "sow-item-ids": [11779]
            }

        # -------use these for edit sow WS -------------------------------------------
        # these are all the attributes that can be updated on put command. Use this or subset
        self.editSowsJson = {  # ok
            "submitted-by": 5981,
            "sow-item-ids" : [11779],
            #"sow-item-purpose": "Anticipated Planned Work",
            # "degree-of-pooling" : 4,
            # "target-logical-amount": 8,
            # "logical-amount-units": "Gb",
            # "run-mode": "Illumina NextSeq-HO 2 X 150",
            # "exome-capture-probe-set": "",
            # "itag-primer-set": "",
            # "sequencing-optimization-factor": 0,
            }

        #----------------------------------------------------------------------------------------------------
        #---------- use these for sequencing project submission to get different types of sp projects created
        self.spSubmissionAttributesGeneric = {
            "submitted-by-cid": 5981,
            "sequencing-projects": [
                {
                    "final-deliverable-project-id": 1091346,   # clarity int FD = 1226772,
                    "sequencing-project-name": "MicrobialMinimalDraftIsolate_Auto Testing rjr ",
                    "sequencing-product-id": 1,
                    "sequencing-strategy-id": 1,
                    "expected-number-of-samples": 1,
                    "eligible-for-public-release": "Y",
                    "sequencing-project-manager-cid": 5981,
                    "sequencing-project-contact-cid": 1717,
                    "sample-contact-cid": 5981,
                    "embargo-days": 0,
                    "auto-schedule-sow-items": "Y",
                    "sequencing-project-comments-and-notes": "testing sp submission with autotest program",
                    "existing-jgi-taxonomy-id": 1492
                }
            ]
        }
        # ----------------------------------------------------------------------------------------------------
        # ---------- use these for Sample submission  -----------------------------
        self.samSubmissionAttributesGeneric = {
            "samples": [
                {
                    "sample-format": "RNAstable",
                    "dnase-treated": "N",
                    "collaborator-concentration-ngul": 100.29,
                    "collaborator-volume-ul": 54.78,
                    "storage-solution": "MDA Reaction Buffer",
                    "starting-culture-axenic-strain-pure": "N",
                    "biosafety-material-category": "Bacteria",
                    "sample-isolation-method": "test method #111",
                    "sample-collection-date": "2012-10-01",
                    "sample-isolated-from": "my house"
                }
            ],
            "submitted-by-cid": 5981,
            "validate-only": False
            }

        # ----------------------------------------------------------------------------------------------------
        # ---------- use these for library-sequencing-status  -----------------------------
        self.library_seq_status = {
            "library-names": ["GXSAU"],
            "submitted-by-cid": 13
        }

        # ----------------------------------------------------------------------------------------------------
        # ---------- use these for rqc rework testing  -----------------------------
        self.rqc_rework_info = {
            "rqc-rework-info": [
                {
                    "physical-run-unit-id": "2-5489974",
                    "library-name": "HYXTG",
                    "targeted-logical-amount": 89.3,
                    "total-logical-amount-completed": 1.2,
                    "abandon-sample": "N",
                    "abandon-library": "N"
                }
            ],
            "submitted-by": 13
        }

    def updateJson(self, jsonName, attribute, value):
        jsonName[attribute]=value

    # def updateEmbeddedJson(self,jsonName,category, attribute,value):
    #     submissionAttribute = jsonName
    #     embeddedJson = jsonName[category]
    #     embeddedJson[attribute]=value
    #     jsonName[category] = embeddedJson
    #     print (jsonName)
    #     input ("wait here")



