#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Becky'
# created 7/25/17

import requests
from xml.etree import ElementTree as ET
from Utils.open_yaml import open_yaml
import Utils.open_logger as ol

#name:  udfValidation  -

class udfTools():
    def __init__(self):
        """
        Init
        """
        self.logger = ol.open_logger()
        dict_config = open_yaml()
        self.clarity_server = dict_config['server_name']
        self.user = dict_config['clarity_user']
        self.pw = dict_config['clarity_password']



    # ---------project udfs--tools--------------------------------------------------------------------------------
    # setURL
    # build the URL to fix the request for clarity API
    # inputs:  server  i.e. 'clarityprd1.jgi-psf.org'
    #          sp  -  the sequencing project ID under test
    def setURLproject(self, sp):
        # https://clarity-dev01.jgi-psf.org/api/v2/projects?name=1012803
        requestURL = 'https://' + self.clarity_server + '/api/v2/projects?name=' + str(sp)
        return requestURL

    # -------------------------------------------------------------------------------------------
    # connectToClarityAPIprojects
    #
    # inputs: requestURL
    # outputs: url of SP UDFs

    def connectToClarityAPIprojects(self,requestURL):
        projURL = ''
        try:
            headers = {'Content-Type': 'application/xml'}
            r = requests.get(requestURL, headers=headers, auth=(self.user, self.pw), stream=True, timeout=2)
        except:
            print("Opps! Can't get the request, Time out")
            return ''
        status = r.status_code
        if status == 200:
            root = ET.fromstring(r.text)
            for project in root.iter('project'):
                projURL = project.get('uri')
        else:
            print("*** Error, Unexpected Returned Status = ", status)

        return projURL

    # -------------------------------------------------------------------------------------------
    # get_project_udfs_by_spid
    #
    # inputs: SP ID
    # outputs: dict of udfs, where key is udfname and value is the udf value as a string
    #           so, for a udf like <udf:field type="String" name="Proposal Id">193</udf:field>
    #           there will be a dict entry like 'Proposal Id': '193'

    def get_project_udfs_by_spid(self, spid):
        project_search_by_name_url = self.setURLproject(spid)
        project_api_url = self.connectToClarityAPIprojects(project_search_by_name_url)
        udf_dict = self.getProjectUDFs(project_api_url)
        return udf_dict
    # -------------------------------------------------------------------------------------------
    # getProjectUDFs
    #
    # inputs: requestURL
    # outputs: url of SP UDFs

    def getProjectUDFs(self, requestURL):
        udfDict = {}
        try:
            headers = {'Content-Type': 'application/xml'}
            r = requests.get(requestURL, headers=headers, auth=(self.user, self.pw), stream=True, timeout=2)
        except:
            print("Opps! Can't get the request, Time out")
            return {}
        status = r.status_code
        if status == 200:
            # print("Valid Returned Status = ", status)
            # print(r.text) # this is the xml of the project base record
            root = ET.fromstring(r.text)
            for i, node in enumerate(root):
                udfName = node.get('name')
                if udfName is not None:
                    # print(udfName)
                    udfValue = node.text
                    # print(udfValue)
                    udfDict[udfName] = udfValue
                # print('name= ' + str(root[i].get('name')) + ', value= ' + str(root[i].text) )
                # print (root[i].text)

            # print (udfDict)

        else:
            print("*** Error, Unexpected Returned Status = ", status)

        return udfDict


    # ---------sample udfs---tools-------------------------------------------------------------------------------

    def setURLsample(self,sampleId, server):
        requestURL = 'https://' + server + '/api/v2/samples?name=' + str(sampleId)
       # print("Getting UDFs from: " + requestURL)  # https://clarity-dev01.jgi-psf.org/api/v2/samples?name=1012803
        #
        return requestURL

    # -------------------------------------------------------------------------------------------
    # findLatestClaritySampleUrlBySampleName
    #
    # inputs: requestURL - a url for a sample search like https://jgi-int.claritylims.com/api/v2/samples?name=339489
    # outputs: the last sample url returned by the sample search, if there are scheduled samples this will return
    #          the most recently created on
    def findLatestClaritySampleUrlBySampleName(self, requestURL):
        sampleURL = ''
        try:
            # r= requests.get(requestURL, timeout=2)
            headers = {'Content-Type': 'application/xml'}
            r = requests.get(requestURL, headers=headers, auth=(self.user, self.pw), stream=True, timeout=2)

        except:
            # status = r.status_code
            print("Opps! Can't get the request, Time out")
            return ''
        status = r.status_code
        if status == 200:
            #print("Valid Returned Status = ", status)
            #print(r.text) #this is the xml of the project base record
            root = ET.fromstring(r.text)
            #print (root.tag)
            for sample in root.iter('sample'):
                #print("from connecttoclarity")
                #print(sample.attrib)
                sampleURL = sample.get('uri')
                # print("-")
        else:
            print("*** Error, Unexpected Returned Status = ", status)

        return sampleURL


    # -------------------------------------------------------------------------------------------
    # getSampleUDFs
    #
    # inputs: url of the clarity sample API
    # outputs: a dictionary of UDFS  (name of udf: value of udf)
    def getSampleUDFs(self,requestURL):
        udfDict = {}
        try:
            headers = {'Content-Type': 'application/xml'}
            r = requests.get(requestURL, headers=headers, auth=(self.user, self.pw), stream=True,timeout=2)
        except:
            print("Opps! Can't get the request, Time out")
            return {}
        status = r.status_code
        if status == 200:
            #print("Valid Returned Status = ", status)
            #print(r.text) # this is the xml of the project base record
            root = ET.fromstring(r.text)
            for node in root.iter():
                udfName = node.get('name')
                #print(udfName)
                udfValue = node.text
                #print(udfValue)
                udfDict[udfName] = udfValue
                #print('name= ' + str(root[i].get('name')) + ', value= '  +  str(root[i].text) )
                #print (root[i].text)


                #print (udfDict)

        else:
            print("*** Error, Unexpected Returned Status = ", status)

        return udfDict




    # ---------artifacts udfs---tools-------------------------------------------------------------------------------

    def setURLartifact(self,artifactName, server):
        requestURL = 'https://' + server + '/api/v2/artifacts?name=' + str(artifactName)
        #print("Getting UDFs from: " + requestURL)  # https://clarity-dev01.jgi-psf.org/api/v2/artifacts?name=BXCCT
        #
        return requestURL

    # ---------artifacts udfs---tools-------------------------------------------------------------------------------

    def setURLartifactUsingLimsId(self,lims_id, server):
        requestURL = 'https://' + server + '/api/v2/artifacts/' + str(lims_id)
        return requestURL

    # -------------------------------------------------------------------------------------------
    # connectToClarityAPIartifacts
    #
    # inputs: requestURL
    # outputs: url of artifacts  (last of list)
    def connectToClarityAPIartifacts(self,requestURL):
        artifactURL = ''
        try:
            # r= requests.get(requestURL, timeout=2)
            headers = {'Content-Type': 'application/xml'}
            r = requests.get(requestURL, headers=headers, auth=(self.user, self.pw), stream=True, timeout=2)

        except:
            # status = r.status_code
            print("Opps! Can't get the request, Time out")
            return ''
        status = r.status_code
        if status == 200:
            #print("Valid Returned Status = ", status)
            #print(r.text)  # this is the xml of the project base record
            root = ET.fromstring(r.text)
            #print(root.tag)
            for art in root.iter('artifact'):
                # print("from connecttoclarity")
                #print(art.attrib)
                artifactURL = art.get('uri')
                # print(artifactURL)
        else:
            print("*** Error, Unexpected Returned Status = ", status)

        return artifactURL     #returns the last in a list if more than one.
#---------------------------------------------------------------------------------------------------
    # -------------------------------------------------------------------------------------------
    # get list of  artifact URLs
    #  example :   return the URLs of all artifacts an entity has  (in this case, the sample 270662 has two)
    # < artifact limsid = "VIS93317A1PA1" uri = "https://clarity-int-1.jgi.doe.gov/api/v2/artifacts/VIS93317A1PA1" / >
    # < artifact limsid = "VIS93317A2PA1" uri = "https://clarity-int-1.jgi.doe.gov/api/v2/artifacts/VIS93317A2PA1" / >
    #
    #
    # inputs: requestURL  (i.e "https://clarity-int-1.jgi.doe.gov/api/v2/artifacts?name=270662"
    # outputs: list of urls (of artifacts )

    def getListClarityAPIartifactsURL(self, requestURL):
        artifactURL = ''
        try:
            # r= requests.get(requestURL, timeout=2)
            headers = {'Content-Type': 'application/xml'}
            r = requests.get(requestURL, headers=headers, auth=(self.user, self.pw), stream=True, timeout=2)

        except:
            # status = r.status_code
            print("Opps! Can't get the request, Time out")
            return ''
        status = r.status_code
        if status == 200:
            artifactURLList = []
            #print("Valid Returned Status = ", status)
            # print(r.text)  # this is the xml of the project base record
            root = ET.fromstring(r.text)
            # print(root.tag)
            for art in root.iter('artifact'):
                # print("from connecttoclarity")
                # print(art.attrib)
                artifactURLList.append ( art.get('uri'))
                # print(artifactURL)
        else:
            print("*** Error, Unexpected Returned Status = ", status)

        return artifactURLList

    # -------------------------------------------------------------------------------------------
    # getArtifactUDFs
    #
    # inputs: requestURL
    # outputs: url of artifact UDFs
    def getArtifactUDFs(self,requestURL):
        udfDict = {}
        try:
            headers = {'Content-Type': 'application/xml'}
            r = requests.get(requestURL, headers=headers, auth=(self.user, self.pw), stream=True, timeout=2)
        except:
            print("Oops! Request timed out.")
            print("url requested: ", requestURL)
            print("auth values, user: %s, pw: %s" % (self.user, self.pw))
            return {}
        status = r.status_code
        if status == 200:
            # print("Valid Returned Status = ", status)
            #print(r.text) # this is the xml of the project base record
            root = ET.fromstring(r.text)
            #self.getSchSampleOfLibraryUDFs(requestURL)  # put here just for debuggin
            #queuedList = self.getWorkflowsOfLibrary(requestURL,"QUEUED") # put here just for debuggin
            # print (queuedList)
            #input("waithere")
            for node in root.iter():
                udfName = node.get('name')
                # print(udfName)
                udfValue = node.text
                # print(udfValue)
                udfDict[udfName] = udfValue
                # print('name= ' + str(root[i].get('name')) + ', value= '  +  str(root[i].text) )
                # print (root[i].text)
                # print (udfDict)

        else:
            print("*** Error, Unexpected Returned Status = ", status)

        return udfDict

    # -------------------------------------------------------------------------------------------
    # getSchSampleOfLibraryUDFs  get the UDFS of the scheduled sample related to the library
    #
    # inputs: Clarity API URL  of  entity (library ) artifact record
    # outputs: dict of UDFs from the scheudule sample related to the artifact
    def getSchSampleOfLibraryUDFs(self,libraryApiURL):
        schSampleUDFs = {}
        try:
            headers = {'Content-Type': 'application/xml'}
            r = requests.get(libraryApiURL, headers=headers, auth=(self.user, self.pw), stream=True, timeout=2)
        except:
            print("Opps! Can't get the request, Time out")
            return {}
        status = r.status_code
        if status == 200:
            # print("Valid Returned Status = ", status)
            #print(r.text)  # this is the xml of the project base record
            root = ET.fromstring(r.text)
            for sample in root.iter('sample'):
                sampleAttrib = sample.attrib
                schSampleURL = sampleAttrib.get("uri")
                print ("Getting Schedule Sample UDFs from ", schSampleURL)
                schSampleUDFs = self.getSampleUDFs(schSampleURL)

        else:
            print("*** Error, Unexpected Returned Status = ", status)

        return schSampleUDFs


    # -------------------------------------------------------------------------------------------
    # getSchSampleArtifactUrl  get the URL  of the scheduled sample (related to the library) artifacts
    #
    # inputs: Clarity API URL  of  entity (library ) artifact record
    # outputs: url of sch sample artifacts related to library

    def getSchSampleArtifactUrl(self, libraryApiURL):
        schSampleArtifactUrl = ''
        try:
            headers = {'Content-Type': 'application/xml'}
            r = requests.get(libraryApiURL, headers=headers, auth=(self.user, self.pw), stream=True, timeout=2)
        except:
            print("Oops! Can't get the request, Time out")
            return {}
        status = r.status_code
        if status == 200:
            #print(r.text)  # this is the xml of the project base record
            root = ET.fromstring(r.text)
            for sample in root.iter('sample'):
                sampleAttrib = sample.attrib
                schSampleURL = sampleAttrib.get("uri")
                print("Getting Schedule Sample artifacts URL from ", schSampleURL)
            try:
                headers = {'Content-Type': 'application/xml'}
                r = requests.get(schSampleURL, headers=headers, auth=(self.user, self.pw), stream=True, timeout=2)
            except:
                print("Oops! Can't get the request, Time out")
                return {}
            status = r.status_code
            if status == 200:
                # print("Valid Returned Status = ", status)
                # print(r.text) # this is the xml of the project base record
                root = ET.fromstring(r.text)
                for artifact in root.iter('artifact'):
                    artifactAttrib = artifact.attrib
                    schSampleArtifactUrl = artifactAttrib.get("uri")
                    #print (schSampleArtifactUrl)
            else:
                print("*** Error, Unexpected Returned Status = ", status)
        else:
            print("*** Error, Unexpected Returned Status = ", status)
        return schSampleArtifactUrl

    # -------------------------------------------------------------------------------------------
    # getWorkflowsFromArtifacts get  a list of the workflow stages of the  entity artifacts page that are in certain status
    #
    # inputs: artifactApiURL = Clarity API URL  of  entity (library ) artifact record,
    #           statusSought = status  of workflow that you are looking for (i.e. "QUEUED")
    # outputs: list of the queues that the entity is in as the inputted status
    def getWorkflowsFromArtifacts(self, artifactApiURL,statusSought):
        queueList = []
        try:
            headers = {'Content-Type': 'application/xml'}
            r = requests.get(artifactApiURL, headers=headers, auth=(self.user, self.pw), stream=True, timeout=2)
        except:
            print("Opps! Can't get the request, Time out")
            return {}
        status = r.status_code
        if status == 200:
            # print("Valid Returned Status = ", status)
            # print(r.text)  # this is the xml of the project base record
            root = ET.fromstring(r.text)
            for workflow in root.iter('workflow-stage'):
                wfAttrib = workflow.attrib
                #print (wfAttrib)
                wfStatus = wfAttrib.get("status")
                wfqueue = wfAttrib.get("name")
                if wfStatus == statusSought:
                    queueList.append(wfqueue)
                    #print(wfStatus,wfqueue)
            #input("next")


        else:
            print("*** Error, Unexpected Returned Status = ", status)
        return queueList


    # -------------------------------------------------------------------------------------------
    # getQueueName
    # returns the (list of) clarity queue name of the artifiact URL (in a workflow - stage  status= "QUEUED"
    # inputs:
    #   artifactURL  - the clarity API  url of the artifact i.e. "https://clarity-int-1.jgi.doe.gov/api/v2/artifacts/VIS93317A1PA1"

    def getQueueName(self, artifactApiURL):

        queueList = self.getWorkflowsFromArtifacts(artifactApiURL, "QUEUED")
        # for queueName in queueList:
        #     print (queueName)
        return queueList


    # -------------------------------------------------------------------------------------------
    # getQueuesOfEntity
    # returns the list of clarity queue names that the artifiact is "queued" (in a workflow - stage  status= "QUEUED"
    # inputs:
    #   entityName  - the clarity name of entitiy (i.e. "27066", "HGSBY" )
    # outputs:
    #   workFlowList - list of workflows the entity is "queued" in

    def getQueuesOfEntity(self, entityName):
        #server = "clarity-int-1.jgi.doe.gov"
        entityURL = self.setURLartifact(entityName, self.clarity_server)
        artifactList = self.getListClarityAPIartifactsURL(entityURL) #list of artifacts URLs associated with entity
        workFlowList = []
        for url in artifactList:
             workFlowList.append(self.getQueueName(url))

        return workFlowList




    # -------------------------------------------------------------------------------------------
    def get_first_clarity_url_by_artifact_name(self, library_name):
        """
        Get the FIRST clarity url for a particular entity by name
        Some searches for artifacts by name in clarity return multiple urls
        An example is libraries or pools that have gone through sequencing or searches for pmo sample ids
        This function will return the FIRST url for that artifact name, which for pools or libraries
        will be the pool or library that was originally created rather than the pool/library artifact
        that is on the flowcell
        For searches by sample pmo id, this function will return the url for the original sample
        rather than the scheduled sample
        :param library_name:
        :return: the URL for the supplied library name
        """
        url = self.setURLartifact(library_name, self.clarity_server)
        try:
            headers = {'Content-Type': 'application/xml'}
            r = requests.get(url, headers=headers, auth=(self.user, self.pw), stream=True, timeout=2)
        except:
            print("Opps! Can't get the request, Time out")
            self.logger.info("Opps! Can't get the request, Time out")
            return {}
        root = ET.fromstring(r.text)
        return(root[0].attrib['uri'])
    # -------------------------------------------------------------------------------------------
    def getSowIdFromLibraryUDFs(self, library_name):
        libraryURL = self.get_first_clarity_url_by_artifact_name(library_name)
        libUDFs = self. getSchSampleOfLibraryUDFs(libraryURL)
        sowItemId = libUDFs["SOW Item ID"]
        return sowItemId


    # def remove_artifact_from_workflow(self, artifact_url):
    #     # myudf = udfTools()
    #     # test_url = 'https://clarity-int-1.jgi.doe.gov/api/v2/artifacts/2-5409583'
    #     list = self.getWorkflowsFromArtifacts(test_url,'QUEUED' )
    #     # to do
    #     return 1

    # history -
        # 8-2  created