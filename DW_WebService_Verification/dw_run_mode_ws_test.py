
# created 1/13/21

# the WS URLs are :
# https://ws-access-dev.jgi.doe.gov
# https://ws-access-int.jgi.doe.gov
# https://ws-access.jgi.doe.gov


#this test runs the runmode WS and verifies that the data return is as specified
# see  https://issues.jgi.doe.gov/browse/QATEST-996
# specification:  https://docs.jgi.doe.gov/display/DW/Retrieve+Run+Mode+Metadata%3A+API+Contract



# The tests included here are:
#
#     GET dw-pc-run-mode status=200; response contains json object representing an array of resource attributes
#     GET dw-pc-run-mode?organization-name=<organization-name> status=200; response contains json object
#     representing an array of resource attributes filtered by <organization-name> is found
#     GET dw-pc-run-mode?organization-name=<organization-name> with invalid or missing <organization-name>;
#     status = 200  with empty list
#     Get dw-pc-run-mode with invalid/incorrect URL; status =404 “error”:“Resource Not Found”
#     POST,PUT,DELETE status=405


import requests
import json
from Utils.util_jsonInput import jsonInput
from Utils.http_request_tools import HttpTools


class  runModeWST():
    def __init__(self):
        self.printOn = True
        self.errorCnt = 0
        self.wsURL = "https://ws-access-int.jgi.doe.gov/dw-pc-run-mode"
        self.myJson = jsonInput()
        self.myRequests = HttpTools()



    # -------------------------------------------------------------------------------------------
    # runWS
    #
    def runWS(self,parm="",parmvalue=""):
        wsURL = self.wsURL
        if parm:
            wsURL = self.wsURL + "/?" + parm +"=" + parmvalue


        if self.printOn:
            print("using WS url: ", wsURL)
        status,responseJson = self.myRequests.http_get_json_w_expectedStatus(wsURL,200)

        return responseJson

    # -------------------------------------------------------------------------------------------
    # checkForRequiredParameters
    #

    def checkForRequiredParameters(self,runModeRecord):
        verificationErrors = 0
        print(json.dumps(runModeRecord, indent=4, sort_keys=True))
        errorMsg = ""
        runModeId = runModeRecord["run-mode-id"]
        if not(runModeId and  isinstance(runModeId,int)):
            verificationErrors +=1
            errorMsg += "run-mode-id  inconsistency,"
            #print("run-mode-id  inconsistency")
        runMode = runModeRecord["run-mode"]
        if not(runMode and  isinstance(runMode,str)):
            verificationErrors +=1
            errorMsg += "run-mode inconsistency,"
            #print("run-mode  inconsistency")


        sequencerModelId = runModeRecord["sequencer-model-id"]
        if not (sequencerModelId and isinstance(sequencerModelId, int)):
            verificationErrors += 1
            errorMsg += "sequencer-model-id inconsistency,"
            #print("sequencer-model-id  inconsistency")
        sequencerModel = runModeRecord["sequencer-model"]
        if not (sequencerModel and isinstance(sequencerModel, str)):
            verificationErrors += 1
            errorMsg += "sequencer-model inconsistency,"
            #print("sequencer-model  inconsistency")
        platformId = runModeRecord["platform-id"]
        if not (platformId and isinstance(platformId, int)):
            verificationErrors += 1
            errorMsg += "platform-id inconsistency,"
            #print("platform-id  inconsistency")
        platform = runModeRecord["platform"]
        if not (platform and isinstance(platform, str)):
            verificationErrors += 1
            errorMsg += "platform inconsistency,"
            #print("platform inconsistency")
        organizationName = runModeRecord["organization-name"]
        if not (organizationName and isinstance(organizationName, str)):
            verificationErrors += 1
            errorMsg += "organization-name inconsistency,"
            #print("organization-name inconsistency")
        active = runModeRecord["active"]
        if active:
            if not (isinstance(active, bool)):
                verificationErrors += 1
                errorMsg += "active inconsistency,"
                #print("active  inconsistency")
            readTotal = runModeRecord["read-total"]
            if not (readTotal and isinstance(readTotal, int)):
                verificationErrors += 1
                errorMsg += "read-total inconsistency,"
                # print("read-total  inconsistency")
            readLengthBp = runModeRecord["read-length-bp"]
            if not (readLengthBp and isinstance(readLengthBp, int)):
                verificationErrors += 1
                errorMsg += "read-length-bp inconsistency,"
                # print("read-length-bp inconsistency")
            clarityLimsQueueId = runModeRecord["clarity-lims-queue-name"]
            if not (clarityLimsQueueId and isinstance(clarityLimsQueueId, int)):
                verificationErrors += 1
                errorMsg += "clarity-lims-queue-id inconsistency"
                #print("clarity-lims-queue-id  inconsistency")
        if verificationErrors > 0:
            print("***DB errors for runmode: ",runMode, ", " , runModeId, ", required parameter(s) inconsistency = ", verificationErrors, " errors")
        return verificationErrors,runMode,errorMsg



    #------------------------------------------------------------------------------------
    # getAllRunModes_and_verifyData
    # run the web service without parameters. It verifies the data in response to be
    # consistent with the contract. Prints a report including errors found
    # inputs: none
    # outputs: none
    def getAllRunModes_and_verifyData(self):
        runModeData = self.runWS()
        #print(runModeData)
        activeCount =0
        JGIcount=0
        HAcount = 0
        totalErrors = 0
        runModeDict={}
        #runModeRecord = runModeData[0]
        for record in runModeData:
            #print(json.dumps(record, indent=4, sort_keys=True))
            if record["active"] == True :
                activeCount +=1
            if record["organization-name"] == "JGI":
                JGIcount += 1
            elif record["organization-name"] == "Hudson Alpha":
                HAcount += 1
            runModeErrors,runMode,errMsg = self.checkForRequiredParameters(record)
            totalErrors = runModeErrors + totalErrors
            if runModeErrors > 0:
                runModeDict[runMode] =errMsg  #save errors for later reporting
        print ("***** Run Mode DB attribute verification Report ! *****")
        print("number of organization-name=JGI = ",JGIcount)
        print("number of organization-name=Hudson Alpha = ", HAcount)
        print("number of active run modes = ", activeCount)
        print("number of total run mode records = ",len(runModeData))

        #print report of this test
        if totalErrors >0 :
            print("total errors found with DB/contract inconsistency= ", totalErrors)
            print("****************************************************************")
            print("RUN MODE  :      ERRORS FOUND:")
            for runMode in runModeDict:
                print (runMode, ":", runModeDict[runMode])



  #------------------------------------------------------------------------------------
    # getAllRunModes_with_parameter
    # run the web service with parameters.
    # inputs: none
    # outputs: none
    def getRunModes_with_query_parameter(self,parm,value):
        runModeData = self.runWS(parm,value)
        JGIcount=0
        HAcount = 0

        for record in runModeData:
            #print(json.dumps(record, indent=4, sort_keys=True))
            if record["organization-name"] == "JGI":
                JGIcount += 1
            elif record["organization-name"] == "Hudson Alpha":
                HAcount += 1

        print("number of organization-name=JGI = ",JGIcount)
        print("number of organization-name=Hudson Alpha = ", HAcount)
        print("number of total run mode records = ",len(runModeData))

        # ------------------------------------------------------------------------------------
        # getRunModes_with_parameter(self,parm):
        # invalid.
        # expects one records returned in response by WS
        # inputs: parameter  (ie.  lc spec id)
        # outputs: prints error report




#------this is how to run the tests--------------------------

myTest = runModeWST()
print ("----------------------------------------------------")
print ("TESTING: run mode WS (without parameter) and verify data ")
myTest.getAllRunModes_and_verifyData()

print ("----------------------------------------------------")
print ("TESTING: run mode WS with query parameter organization-name=JGI")
myTest.getRunModes_with_query_parameter("organization-name","JGI")

print ("----------------------------------------------------")
print ("TESTING: run mode WS with query parameter organization-name=Hudson Alpha")
myTest.getRunModes_with_query_parameter("organization-name","Hudson Alpha")


print ("----------------------------------------------------")
print ("----Error testing---organization name value not found --expecting no records -------")
print ("TESTING: run mode WS with query parameter organization-name=HudsonAlpha")
myTest.getRunModes_with_query_parameter("organization-name","HudsonAlpha")

print ("----------------------------------------------------")
print ("----Error testing---using id as parameter --expecting status = 404 -------")
print ("TESTING: run mode WS with parameter id")
saveURL =  myTest.wsURL
myTest.wsURL = "https://ws-access-int.jgi.doe.gov/dw-pc-run-mode/53"
runModeData = myTest.runWS()
myTest.wsURL = saveURL
print(runModeData)

print ("----------------------------------------------------")
print ("----Error testing---invalid query parameter -- expecting all records -------")
print ("TESTING: run mode WS with invalid query parameter platform")
myTest.getRunModes_with_query_parameter("platform","PacBio")

print ("----------------------------------------------------")

print ("----Error testing---incorrect URL -- expecting status 404 -------")
print ("TESTING: run mode WS incorrect URL")
saveURL =  myTest.wsURL
myTest.wsURL = "https://ws-access-int.jgi.doe.gov/dw-pc-run-mode/JGI"
runModeData = myTest.runWS()
myTest.wsURL = saveURL
print(runModeData)

print ("----------------------------------------------------")
print("---end---")
#
#

#









