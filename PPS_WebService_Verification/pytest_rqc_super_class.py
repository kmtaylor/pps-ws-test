from decimal import Decimal

from assertpy import assert_that
from enum import Enum
import copy

from Utils.clarity_constants import ClarityQueues, ClarityReQueues
from Utils.http_request_tools import HttpTools
from Utils.sow_status import SowStatus
from Utils.util_jsonInput import jsonInput
from Utils.util_udfTools import udfTools
from Utils.open_yaml import open_yaml


class RqcActions(Enum):
    MAKE_NEW_LIBRARY = 'make-new-library'
    QPCR_EXISTING_LIBRARY = 'qpcr-existing-library'
    SEQUENCE_EXISTING_LIBRARY = 'sequence-existing-library'
    USE_LIBRARY_IN_NEW_POOL = 'use-existing-library-in-new-pool'
    RESCHEDULE_POOL = 'reschedule-pool'


class TestRqcSuper:

    dict_config = open_yaml()
    ws_url = dict_config['ws_url']
    clarity_server = dict_config['server_name']
    user_id = dict_config['user_id']

#    my_db = DataBaseTools()
#    my_db.connect(my_db.int_db)
    my_udf = udfTools()
#    data_gen = data_gen_tools.DataGenTools()
    my_json = jsonInput()
    my_http = HttpTools()

    INQUIRY_URL = 'pps-rqc-rework-inquiry'
    ACTION_URL = 'pps-rqc-rework-action'

    INQUIRY_SUCCESS_STATUS = 201
    ACTION_SUCCESS_STATUS = 204

    # TEST FUNCTIONS
    def check_inquiry_response(self, lims_id, library_name, allowed_lab_actions):
        # create the submission json
        submission_json = self.submission_json_creation(lims_id, library_name)

        # post to rqc-rework-inquiry
        ws_response = self.post_to_rqc_service(self.INQUIRY_URL, submission_json, self.INQUIRY_SUCCESS_STATUS)

        # check that the correct info is shown in the results
        self.validate_allowed_actions_list(ws_response, allowed_lab_actions)

    def post_to_rqc_service(self, endpoint, submission_json, success_status):
        url = self.ws_url + endpoint
        status, ws_resp = self.my_http.http_post_json_w_expectedStatus(
            url, submission_json, success_status)

        error_msg = f"POST to {url} with json\n {submission_json} did not return expected success status"
        assert_that(status, description=error_msg).is_equal_to(success_status)

        return ws_resp

    def check_no_action_complete(self, pps_uss_db, lims_id, library_name):
        # get the sow id for the library
        sow_id = self.my_udf.getSowIdFromLibraryUDFs(library_name)

        # check the status, tla and tlac for the sow
        field_list = 'CURRENT_STATUS_ID, TARGET_LOGICAL_AMOUNT, TOTAL_LOGICAL_AMOUNT_COMPLETED'
        orig_sow_info = pps_uss_db.get_columns_from_single_row_single_table(
            field_list, 'uss.dt_sow_item', 'sow_item_id', str(sow_id))
        orig_status_id = orig_sow_info['CURRENT_STATUS_ID']
        orig_tla = orig_sow_info['TARGET_LOGICAL_AMOUNT']
        orig_tla_completed = orig_sow_info['TOTAL_LOGICAL_AMOUNT_COMPLETED']

        sow_status_error_msg = 'Error - sow status did not get updated to Awaiting QA/QC prior to testing'
        assert_that(orig_status_id, description=sow_status_error_msg).is_equal_to(SowStatus.AWAITING_QA_QC.value)

        new_tla = 111.1
        tla_completed = 11.3

        # # make sure that the new tla value is different from the original tla, otherwise we can't
        # # tell if the field really got updated
        # if new_tla == orig_tla:
        #     new_tla += Decimal(5)

        # create the submission json
        submission_json = self.submission_json_creation(lims_id, library_name)
        submission_json["rqc-rework-info"][0]["sow-item-complete"] = "Complete"
        submission_json["rqc-rework-info"][0]["targeted-logical-amount"] = new_tla
        submission_json["rqc-rework-info"][0]["total-logical-amount-completed"] = tla_completed

        # post to rqc-rework-action with no lab action
        self.post_to_rqc_service(self.ACTION_URL, submission_json, self.ACTION_SUCCESS_STATUS)

        # if post succeeded, check that the tla and tlac has been updated
        updated_sow_info = pps_uss_db.get_columns_from_single_row_single_table(field_list,
                                                                  'uss.dt_sow_item',
                                                                  'sow_item_id',
                                                                  str(sow_id))
        updated_status_id = updated_sow_info['CURRENT_STATUS_ID']
        updated_tla = updated_sow_info['TARGET_LOGICAL_AMOUNT']
        updated_tla_completed = updated_sow_info['TOTAL_LOGICAL_AMOUNT_COMPLETED']

        # the postgres db returns Decimals, so need to convert some floats below
        assert_that(updated_status_id, description='sow status was not updated correctly').\
            is_equal_to(SowStatus.COMPLETE.value)
        assert_that(updated_tla, description='tla was not updated correctly').is_close_to(new_tla, 0.1)
        assert_that(updated_tla_completed, description='tlac was not updated correctly').\
            is_close_to(orig_tla_completed + Decimal(tla_completed), Decimal(0.1))

    def check_no_action_needs_attention_and_abandon(self, pps_uss_db, lims_id, library_name):
        # get the sow id for the library
        sow_id = self.my_udf.getSowIdFromLibraryUDFs(library_name)

        # get the sample id from the sow id
        sample_id = pps_uss_db.get_sample_from_sow(sow_id)

        # create the submission json
        submission_json = self.submission_json_creation(lims_id, library_name)
        submission_json["rqc-rework-info"][0]["abandon-sample"] = "Y"
        submission_json["rqc-rework-info"][0]["abandon-library"] = "Y"
        submission_json["rqc-rework-info"][0]["sow-item-complete"] = "Needs Attention"

        # post to rqc-rework-action with no lab action
        self.post_to_rqc_service(self.ACTION_URL, submission_json, self.ACTION_SUCCESS_STATUS)

        # check that the sow and sample status have been updated
        # and the library and sample artifacts are in the Abandon queue in Clarity
        updated_sow_status = pps_uss_db.get_status('sow', sow_id)
        assert_that(updated_sow_status, description='sow status was not updated correctly').\
            is_equal_to("Needs Attention")

        updated_sample_status = pps_uss_db.get_status('sample', sample_id)
        assert_that(updated_sample_status, description='sample status was not updated correctly').\
            is_equal_to("Abandoned")

        # TODO add verification that library and sample are in abandoned queue

    def check_make_new_library(self, lims_id, library_name):
        # get the scheduled sample artifact
        library_url = self.my_udf.get_first_clarity_url_by_artifact_name(library_name)
        scheduled_sample_url = self.my_udf.getSchSampleArtifactUrl(library_url)

        # verify that the scheduled sample is not currently queued anywhere in Clarity
        self.verify_entity_not_queued(scheduled_sample_url)

        # create the submission json
        submission_json = self.submission_json_creation(lims_id, library_name)

        # post to rqc-rework-action with make library lab action
        url = self.ACTION_URL + "/" + RqcActions.MAKE_NEW_LIBRARY.value
        self.post_to_rqc_service(url, submission_json, self.ACTION_SUCCESS_STATUS)

        # check that the scheduled sample artifact has been queued to the aliquot creation queue
        self.verify_entity_is_queued(scheduled_sample_url, ClarityQueues.ALIQUOT_CREATION.value)

        # TODO add checks for requeue entities to the rest of the tests
        # check that the scheduled sample artifact shows the correct Requeue in its workflows
        self.verify_entity_shows_repost(scheduled_sample_url, ClarityReQueues.QC_REQUEUE_FOR_LIBRARY_CREATION.value)

    def check_qpcr_existing_library(self, lims_id, library_name):
        # verify that the library is not currently queued anywhere in Clarity
        library_url = self.my_udf.get_first_clarity_url_by_artifact_name(library_name)
        self.verify_entity_not_queued(library_url)

        # create the submission json
        submission_json = self.submission_json_creation(lims_id, library_name)

        # post to rqc-rework-action with qpcr existing library action
        url = self.ACTION_URL + "/" + RqcActions.QPCR_EXISTING_LIBRARY.value
        self.post_to_rqc_service(url, submission_json, self.ACTION_SUCCESS_STATUS)

        # check that the library artifact has been queued to the qPCR queue in Clarity
        self.verify_entity_is_queued(library_url, ClarityQueues.QPCR.value)
        self.verify_entity_shows_repost(library_url, ClarityReQueues.QC_REQUEUE_FOR_QPCR.value)

    def check_use_library_in_new_pool(self, lims_id, library_name):
        # verify that the library is not currently queued anywhere in Clarity
        library_url = self.my_udf.get_first_clarity_url_by_artifact_name(library_name)
        self.verify_entity_not_queued(library_url)

        # create the submission json
        submission_json = self.submission_json_creation(lims_id, library_name)

        # post to rqc-rework-action with qpcr existing library action
        url = self.ACTION_URL + "/" + RqcActions.USE_LIBRARY_IN_NEW_POOL.value
        self.post_to_rqc_service(url, submission_json, self.ACTION_SUCCESS_STATUS)

        # check that the library artifact has been queued to the pooling queue in Clarity
        self.verify_entity_is_queued(library_url, ClarityQueues.POOLING.value)
        self.verify_entity_shows_repost(library_url, ClarityReQueues.QC_REQUEUE_FOR_POOL_CREATION.value)

    def check_sequence_existing_library(self, lims_id, library_name):
        # verify that the library is not currently queued anywhere in Clarity
        library_url = self.my_udf.get_first_clarity_url_by_artifact_name(library_name)

        orig_queue_list = self.my_udf.getWorkflowsFromArtifacts(library_url, "QUEUED")
        assert len(orig_queue_list) == 0

        # create the submission json
        submission_json = copy.deepcopy(self.my_json.rqc_rework_info)
        submission_json["rqc-rework-info"][0]["physical-run-unit-id"] = lims_id
        submission_json["rqc-rework-info"][0]["library-name"] = library_name

        # post to rqc-rework-action with sequence existing library action
        url = self.ACTION_URL + "/" + RqcActions.SEQUENCE_EXISTING_LIBRARY.value
        self.post_to_rqc_service(url, submission_json, self.ACTION_SUCCESS_STATUS)

        # check that the library artifact has been queued to the sequencing queue in Clarity
        self.verify_entity_is_queued(library_url, ClarityQueues.ILLUMINA_SEQUENCING.value)
        self.verify_entity_shows_repost(library_url, ClarityReQueues.QC_REQUEUE_FOR_SEQUENCING.value)

    def check_reschedule_illumina_pool(self, lims_id, pool_name, library_name):
        # verify that the pool is not currently queued anywhere in Clarity
        pool_url = self.my_udf.get_first_clarity_url_by_artifact_name(pool_name)

        # Need to check the pool here, not library!
        self.verify_entity_not_queued(pool_url)

        # create the submission json
        # use the library name here, not the pool name (required by rqc ws)
        submission_json = self.submission_json_creation(lims_id, library_name)

        # post to rqc-rework-action
        url = self.ACTION_URL + "/" + RqcActions.RESCHEDULE_POOL.value
        self.post_to_rqc_service(url, submission_json, self.ACTION_SUCCESS_STATUS)

        # check that the pool artifact has been queued to the sequencing queue in Clarity
        self.verify_entity_is_queued(pool_url, ClarityQueues.ILLUMINA_SEQUENCING.value)
        self.verify_entity_shows_repost(pool_url, ClarityReQueues.QC_REQUEUE_FOR_SEQUENCING.value)

    # HELPER FUNCTIONS ###########################################################
    def submission_json_creation(self, lims_id, library_name):
        submission_json = copy.deepcopy(self.my_json.rqc_rework_info)
        submission_json["submitted-by"] = self.user_id
        submission_json["rqc-rework-info"][0]["physical-run-unit-id"] = lims_id
        submission_json["rqc-rework-info"][0]["library-name"] = library_name
        return submission_json

    @staticmethod
    def validate_allowed_actions_list(ws_resp, allowed_lab_actions):
        # get the allowed actions list from the response
        actions_list = ws_resp["rqc-rework-actions"][0]["allowed-actions"]

        # check that every action in the response is in the allowed actions list
        for action in actions_list:
            assert_that(allowed_lab_actions).contains(action["lab-action"])

        # check that the length of the response actions is the same as the allowed actions
        # this verifies that there are no extra actions in the response that are not in our allowed list
        assert_that(len(actions_list)).is_equal_to(len(allowed_lab_actions))

    def verify_entity_not_queued(self, entity_url):
        orig_queue_list = self.my_udf.getWorkflowsFromArtifacts(entity_url, "QUEUED")
        assert_that(len(orig_queue_list), description=f'{entity_url} should not be queued prior to RQC call'). \
            is_equal_to(0)

    def verify_entity_is_queued(self, entity_url, queue):
        new_queue_list = self.my_udf.getWorkflowsFromArtifacts(entity_url, "QUEUED")
        assert_that(len(new_queue_list), description=f'{entity_url} was not requeued').is_equal_to(1)
        assert_that(new_queue_list[0], description=f'{entity_url} was not requeued to {queue}'). \
            is_equal_to(queue)

    def verify_entity_shows_repost(self, entity_url, requeue):
        completed_workflow_list = self.my_udf.getWorkflowsFromArtifacts(entity_url, "COMPLETE")
        assert_that(completed_workflow_list, description=f'{entity_url} does not show a post to {requeue}'). \
            contains(requeue)
