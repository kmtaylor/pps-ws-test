#!/usr/bin/env python
"""this file contains the getter/setter methods for database"""
# -*- coding: utf-8 -*-
__author__ = 'Kristen'

import psycopg2
import psycopg2.extras
from Utils.open_yaml import open_yaml


# getter and setter methods
#     connect  - connects to a database
#     doQuery  - will run the query
#     get:
#     getStatusCV  - gets the text value of the current status id
#     getDBAttribute - get any one attribute from table
#     getDBAttributeList - get a list of attributes from table
#     getStatus -
#     put
#     update   - runs statement and commits

class DataBaseToolsPostgres:
    """this class gets data and updates data from the db"""
    def __init__(self, db='dw'):
        """ initializes stuff"""
        dict_config = open_yaml()

        if db == 'dw':
            self.pg_host = dict_config['pg_host']
            self.pg_db_name = dict_config['pg_db_name']
            self.pg_port = dict_config['pg_port']
            self.pg_user = dict_config['pg_user']
            self.pg_pwd = dict_config['pg_pwd']
        elif db == 'pps':
            self.pg_host = dict_config['pg_pps_host']
            self.pg_db_name = dict_config['pg_pps_db_name']
            self.pg_port = dict_config['pg_pps_port']
            self.pg_user = dict_config['pg_pps_user']
            self.pg_pwd = dict_config['pg_pps_pwd']
        else:
            raise ValueError('DataBaseToolsPostgres was called with value other than dw or pps')

        print("db connection info: {} {} {}".format(self.pg_host, self.pg_port, self.pg_db_name))
        self.error_cnt = 0
        self.cursor = ""

    # ----------------------------------------------------------------------------------------
    #  this connects to the database requested
    # inputs:
    #       database - 'prod' to connect to genprd1,  everything else will connect to genint1
    def connect(self):
        """ makes connection to the desired database"""
        self.error_cnt = 0

        # connect to database
        db = None
        try:
            db = psycopg2.connect(host=self.pg_host, database=self.pg_db_name, user=self.pg_user,
                                  password=self.pg_pwd, port=self.pg_port, cursor_factory=psycopg2.extras.DictCursor)
            db.autocommit = True
        except psycopg2.OperationalError as err:
            print("- Postgres error: %s" % err)
        except:
            print("- Unknown error")

        if not db:
            print("Error - cannot connect to DB!")

        self.cursor = db.cursor()
        return db

    # ----------------------------------------------------------------------------------------
    #  this queries the database and returns the value of the field requested.
    #  Only single value. search is '='
    # inputs:
    #       field - name of the database table field that you want to get data from,
    #               i.e. SEQUENCING_PROJECT_NAME
    #       table - name of database table
    #       key - the field name for the key to search, i.e. SAMPLE_ID
    #       value - the value of the key to search the database   (method uses =)
    # example:  if you want the sequencing project name from SP table use:
    #               get_database_attribute("SEQUENCING_PROJECT_NAME","uss.dt_sequencing_project",
    #               "sequencing_project_id","1138427") ->  select SEQUENCING_PROJECT_NAME from uss.dt_sequencing_project
    #               where sequencing_project_id=1138427
    def get_database_attribute(self, dbfield, dbtable, dbkey, dbvalue):
        "gets one row of data"
        query = 'select ' + dbfield + ' from ' + dbtable + ' where ' + dbkey + ' in ' + '(' + dbvalue + ')'
        self.cursor.execute(query)
        for row in self.cursor:
            attr = str(row[0])
        return attr

    # ----------------------------------------------------------------------------------------
    #  this queries the database and returns the values of the columns requested from a single row
    #  Only single row. search is '='
    # inputs:
    #       dbfields - a comma separated string containing the names of the database columns
    #                that you want to get data from,
    #                like 'CURRENT_STATUS_ID, TARGET_LOGICAL_AMOUNT, TOTAL_LOGICAL_AMOUNT_COMPLETED'
    #       table - name of database table
    #       key - the field name for the key to search, i.e. SOW_ID
    #       value - the value of the key to search the database   (method uses =)
    # example:  if you want the current status and tla from sow table use:
    #               get_database_attribute("CURRENT_STATUS_ID, TARGET_LOGICAL_AMOUNT","uss.dt_sow_item",
    #               "sow_item_id","386659") ->  select CURRENT_STATUS_ID, TARGET_LOGICAL_AMOUNT
    #               from uss.dt_sow_item
    #               where sow_item_id=386659
    def get_columns_from_single_row_single_table(self, dbfields, dbtable, dbkey, dbvalue):
        "gets one row of data from a single table"
        query = 'select ' + dbfields + ' from ' + dbtable + ' where ' + dbkey + ' in ' + '(' + dbvalue + ')'
        self.cursor.execute(query)
        row = self.cursor.fetchone()

        attr_dict = {}

        # make a list from the dbfields string
        field_list = dbfields.split(", ")

        # add elements to the attr_dict, the keys will be the items in dbfields, which are the db column names
        # and the values will be the value in the column returned by the query
        # so it's something like:
        #    {"sequencing_product_id": 1, "active": "Y"}
        for count, field in enumerate(field_list):
            attr_dict[field] = row[count]

        return attr_dict
    # ----------------------------------------------------------------------------------------
    #  this querys the data base and returns the value(s) of the field requested as a list.
    #  Multiple values may be returned. search is '='
    # inputs:
    #       field - name of the database table field that you want to get data from,
    #               i.e. SEQUENCING_PROJECT_NAME
    #       table - name of database table
    #       key - the field name for the key to search, i.e. SAMPLE_ID
    #       value - the value of the key to search the database   (method uses =)
    # outputs:  returns the 1st object (should only be one) of each row of query result
    #           (i.e. find all sow_items with sp=x)

    def get_database_attribute_list(self, select_field, dbtable, dbkey, dbvalue):
        """ gets all attibutes of selected field"""
        query = 'select ' + select_field + ' from ' + dbtable + \
                ' where ' + dbkey + ' in ' + '(' + dbvalue + ')'
        # print (query)
        self.cursor.execute(query)
        attr_list = []
        for row in self.cursor:
            attr = str(row[0])
            attr_list.append(str(attr))
        return attr_list

    # ----------------------------------------------------------------------------------------
    #  this gets the status from the cv table that matches the current_status_id
    #  of the table in question
    # inputs:
    #       type - the type of status: "sample", "sp", "sow", "fd","ap", "at"
    #       id - the id of the sample, sp, sow, fd, ap or at (i.e. the sample_id value)
    # outputs:  returns the status (from cv table of the id requested)
    #
    #  This is an example of what the sql statement would look like when formed by this method:
    #       select
    #       cv.status
    #       from uss.dt_sequencing_project t
    #       LEFT  JOIN uss.DT_SEQ_PROJECT_STATUS_CV cv ON cv.STATUS_ID = t.CURRENT_STATUS_ID
    #       where t.SEQUENCING_PROJECT_ID = 1047857;
    def get_status(self, entity, entity_id):
        """ returns the text status of entity"""
        status = ''
        table_name = ""
        field_key = ""
        cv_table = ""
        if entity == 'sp':
            table_name = 'uss.dt_sequencing_project'
            cv_table = 'uss.DT_SEQ_PROJECT_STATUS_CV'
            field_key = 'SEQUENCING_PROJECT_ID'
        elif entity == 'sample':
            table_name = 'uss.dt_sample'
            cv_table = 'uss.dt_sample_status_cv'
            field_key = 'SAMPLE_ID'
        elif entity == 'sow':
            table_name = 'uss.dt_sow_item'
            cv_table = 'uss.dt_sow_item_status_cv'
            field_key = 'sow_item_id'
        elif entity == 'fd':
            table_name = 'uss.dt_final_deliv_project'
            cv_table = 'uss.DT_FINAL_DELIV_PROJ_STATUS_CV'
            field_key = 'final_deliv_project_id'
        elif entity == 'ap':
            table_name = 'uss.DT_ANALYSIS_PROJECT'
            cv_table = 'uss.DT_ANALYSIS_PROJECT_STATUS_CV'
            field_key = 'ANALYSIS_PROJECT_ID'
        elif type == 'at':
            table_name = 'uss.DT_ANALYSIS_TASK'
            cv_table = 'uss.DT_ANALYSIS_TASK_STATUS_CV'
            field_key = 'ANALYSIS_TASK_ID'

        query = 'select cv.status from ' + table_name + ' t LEFT JOIN ' + cv_table + \
                ' cv ON cv.STATUS_ID = t.CURRENT_STATUS_ID where t.' + \
                field_key + ' = ' + '(' + str(entity_id) + ')'
        self.cursor.execute(query)
        for row in self.cursor:
            status = str(row[0])
        return status

    # ----------------------------------------------------------------------------------------
    #  simply runs the query and returns the single value found
    # inputs:
    #       query - string that has full query command
    # outputs:
    #       attr - the result of the query (a single attribute request)
    # returns 1st accourance

    def do_query(self, query):
        """does query"""
        self.cursor.execute(query)
        attr = ''
        for row in self.cursor:
            attr = str(row[0])
        return attr

    # ----------------------------------------------------------------------------------------
    #  runs the query and returns the number of rows
    # inputs:
    #       query - string that has full query
    # outputs:
    #       count  - the result of the query (a single attribute request)

    def do_query_get_all_rows(self, query):  # returns list
        """gets all rows from query"""
        table_list = []
        self.cursor.execute(query)
        i = 0
        # print (query)
        for row in self.cursor:
            attr = str(row[0])
            table_list.append(attr)
            i += 1
        return table_list

    def do_query_get_rows_as_json(self, query):
        column_names, data_rows = self.do_query_get_all_rows_cols(query)
        db_list = []
        for row in data_rows:
            db_dict = {}
            for index, column in enumerate(column_names):
                db_dict[column] = row[index]
            db_list.append(db_dict)
        return db_list

    def do_query_get_all_rows_cols(self, query):
        """
        Gets db info for any kind of query
        :param query: a string containing the sql for the query to perform
        :return: a list of tuples, the tuples contains the values from each row of the table, like (1, "name", "Y")
        """

        data_rows = []

        self.cursor.execute(query)
        # the line below is a little different in oracle vs postgres
        column_names = [desc.name for desc in self.cursor.description]
        for row in self.cursor:
            data_rows.append(row)
        return column_names, data_rows

    # ----------------------------------------------------------------------------------------
    #  do_update
    #  simply runs a update statement
    # inputs:
    #       statement - string that has full  update statement
    def do_update(self, statement):
        """runs a update statement"""
        self.cursor.execute(statement)
        self.cursor.execute("commit")

    # ----------------------------------------------------------------------------------------
    #  update_db_attibute
    #  this will update the data base
    # inputs:

    #       db_table - name of database table
    #       db_field_dict - a dictionary of field_names:newvalue that needs to be updated
    #                   ([current_status_id:1,status_comments:"itssupp-xxx"])
    #       db_key -  update the records using this field name for the key to search,
    #              i.e. SAMPLE_ID
    #       db_value - update all the records that have these values for the key
    #               (i.e   sequencing_project_id in    (111,222,333))
    # example:  if you want to update the status id and the status comments
    # for sequencing project id = 123 and 456:
    #               updateDBattibute("uss.dt_sequencing_project",[],
    #               "sequencing_project_id",["123","456"])
    def update_db_attibute(self, db_table, db_field_dict, db_key, db_value):
        """ update one attribute of database table"""
        attributes_to_be_updated = ""

        for field_name, field_value in db_field_dict.items():
            attributes_to_be_updated += field_name + "=" + field_value + ","

        attributes_to_be_updated = attributes_to_be_updated[0:-1]  # removes the trailing ","
        statement = 'update ' + db_table + ' set ' + attributes_to_be_updated + \
                    ' where ' + db_key + ' in ' + '(' + db_value + ')'

        self.cursor.execute(statement)
        self.cursor.execute("commit")

    def get_seq_strats_for_seq_prod(self, seq_prod_id):
        def_seq_strat = 0
        alt_strat_list = []
        query = f"SELECT allowed_seq_strat_id, default_strat_for_product " \
                f"FROM pc.dt_m2m_seq_prod_strats " \
                f"where sequencing_product_id = {seq_prod_id} " \
                f"and active = 'Y'"
        column_names, seq_strat_rows = self.do_query_get_all_rows_cols(query)
        for row in seq_strat_rows:
            # if this is the default sequencing strategy, then the value in row[1] will be Y
            if row[1] == 'Y':
                def_seq_strat = int(row[0])
            else:
                alt_strat_list.append(int(row[0]))
        return def_seq_strat, alt_strat_list

    def update_sow_status_by_spid(self, spids, status_id):
        """
        update status of all sows related to a list of spids to the given status id
        :param spids: a list of spids
        :param status_id: the status id that the sows should be updated to
        :return:
        """
        # the spids are in a list, convert to a string without the brackets
        spids_str = str(spids)[1:-1]

        update_statement = f"update uss.dt_sow_item set current_status_id = {status_id}, " \
                           f"status_comments = 'updated by QA team automated test for RQC web service', " \
                           f"status_date = CURRENT_TIMESTAMP " \
                           f"where sequencing_project_id in ({spids_str}) "

        print(update_statement)
        self.do_update(update_statement)

    def get_sample_from_sow(self, sow_id):
        """
        This function is making the assumption that any give sow id will be associated with
        only one sample id
        :param sow_id:
        :return: sample id associated with that sow id
        """
        query = f"SELECT sample_id FROM uss.dt_m2m_samplesowitem where sow_item_id = {sow_id}"
        results = self.do_query_get_all_rows(query)

        # fail if the query returns multiple sample ids
        assert len(results) == 1

        return int(results[0])


def minitest():
    spstuff = DataBaseToolsPostgres()
    spstuff.connect()
    ans = spstuff.get_database_attribute(
        "SEQUENCING_PROJECT_NAME", "uss.dt_sequencing_project", "sequencing_project_id", "1138427")
    print(ans)
    print('...')
    sow_id = '412041'
    field_list = 'CURRENT_STATUS_ID, TARGET_LOGICAL_AMOUNT, TOTAL_LOGICAL_AMOUNT_COMPLETED'
    attr_dict = orig_sow_info = spstuff.get_columns_from_single_row_single_table(field_list, 'uss.dt_sow_item', 'sow_item_id', str(sow_id))
    print(attr_dict)

    ATTRIBUTE = spstuff.get_database_attribute_list('*', 'uss.dt_sow_item',
                                                    'SEQUENCING_PROJECT_ID', '1138427')
    print(ATTRIBUTE)

    SP = 1138427
    print("testing get statuses of different items")
    STATUS = spstuff.get_status('sp', SP)
    print('sp status=', STATUS)
