# Standard library imports...
from unittest import TestCase
from unittest.mock import patch, Mock
import json
import requests

# Local imports...
import Our_UnitTests.data_gen_mock_data as mock_data
import Utils.data_gen_tools
from Utils.Analyte import Analyte
from Utils.clarity_constants import ClarityQueues, Container


class TestAnalyte(TestCase):

    def setUp(self):
        """setup"""
        self.data = Utils.data_gen_tools.DataGenTools()

    @patch.object(
        requests,  # object whose attribute I want to patch
        'post',  # the attribute name I want to patch
        return_value=(Mock(status_code=201, text=mock_data.JSON_THREE_TUBE_SAMPLES))
        # replacement for the patched attribute
    )
    def test_get_all_lims_ids(self, mock_requests):
        analytes = self.data.create_samples(3, Container.TUBE, 1)
        lims_ids = Analyte.get_all_lims_ids(analytes)
        self.assertEqual(3, len(lims_ids))
        data = json.loads(mock_data.JSON_THREE_TUBE_SAMPLES)
        self.assertTrue(all(item in lims_ids for item in data['data-generation-request']['analyte-lims-ids']))

    @patch.object(
        requests,  # object whose attribute I want to patch
        'post',  # the attribute name I want to patch
        return_value=(Mock(status_code=201, text=mock_data.JSON_THREE_TUBE_SAMPLES))
    )
    def test_get_all_names(self, mock_requests):
        analytes = self.data.create_samples(3, Container.TUBE, 1)
        names = Analyte.get_all_names(analytes)
        self.assertEqual(3, len(names))
        # the names of analytes are strings, so convert the THREE_TUBES_SAMPLES_IDS
        # to strings before comparing with the names
        string_list = [str(id) for id in mock_data.THREE_TUBES_SAMPLES_IDS]
        self.assertTrue(all(item in names for item in string_list))

    @patch.object(
        requests,  # object whose attribute I want to patch
        'post',  # the attribute name I want to patch
        return_value=(Mock(status_code=201, text=mock_data.JSON_THREE_TUBE_SAMPLES))
    )
    def test_get_all_sample_ids(self, mock_requests):
        analytes = self.data.create_samples(3, Container.TUBE, 1)
        sample_ids = Analyte.get_all_sample_ids(analytes)
        self.assertEqual(3, len(sample_ids))
        self.assertTrue(all(item in sample_ids for item in mock_data.THREE_TUBES_SAMPLES_IDS))

    @patch.object(
        requests,  # object whose attribute I want to patch
        'post',  # the attribute name I want to patch
        return_value=(Mock(status_code=201, text=mock_data.JSON_THREE_TUBE_SAMPLES))
        # replacement for the patched attribute
    )
    def test_get_all_sp_ids(self, mock_requests):
        analytes = self.data.create_samples(3, Container.TUBE, 1)
        sp_ids = Analyte.get_all_sp_ids(analytes)
        self.assertEqual(3, len(sp_ids))
        data = json.loads(mock_data.JSON_THREE_TUBE_SAMPLES)
        self.assertTrue(all(item in sp_ids for item in data['sp-ids']))

    @patch.object(
        requests,  # object whose attribute I want to patch
        'post',  # the attribute name I want to patch
        return_value=(Mock(status_code=201, text=mock_data.JSON_POOL_10))
        # replacement for the patched attribute
    )
    def test_get_all_sp_ids(self, mock_requests):
        analytes = self.data.create_samples(10, Container.TUBE, 1, ClarityQueues.RQC)
        sp_ids = Analyte.get_all_sp_ids(analytes)
        sample_ids = Analyte.get_all_sample_ids(analytes)
        self.assertEqual(1, len(analytes))
        self.assertEqual(10, len(sp_ids))
        self.assertEqual(10, len(sample_ids))

    @patch.object(
        requests,  # object whose attribute I want to patch
        'post',  # the attribute name I want to patch
        return_value=(Mock(status_code=201, text=mock_data.JSON_POOL_10_SINGLE_SPID))
        # replacement for the patched attribute
    )
    def test_get_all_sp_ids_2(self, mock_requests):
        analytes = self.data.create_samples(72, Container.TUBE, 1, ClarityQueues.RQC)
        sp_ids = Analyte.get_all_sp_ids(analytes)
        sample_ids = Analyte.get_all_sample_ids(analytes)
        self.assertEqual(10, len(analytes))
        self.assertEqual(1, len(sp_ids))
        self.assertEqual(10, len(sample_ids))
