#!/usr/bin/env python
"""this file contains the getter/setter methods for database"""
# -*- coding: utf-8 -*-
__author__ = 'Becky'

# import os

import cx_Oracle
from Utils.open_yaml import open_yaml


# getter and setter methods
#     connect  - connects to a database
#     doQuery  - will run the query
#     get:
#     getStatusCV  - gets the text value of the current status id
#     getDBAttribute - get any one attribute from table
#     getDBAttributeList - get a list of attributes from table
#     getStatus -
#     put
#     update   - runs statement and commits

class DataBaseTools:
    """this class gets data and updates data from the db"""

    def __init__(self):
        """ initializes stuff"""
        dict_config = open_yaml()
        # TODO change all usages of int_db to db_server_name
        self.int_db = dict_config['db_server_name']
        self.db_user = dict_config['db_user_name']
        self.db_passw = dict_config['db_password']
        self.error_cnt = 0
        self.cursor = ""

    # ----------------------------------------------------------------------------------------
    #  this connects to the data base requested
    # inputs:
    #       database - 'prod' to connect to genprd1,  everything else will connect to genint1
    def connect(self, database):
        """ makes connection to the desired database"""
        self.error_cnt = 0
        database = cx_Oracle.connect(self.db_user, self.db_passw, database)
        # print("connected to database: ", database)
        self.cursor = database.cursor()

    # ----------------------------------------------------------------------------------------
    #  this querys the data base and returns the value of the field requested.
    #  Only single value. search is '='
    # inputs:
    #       field - name of the database table field that you want to get data from,
    #               i.e. SEQUENCING_PROJECT_NAME
    #       table - name of database table
    #       key - the field name for the key to search, i.e. SAMPLE_ID
    #       value - the value of the key to search the database   (method uses =)
    # example:  if you want the sequencing project name from SP table use:
    #               get_database_attribute("SEQUENCING_PROJECT_NAME","uss.dt_sequencing_project",
    #               "sequencing_project_id","1138427") ->  select SEQUENCING_PROJECT_NAME from uss.dt_sequencing_project
    #               where sequencing_project_id=1138427
    def get_database_attribute(self, dbfield, dbtable, dbkey, dbvalue):
        "gets one row of data"
        query = 'select ' + dbfield + ' from ' + dbtable + ' where ' + dbkey + ' in ' + dbvalue
        self.cursor.execute(query)
        for row in self.cursor:
            attr = str(row[0])

        return attr

    # ----------------------------------------------------------------------------------------
    #  this queries the database and returns the values of the columns requested from a single row
    #  Only single row. search is '='
    # inputs:
    #       dbfields - a comma separated string containing the names of the database columns
    #                that you want to get data from,
    #                like 'CURRENT_STATUS_ID, TARGET_LOGICAL_AMOUNT, TOTAL_LOGICAL_AMOUNT_COMPLETED'
    #       table - name of database table
    #       key - the field name for the key to search, i.e. SOW_ID
    #       value - the value of the key to search the database   (method uses =)
    # example:  if you want the current status and tla from sow table use:
    #               get_database_attribute("CURRENT_STATUS_ID, TARGET_LOGICAL_AMOUNT","uss.dt_sow_item",
    #               "sow_item_id","386659") ->  select CURRENT_STATUS_ID, TARGET_LOGICAL_AMOUNT
    #               from uss.dt_sow_item
    #               where sow_item_id=386659
    def get_columns_from_single_row(self, dbfields, dbtable, dbkey, dbvalue):
        "gets one row of data"
        query = 'select ' + dbfields + ' from ' + dbtable + ' where ' + dbkey + ' in ' + dbvalue
        self.cursor.execute(query)
        row = self.cursor.fetchone()

        # make a list from the dbfields string
        field_list = dbfields.split(", ")
        attr_dict = {}

        for count, field in enumerate(field_list):
            attr_dict[field] = row[count]

        return attr_dict

    # ----------------------------------------------------------------------------------------
    #  this querys the data base and returns the value(s) of the field requested as a list.
    #  Multiple values may be returned. search is '='
    # inputs:
    #       field - name of the database table field that you want to get data from,
    #               i.e. SEQUENCING_PROJECT_NAME
    #       table - name of database table
    #       key - the field name for the key to search, i.e. SAMPLE_ID
    #       value - the value of the key to search the database   (method uses =)
    # outputs:  returns the 1st object (should only be one) of each row of query result
    #           (i.e. find all sow_items with sp=x)

    def get_database_attribute_list(self, select_field, dbtable, dbkey, dbvalue):
        """ gets all attibutes of selected field"""
        query = 'select ' + select_field + ' from ' + dbtable + \
                ' where ' + dbkey + ' in ' + dbvalue
        # print (query)
        self.cursor.execute(query)
        attr_list = []
        for row in self.cursor:
            attr = str(row[0])
            attr_list.append(str(attr))
        return attr_list

    # ----------------------------------------------------------------------------------------
    #  this gets the status from the cv table that matches the current_status_id
    #  of the table in question
    # inputs:
    #       type - the type of status: "sample", "sp", "sow", "fd","ap", "at"
    #       id - the id of the sample, sp, sow, fd, ap or at  (ie. the sample_id value)
    # outputs:  returns the status (from cv table of the id requested)
    #
    #  This is an example of what the sql statement would look like when formed by this method:
    #       select
    #       cv.status
    #       from uss.dt_sequencing_project t
    #       LEFT  JOIN uss.DT_SEQ_PROJECT_STATUS_CV cv ON cv.STATUS_ID = t.CURRENT_STATUS_ID
    #       where t.SEQUENCING_PROJECT_ID = 1047857;
    def get_status(self, entity, entity_id):
        """ returns the text status of entity"""
        status = ''
        table_name = ""
        field_key = ""
        cv_table = ""
        if entity == 'sp':
            table_name = 'uss.dt_sequencing_project'
            cv_table = 'uss.DT_SEQ_PROJECT_STATUS_CV'
            field_key = 'SEQUENCING_PROJECT_ID'
        elif entity == 'sample':
            table_name = 'uss.dt_sample'
            cv_table = 'uss.dt_sample_status_cv'
            field_key = 'SAMPLE_ID'
        elif entity == 'sow':
            table_name = 'uss.dt_sow_item'
            cv_table = 'uss.dt_sow_item_status_cv'
            field_key = 'sow_item_id'
        elif entity == 'fd':
            table_name = 'uss.dt_final_deliv_project'
            cv_table = 'uss.DT_FINAL_DELIV_PROJ_STATUS_CV'
            field_key = 'final_deliv_project_id'
        elif entity == 'ap':
            table_name = 'uss.DT_ANALYSIS_PROJECT'
            cv_table = 'uss.DT_ANALYSIS_PROJECT_STATUS_CV'
            field_key = 'ANALYSIS_PROJECT_ID'
        elif type == 'at':
            table_name = 'uss.DT_ANALYSIS_TASK'
            cv_table = 'uss.DT_ANALYSIS_TASK_STATUS_CV'
            field_key = 'ANALYSIS_TASK_ID'

        query = 'select cv.status from ' + table_name + ' t LEFT JOIN ' + cv_table + \
                ' cv ON cv.STATUS_ID = t.CURRENT_STATUS_ID where t.' + \
                field_key + ' = ' + str(entity_id)
        self.cursor.execute(query)
        for row in self.cursor:
            status = str(row[0])
        return status

    # ----------------------------------------------------------------------------------------
    #  simply runs the query and returns the single value found
    # inputs:
    #       query - string that has full query command
    # outputs:
    #       attr - the result of the query (a single attribute request)
    # returns 1st occurrence

    def do_query(self, query):
        """does query"""
        self.cursor.execute(query)
        attr = ''
        for row in self.cursor:
            attr = str(row[0])
        return attr

    # ----------------------------------------------------------------------------------------
    #  runs the query and returns the number of rows
    # inputs:
    #       query - string that has full query
    # outputs:
    #       count  - the result of the query (a single attribute request)

    def do_query_get_all_rows(self, query):  # returns list
        """gets all rows from query"""
        table_list = []
        self.cursor.execute(query)
        # print (query)
        for row in self.cursor:
            attr = str(row[0])
            table_list.append(attr)
        return table_list

    def do_query_get_rows_as_json(self, query):
        column_names, data_rows = self.do_query_get_all_rows_cols(query)
        db_list = []
        for row in data_rows:
            db_dict = {}
            for index, column in enumerate(column_names):
                db_dict[column] = row[index]
            db_list.append(db_dict)
        return db_list

    def do_query_get_all_rows_cols(self, query):
        """
        Gets db info for any kind of query
        :param query: a string containing the sql for the query to perform
        :return: a list of tuples, the tuples contains the values from each row of the table, like (1, "name", "Y")
        """

        data_rows = []

        self.cursor.execute(query)
        # the line below is a little different in oracle vs postgres
        column_names = [desc[0] for desc in self.cursor.description]
        for row in self.cursor:
            data_rows.append(row)
        return column_names, data_rows

    # ----------------------------------------------------------------------------------------
    #  do_upate
    #  simply runs a update statement
    # inputs:
    #       statement - string that has full  update statement
    def do_update(self, statement):
        """runs a update statement"""
        self.cursor.execute(statement)
        self.cursor.execute("commit")

    # ----------------------------------------------------------------------------------------
    #  update_db_attibute
    #  this will update the data base
    # inputs:

    #       db_table - name of database table
    #       db_field_dict - a dictionary of field_names:newvalue that needs to be updated
    #                   ([current_status_id:1,status_comments:"itssupp-xxx"])
    #       db_key -  update the records using this field name for the key to search,
    #              i.e. SAMPLE_ID
    #       db_value - update all the records that have these values for the key
    #               (i.e   sequencing_project_id in    (111,222,333))
    # example:  if you want to update the status id and the status comments
    # for sequencing project id = 123 and 456:
    #               updateDBattibute("uss.dt_sequencing_project",[],
    #               "sequencing_project_id",["123","456"])
    def update_db_attibute(self, db_table, db_field_dict, db_key, db_value):
        """ update one attribute of database table"""
        attributes_to_be_updated = ""

        for field_name, field_value in db_field_dict.items():
            attributes_to_be_updated += field_name + "=" + field_value + ","

        attributes_to_be_updated = attributes_to_be_updated[0:-1]  # removes the trailing ","
        statement = 'update ' + db_table + ' set ' + attributes_to_be_updated + \
                    ' where ' + db_key + ' in ' + db_value

        self.cursor.execute(statement)
        self.cursor.execute("commit")

    # -----------------------------------------------------
    # deleteSPandChildren
    # set status to delete for SP and children
    # SPs, samples and sows only
    # ------------------------------------------------------
    def deleteSPandChildren(self, spId):
        # get all samples from DB
        query = 'select sam.SAMPLE_ID from uss.dt_sequencing_project sp ' \
                'left join uss.dt_sow_item sow on sp.sequencing_project_id = sow.sequencing_project_id ' \
                'left join uss.dt_m2m_samplesowitem m2sam on sow.sow_item_id = m2sam.sow_item_id ' \
                'left join uss.dt_sample sam on m2sam.sample_id = sam.sample_id ' \
                'where sp.sequencing_project_id in (' + str(spId) + ') and sam.current_status_id not in (16)'

        sampleList = self.do_query_get_all_rows(query)
        sampleSet = set(sampleList)
        self.sampleSet = sampleSet

        # get all sows
        query = 'select sow.SOW_ITEM_ID from uss.dt_sow_item sow ' \
                'where sow.sequencing_project_id in (' + str(
            spId) + ') and sow.current_status_id not in (10)'

        sowList = self.do_query_get_all_rows(query)
        sowSet = set(sowList)
        self.sowSet = sowSet

        # get all sps
        query = 'select sp.sequencing_project_id from uss.dt_sequencing_project sp ' \
                'where sp.sequencing_project_id in (' + str(spId) + ') and sp.current_status_id not in (9)'

        spList = self.do_query_get_all_rows(query)
        spSet = set(spList)
        self.spSet = spSet

        # update database
        self.deleteSamples(sampleSet)
        self.deleteSows(sowSet)
        self.deleteSPs(spSet)

        print("Cleanup: SP(s)= ", spSet, " ,sample(s)= ", sampleSet, " ,sow(s)= ", sowSet,
              ": statuses has been set to 'Deleted'")

    # ---------------------------------------------------------------------------------
    # deleteSamples
    # set status to delete for sample set
    def deleteSamples(self, sampleSet):

        # delete the samples
        if len(sampleSet) == 0:
            print("*** no samples to delete")
            # self.sampleSet.add("No Samples to Delete")
            self.sampleSet = "***None***"

        else:
            y = ",".join(map(str, sampleSet))
            query = "update uss.dt_sample set current_status_id=16, " \
                    "status_comments = 'updated by PPS_Webservice_Verification scripts',status_date = sysdate " \
                    "where sample_id in (" + y + ") and current_status_id not in (16)"
            self.do_update(query)

    # ---------------------------------------------------------------------------------
    # ---------------------------------------------------------------------------------
    # deleteSows
    # set status to delete for sow set
    def deleteSows(self, sowSet):

        # delete the sows
        if len(sowSet) == 0:
            print("*** no sows to delete")
            self.sowSet = "***None***"

        else:
            y = ",".join(map(str, sowSet))
            query = "update uss.dt_sow_item set current_status_id=10, " \
                    "status_comments = 'updated by PPS_Webservice_Verification scripts',status_date = sysdate " \
                    "where sow_item_id in (" + y + ") " \
                                                   "and current_status_id not in (10)"

            self.do_update(query)

    # ---------------------------------------------------------------------------------
    # ---------------------------------------------------------------------------------
    # deleteSPs
    # set status to delete for SP set (must change status to Created(1) before able to delete
    def deleteSPs(self, spSet):

        # delete the sps
        if len(spSet) == 0:
            print("*** no SPs to delete")
            self.spSet = "***None***"

        else:
            y = ",".join(map(str, spSet))
            # change SP status to CREATED before deleteing
            query = "update uss.dt_sequencing_project  set current_status_id=1, " \
                    "status_comments = 'updated by PPS_Webservice_Verification scripts',status_date = sysdate " \
                    "where sequencing_project_id in (" + y + ") "
            self.do_update(query)
            # now delete
            query = "update uss.dt_sequencing_project  set current_status_id=9, " \
                    "status_comments = 'updated by PPS_Webservice_Verification scripts',status_date = sysdate " \
                    "where sequencing_project_id in (" + y + ") "

            self.do_update(query)

    def update_sow_status_by_spid(self, spids, status_id):
        """
        update status of all sows related to a list of spids to the given status id
        :param spids: a list of spids
        :param status_id: the status id that the sows should be updated to
        :return:
        """
        # the spids are in a list, convert to a string without the brackets
        spids_str = str(spids)[1:-1]

        update_statement = f"update uss.dt_sow_item set current_status_id = {status_id}, " \
                           f"status_comments = 'updated by QA team automated test for RQC web service', " \
                           f"status_date = sysdate " \
                           f"where sequencing_project_id in ({spids_str}) "

        print(update_statement)
        self.do_update(update_statement)

    def get_sample_from_sow(self, sow_id):
        """
        This function is making the assumption that any give sow id will be associated with
        only one sample id
        :param sow_id:
        :return: sample id associated with that sow id
        """
        query = f"SELECT sample_id FROM uss.dt_m2m_samplesowitem where sow_item_id = {sow_id}"
        results = self.do_query_get_all_rows(query)

        # fail if the query returns multiple sample ids
        assert len(results) == 1

        return int(results[0])
