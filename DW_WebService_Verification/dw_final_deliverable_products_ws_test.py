
# created 11/1/21

# the WS URLs are :
# https://ws-access-dev.jgi.doe.gov
# https://ws-access-int.jgi.doe.gov
# https://ws-access.jgi.doe.gov


# this test runs the Final Deliverable Products WS and verifies that the data return is as specified
# see
# specification:

# The tests included here are:
#
#     GET dw-pc-final-deliverable-products status=200; response contains an array of json objects of resource attributes
#     GET dw-pc-final-deliverable-products/id# status=200; response contains json object of resource attributes
#     POST,PUT,DELETE status=405

import json

from Utils.open_yaml import open_yaml
from Utils.util_jsonInput import jsonInput
from Utils.http_request_tools import HttpTools
from Utils.util_dbTools_Postgres import DataBaseToolsPostgres
from Utils.wsTools import wsTools


class FinalDeliverableProductsWST:
    def __init__(self):
        self.printOn = True
        self.errorCnt = 0
        dict_config = open_yaml()
        self.wsURL = dict_config['ws_url'] + "dw-pc-final-deliverable-products"
        self.myJson = jsonInput()
        self.myRequests = HttpTools()
        self.myDB = DataBaseToolsPostgres()
        self.myDB.connect()
        self.myWsTools = wsTools()
        self.errorMsgs = []

    # -------------------------------------------------------------------------------------------
    # check_for_required_parameters
    #
    def check_for_required_parameters(self, record):
        # check the base parameters
        self.myWsTools.checkKey(self.errorMsgs, record, 'final-deliverable-product-id', True, int)
        self.myWsTools.checkKey(self.errorMsgs, record, 'final-deliverable-product-name', True, str)
        self.myWsTools.checkKey(self.errorMsgs, record, 'naming-rule', True, dict, True)
        self.myWsTools.checkKey(self.errorMsgs, record['naming-rule'], 'base', True, str)
        self.myWsTools.checkKey(self.errorMsgs, record['naming-rule'], 'rule', True, dict, null_ok=True)
        self.myWsTools.checkKey(self.errorMsgs, record, 'product-search-category-name', True, str)
        self.myWsTools.checkKey(self.errorMsgs, record, 'genome-type', True, dict,)
        self.myWsTools.checkKey(self.errorMsgs, record, 'sequencing-products', True, list, null_ok=True)
        self.myWsTools.checkKey(self.errorMsgs, record, 'analysis-products', True, list, null_ok=True)

        for sp in record['sequencing-products']:
            self.myWsTools.checkKey(self.errorMsgs, sp, 'sequencing-product-id', True, int)
            self.myWsTools.checkKey(self.errorMsgs, sp, 'sequencing-product-name', True, str)
            self.myWsTools.checkKey(self.errorMsgs, sp, 'default-sequencing-strategy-id', True, int)
            self.myWsTools.checkKey(self.errorMsgs, sp, 'auto-schedule-sow-items', True, str)
            self.myWsTools.checkKey(self.errorMsgs, sp, 'alternate-sequencing-strategies', True, list)
            self.myWsTools.checkKey(self.errorMsgs, sp, 'naming-rule', True, dict)
            self.myWsTools.checkKey(self.errorMsgs, sp['naming-rule'], 'base', True, str)
            self.myWsTools.checkKey(self.errorMsgs, sp['naming-rule'], 'rule', True, dict, null_ok=True)
            for strat in sp['alternate-sequencing-strategies']:
                self.myWsTools.checkKey(self.errorMsgs, strat, 'sequencing-strategy-id', False, int)
                self.myWsTools.checkKey(self.errorMsgs, strat, 'sequencing-strategy-name', False, str)

        for ap in record['analysis-products']:
            self.myWsTools.checkKey(self.errorMsgs, ap, 'analysis-product-id', True, int)
            self.myWsTools.checkKey(self.errorMsgs, ap, 'analysis-product-name', True, str)
            self.myWsTools.checkKey(self.errorMsgs, ap, 'ranking', True, int)
            self.myWsTools.checkKey(self.errorMsgs, ap, 'pi-review-required', True, str)
            self.myWsTools.checkKey(self.errorMsgs, ap, 'pi-review-duration', True, int)
            self.myWsTools.checkKey(self.errorMsgs, ap, 'naming-rule', True, dict)
            self.myWsTools.checkKey(self.errorMsgs, ap['naming-rule'], 'base', True, str)
            self.myWsTools.checkKey(self.errorMsgs, ap['naming-rule'], 'rule', True, dict, null_ok=True)
            self.myWsTools.checkKey(self.errorMsgs, ap, 'build-on-create', True, str)
            self.myWsTools.checkKey(self.errorMsgs, ap, 'analysis-task-templates', True, list)

            for att in ap['analysis-task-templates']:
                self.myWsTools.checkKey(self.errorMsgs, att, 'analysis-task-type-id', True, int)
                self.myWsTools.checkKey(self.errorMsgs, att, 'analysis-task-type-name', True, str)
                self.myWsTools.checkKey(self.errorMsgs, att, 'ranking', True, int)
                self.myWsTools.checkKey(self.errorMsgs, att, 'creation-strategy-id', True, int)

    # ------------------------------------------------------------------------------------
    # get_all_records_and_verify_schema
    # run the web service without parameters. It verifies the data in response to be
    # consistent with the contract. Prints a report including errors found
    # inputs: none
    # outputs: none
    def get_all_records_and_verify_schema(self):
        response_data = self.myWsTools.runWS(self.wsURL)

        for record in response_data:
            # print(json.dumps(record, indent=4, sort_keys=True))
            self.check_for_required_parameters(record)

    # ------------------------------------------------------------------------------------
    # get_records_with_parameter(self,parm):
    # run the web service with parameters.
    # expects one records returned in response by WS
    # inputs: parameter  (ie.  lc spec id)
    # outputs: prints error report
    def get_records_with_parameter(self, parm):
        record = self.myWsTools.runWS(self.wsURL, parm)
        # print(record)

        # get the sequencing product info from the db for this id
        # the order of the select items needs to match
        db_query = f"SELECT sequencing_product_id, sequencing_product_name " \
                   f"FROM uss.vw_final_deliv_prod_sequencing " \
                   f"where final_deliv_product_id = {parm};"
        column_names, db_sp_list = self.myDB.do_query_get_all_rows_cols(db_query)
        # print(db_sp_list)
        ws_sp_list = record['sequencing-products']

        # check that each sp id/name associated with this fd in the db is listed in the ws response
        self.compare_db_entries_to_ws_response(db_sp_list, ws_sp_list, 'sequencing-product', parm)

        # get the analysis product info from the db for this id
        db_query = f"SELECT analysis_product_id, analysis_product_name " \
                   f"FROM uss.vw_final_deliv_prod_analysis " \
                   f"where final_deliv_product_id = {parm};"
        column_names, db_ap_list = self.myDB.do_query_get_all_rows_cols(db_query)
        # print(db_ap_list)
        ws_ap_list = record['analysis-products']

        # check that each ap id/name associated with this fd in the db is listed in the ws response
        self.compare_db_entries_to_ws_response(db_ap_list, ws_ap_list, 'analysis-product', parm)

        # for each sp, check that the alternate sequencing strategy info shown by webservice
        # matches that in db
        for sp in record['sequencing-products']:
            seq_prod_id = sp['sequencing-product-id']
            default_strat, alt_strat_list = self.myDB.get_seq_strats_for_seq_prod(seq_prod_id)
            # check that default sequencing strategy is correct
            if not sp['default-sequencing-strategy-id'] == default_strat:
                self.errorMsgs.append(f'ws default seq strat does not match db default seq strat')
            # check if alternate sequencing strategies are displayed by the ws
            for db_alt_strat in alt_strat_list:
                alt_strat_found = False
                for ws_strat in sp['alternate-sequencing-strategies']:
                    if ws_strat['sequencing-strategy-id'] == db_alt_strat:
                        alt_strat_found = True
                        break
                if not alt_strat_found:
                    self.errorMsgs.append(f'alternate sequencing strategy id in database {db_alt_strat} was not '
                                          f'found in ws response for sequencing product {seq_prod_id}')

    def compare_db_entries_to_ws_response(self, db_rows, ws_list, key, parm):
        for row in db_rows:
            ws_contains_info = False
            db_sp_id = int(row[0])  # 0 because the id value is the first item in the query
            db_sp_name = row[1] # 1 because the name value is the second item in the query
            for ws_info in ws_list:
                if ws_info[f'{key}-id'] == db_sp_id and ws_info[f'{key}-name'] == db_sp_name:
                    ws_contains_info = True
                    break

            if not ws_contains_info:
                self.errorMsgs.append(f'database shows spid {db_sp_id} sp name {db_sp_name} '
                                      f'associated with fdid {parm} but the webservice does not')


# ------this is how to run the tests--------------------------
myTest = FinalDeliverableProductsWST()
# print("----------------------------------------------------")
print("TESTING: get all records and verify schema")
myTest.get_all_records_and_verify_schema()

print("----------------------------------------------------")
print("TESTING: with valid <id> parameter.")
myTest.get_records_with_parameter(10)

print("----------------------------------------------------")
print("TESTING: for alt sequencing strategies.")
# this test should be run with an FD prod id that has seq products that have alt seq strategies
# per Dana comment in DW-4950, those FD ids currently are: 2, 51, 58, 59, 60, 61, 77, 82
myTest.get_records_with_parameter(2)

# TODO invalid parameter test
# print("----------------------------------------------------")
# print("TESTING: url with invalid <id> parameter.")

# TODO PUT not allowed
# TODO POST not allowed
# TODO DELETE not allowed

print("----------------------------------------------------")
print("---end---")

if len(myTest.errorMsgs) == 0:
    print("No Errors")
else:
    print("Errors occurred!")
    print("Error messages:")
    print(myTest.errorMsgs)
