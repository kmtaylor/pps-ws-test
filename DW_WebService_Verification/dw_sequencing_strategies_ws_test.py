# created 11/10/21

# the WS URLs are :
# https://ws-access-dev.jgi.doe.gov
# https://ws-access-int.jgi.doe.gov
# https://ws-access.jgi.doe.gov


# this test runs the Sequencing Products WS and verifies that the data return is as specified
# see specification:
# https://docs.jgi.doe.gov/display/DW/Sequencing+Strategy%3A+API+Contract

# The tests included here are:
#
#     GET dw-pc-sequencing-strategies status=200; response contains an array of json objects of resource attributes
#     GET dw-pc-sequencing-strategies/id# status=200; response contains a single json object of resource attributes
#     GET dw-pc-sequencing-strategies?active=true response contains a list of only the active items

import json

from Utils.open_yaml import open_yaml
from Utils.util_jsonInput import jsonInput
from Utils.http_request_tools import HttpTools
from Utils.util_dbTools_Postgres import DataBaseToolsPostgres
from Utils.wsTools import wsTools


class SequencingStrategiesWST:
    def __init__(self):
        self.printOn = True
        self.errorCnt = 0
        dict_config = open_yaml()
        self.wsURL = dict_config['ws_url'] + "dw-pc-sequencing-strategies"
        self.myJson = jsonInput()
        self.myRequests = HttpTools()
        self.myDB = DataBaseToolsPostgres()
        self.myDB.connect()
        self.myWsTools = wsTools()
        self.errorMsgs = []

    # ------------------------------------------------------------------------------------
    # get_all_records_and_verify_schema
    # run the web service without parameters. It verifies the data in response to be
    # consistent with the contract. Prints a report including errors found
    # inputs: none
    # outputs: none
    def get_all_records_and_verify_schema(self):
        response_data = self.myWsTools.runWS(self.wsURL)

        for record in response_data:
            # print(json.dumps(record, indent=4, sort_keys=True))
            self.check_for_required_parameters(record)

            # uncomment the line below if you want to do a db check for every item
            # returned by the web service
            # self.get_records_with_parameter(record['sequencing-product-id'])

    # -------------------------------------------------------------------------------------------
    # check_for_required_parameters
    #
    def check_for_required_parameters(self, record):
        # check the base parameters
        self.myWsTools.checkKey(self.errorMsgs, record, 'sequencing-strategy-id', True, int)
        self.myWsTools.checkKey(self.errorMsgs, record, 'sequencing-strategy-name', True, str)
        self.myWsTools.checkKey(self.errorMsgs, record, 'active', True, bool)
        self.myWsTools.checkKey(self.errorMsgs, record, 'work-flow-id', False, int)
        self.myWsTools.checkKey(self.errorMsgs, record, 'material-type-id', True, int)
        self.myWsTools.checkKey(self.errorMsgs, record, 'material-type-name', True, str)
        self.myWsTools.checkKey(self.errorMsgs, record, 'material-category-name', True, str)
        self.myWsTools.checkKey(self.errorMsgs, record, 'sample-qc-volume-ul', True, int)
        self.myWsTools.checkKey(self.errorMsgs, record, 'sample-mapping-strategy-name', True, str)
        self.myWsTools.checkKey(self.errorMsgs, record, 'sample-grouping-required', True, bool)
        # TODO see if there is a python date type that will work for these two dates types,
        #  otherwise code some checking?
        self.myWsTools.checkKey(self.errorMsgs, record, 'aud-date-created', True, str)
        self.myWsTools.checkKey(self.errorMsgs, record, 'aud-last-modified', True, str)

        # uncomment below to do the db check for every record
        # self.get_records_with_parameter(record['sequencing-strategy-id'])

    # ------------------------------------------------------------------------------------
    # get_records_with_parameter(self,parm):
    # run the web service with parameters.
    # expects one records returned in response by WS
    # inputs: parameter  (ie.  lc spec id)
    # outputs: prints error report
    def get_records_with_parameter(self, parm):
        record = self.myWsTools.runWS(self.wsURL, parm)

        # get the sequencing strategy info from the db for this id
        # the order of the select items needs to match
        db_columns = """strat.sequencing_strategy_id, strat.sequencing_strategy_name, 
                            strat.active, strat.workflow_id, 
                            strat.aud_date_created, strat.aud_last_modified, strat.material_type_id, 
                            map.sample_mapping_strategy, strat.sample_grouping_required, 
                            mt.material_type, mt.material_category, mt.sample_qc_volume_ul 
                        """
        db_query = f"SELECT {db_columns} " \
                   f"FROM pc.dt_sequencing_strategy strat " \
                   f"left join pc.dt_material_type_cv mt on strat.material_type_id = mt.material_type_id " \
                   f"left join pc.dt_sample_mapping_strategy_cv map on " \
                   f"strat.sample_mapping_strategy_id = map.sample_mapping_strategy_id " \
                   f"where strat.sequencing_strategy_id = {parm};"
        db_json_list = self.myDB.do_query_get_rows_as_json(db_query)

        # because our query returns only one row/dictionary, we will assign the first (and only) dict
        # to db_info
        db_info = db_json_list[0]

        # compare values from the ws with values from the db
        json_key_value = 'sequencing-strategy-id'
        self.myWsTools.compare_ws_to_db(self.errorMsgs, json_key_value, record, 'sequencing-strategy-id',
                                        db_info, 'sequencing_strategy_id')
        self.myWsTools.compare_ws_to_db(self.errorMsgs, json_key_value, record, 'sequencing-strategy-name',
                                        db_info, 'sequencing_strategy_name')
        self.myWsTools.compare_ws_to_db(self.errorMsgs, json_key_value, record, 'active', db_info, 'active')
        self.myWsTools.compare_ws_to_db(self.errorMsgs, json_key_value, record, 'material-type-id',
                                        db_info, 'material_type_id')
        self.myWsTools.compare_ws_to_db(self.errorMsgs, json_key_value, record, 'material-type-name',
                                        db_info, 'material_type')
        self.myWsTools.compare_ws_to_db(self.errorMsgs, json_key_value, record, 'material-category-name',
                                        db_info, 'material_category')
        self.myWsTools.compare_ws_to_db(self.errorMsgs, json_key_value, record, 'sample-qc-volume-ul',
                                        db_info, 'sample_qc_volume_ul')
        self.myWsTools.compare_ws_to_db(self.errorMsgs, json_key_value, record, 'sample-mapping-strategy-name',
                                        db_info, 'sample_mapping_strategy')
        # self.myWsTools.compare_ws_to_db(self.errorMsgs, json_key_value, record, 'aud-date-created',
        #                                db_info, 'aud_date_created')
        # self.myWsTools.compare_ws_to_db(self.errorMsgs, json_key_value, record, 'aud-last-modified',
        #                                db_info, 'aud_last_modified')

        # for sample grouping, the db shows values like 'Y|N', the webservice returns true or false
        if ((db_info['sample_grouping_required'] == 'N' and record['sample-grouping-required']) or
            (db_info['sample_grouping_required'] == 'Y' and not record['sample-grouping-required'])):

            err_msg = f"for {json_key_value} {record[json_key_value]} the ws key 'sample-grouping-required' " \
                      f"shows {record['sample-grouping-required']} " \
                      f"while the db record for 'sample_grouping_required' shows {db_info['sample_grouping_required']}"
            self.errorMsgs.append(err_msg)

    def test_active_records(self):
        self.myWsTools.test_active_records(self.errorMsgs, self.wsURL)


# ------this is how to run the tests--------------------------
myTest = SequencingStrategiesWST()
print("----------------------------------------------------")
print("TESTING: get all records and verify schema")
myTest.get_all_records_and_verify_schema()

print("----------------------------------------------------")
print("TESTING: with valid <id> parameters.")
myTest.get_records_with_parameter(1)


print("----------------------------------------------------")
print("TESTING: get all active records")
myTest.test_active_records()
print("----------------------------------------------------")

# TODO invalid parameter test
# print("----------------------------------------------------")
# print("TESTING: url with invalid <id> parameter.")

# TODO PUT not allowed
# TODO POST not allowed
# TODO DELETE not allowed

print("----------------------------------------------------")
print("---end---")

if len(myTest.errorMsgs) == 0:
    print("No Errors")
else:
    print("Errors occurred!")
    print("Error messages:")
    print(myTest.errorMsgs)
