"""
   Tools to aid with creating SPs/samples/sows for testing
"""
import json
from assertpy import assert_that

from Utils.clarity_constants import ClarityQueues, Container
from Utils.data_gen_parser import DataGenParser
from Utils.http_request_tools import HttpTools
from Utils.open_yaml import open_yaml


class DataGenTools:
    """
       Tools to aid with creating SPs/samples/sows for testing
    """
    # Data Generator URL
    DG = 'pps-data-generator'

    def __init__(self):
        """
        Init
        """
        dict_config = open_yaml()
        self.dg_url = dict_config['ws_url'] + self.DG
        self.clarity_server = dict_config['server_name']
        self.user_id = dict_config['user_id']
        self.clarity_user = dict_config['clarity_user']
        self.clarity_pw = dict_config['clarity_password']
        self.my_http = HttpTools()

    def create_samples(self, number, container, spid, destination=ClarityQueues.APPROVE_SHIPPING, dop=None):
        """
            create new FDs, SPs, samples and sows, samples will be in supply chain queue
            :param number: the number of samples to create
            :param container:  container type samples will be created in
            :param spid: the sequencing project id to use in FD/SP/sample creation
            :param destination: the Clarity destination queue
            :return: a list of the library artifacts created by the request
            :exception: if samples can not be created
        """
        print('data gen tools creating samples in %s workflow' % destination)
        print('dg request values: number:%s, container:%s, spid:%s' % (number, container, spid))

        json_submission = self.make_data_gen_json(number, container, spid, destination, dop)
        response = self.my_http.http_post_json(self.dg_url, json_submission)

        parser = DataGenParser(response)

        # in case of error, display this error message
        error_msg = f'Data Generator failed with this message\n{parser.data}'

        # data generator errors can be detected by presence of "status" key in the response
        assert_that(parser.data, description=error_msg).does_not_contain_key('status')

        # old error checking
        # if "status" in parser.data.keys():
        #     print("****  ERROR ****", parser.data)
        #     exit()

        artifacts = parser.get_analytes()
        return artifacts

    def create_illumina_libraries(self, number, container, spid=1):
        """
            create new FDs, SPs, samples and sows and libraries in qPCR queue
            :param number: the number of samples to create
            :param container:  container type samples will be created in
            :param spid: the sequencing project id to use in FD/SP/sample creation
            :return: a list of the library artifacts created by the request
            :exception: if samples can not be created
        """
        print('data gen tools creating illumina libraries in qPCR workflow')
        print('dg request values: number:%s, container:%s, spid:%s' % (number, container, spid))

        json_submission = self.make_data_gen_json(number, container, spid, ClarityQueues.QPCR)
        response = self.my_http.http_post_json(self.dg_url, json_submission)
        parser = DataGenParser(response)
        artifacts = parser.get_analytes()
        return artifacts

    def create_pacbio_libraries(self, number, container, spid=70):
        """
            create new FDs, SPs, samples, sows and libraries in the annealing queue
            :param number: the number of samples to create
            :param container:  container type samples will be created in
            :param spid: the sequencing project id to use in FD/SP/sample creation
            :return: a list of SPIDs and PMO sample IDs created by the request
            :exception: if samples can not be created
        """
        print('data gen tools creating pacbio libraries in annealing workflow')
        print('dg request values: number:%s, container:%s, spid:%s' % (number, container, spid))

        json_submission = self.make_data_gen_json(number, container, spid, ClarityQueues.PB_ANNEALING)
        response = self.my_http.http_post_json(self.dg_url, json_submission)
        parser = DataGenParser(response)
        artifacts = parser.get_analytes()
        return artifacts


    def create_pru_illumina_dop1(self, number, container):
        """
            create new physical run units that have gone through Clarity
            :param number: the number of prus to create
            :param container:  container type samples and libraries will be created in
            :return: a list of Clarity artifacts created by the request
            :exception: if samples or prus can not be created
        """
        destination = ClarityQueues.RQC

        # spid 72 is currently the only non RnD spid that creates samples with dop = 1
        spid = 72
        dop = 1
        return self.create_samples(number, container, spid, destination, dop)

    def create_sequencing_project(self, number, container, spid):
        """
            create new FDs, SPs, samples and sows without submitting sample metadata
            SPs, samples and sows will have status "Awaiting Collaborator Metadata"
            :param number: the number of samples to create
            :param container:  container type samples will be created in
            :param spid: the sequencing project id to use in FD/SP/sample creation
            :return: a list of the SP ids created by the request
            :exception: if entities can not be created
        """
        print('data gen tools creating SPs without sample metadata submission')
        print('dg request values: number:%s, container:%s, spid:%s' % (number, container, spid))

        json_submission = self.make_data_gen_json(number, container, spid, ClarityQueues.SAMPLE_SUBMISSION)
        response = self.my_http.http_post_json(self.dg_url, json_submission)

        parser = DataGenParser(response)

        # in case of error, display this error message
        error_msg = f'Data Generator failed with this message\n{parser.data}'

        # data generator errors can be detected by presence of "status" key in the response
        assert_that(parser.data, description=error_msg).does_not_contain_key('status')

        spids = parser.get_spids()
        return spids

    def make_data_gen_json(self, number, container, spid,
                           destination=ClarityQueues.APPROVE_SHIPPING, dop=None, auto=True):
        """

        :param number: the number of samples to create
        :param container: container type samples will be created in
        :param spid: the sequencing project id to use in FD/SP/sample creation
        :param destination: the Clarity queue that the samples will be created in
        :param dop: the degree of pooling for the sows that will be created
        :param auto: boolean to indicate whether the auto schedule flag should be set to Y
        :return:
        """
        dg_json = {'number-samples': number, 'sequencing-product-id': spid}

        if isinstance(container, Container):
            dg_json['sample-container-type'] = container.value
        else:
            raise Exception("container must be in Container enum")

        dg_json['destination-process-type'] = destination.value

        if dop is not None:
            dg_json['degree-of-pooling'] = dop

        dg_json['sequencing-project-manager-id'] = self.user_id

        if auto:
            dg_json['auto-schedule-sow-items'] = 'Y'

        dg_json['clarity-username'] = self.clarity_user
        dg_json['clarity-password'] = self.clarity_pw

        return json.dumps(dg_json)
