# This script can be used to debug database queries without having to run the whole test
# multiple times, which can be useful if the test is creating Clarity artifacts, and you
# want to skip that step while you are debugging your query

# I'm putting this in the Utils folder because I'm thinking that in Jenkins we might have jobs
# that run all the tests in the PPS_WebService_Verification folder, or the DW_WebService_Verification folder
# and this test should not be run in those cases, so I am putting it in Utils for now

from Utils.util_dbTools import DataBaseTools
from assertpy import assert_that

myDB = DataBaseTools()
myDB.connect(myDB.int_db)

def test_can_edit_sow_of_sample():
    sow_id_string = '418275'

    # verify that database has been updated
    db_query = f"select sow.SOW_ITEM_ID, sow.QC_TYPE_ID, " \
               f"dopcv.DEGREE_OF_POOLING, runcv.RUN_MODE, sow.TARGET_LOGICAL_AMOUNT, " \
               f"laucv.LOGICAL_AMOUNT_UNITS, sow.TOTAL_LOGICAL_AMOUNT_COMPLETED, " \
               f"sow.SM_INSTRUCTIONS as sm_comments, " \
               f"sow.LC_INSTRUCTIONS as lc_comments, " \
               f"sow.SQ_INSTRUCTIONS as sq_comments " \
               f"from uss.dt_sow_item sow " \
               f"left join USS.DT_RUN_MODE_CV runcv on sow.RUN_MODE_ID = runcv.RUN_MODE_ID " \
               f"left join USS.DT_DEGREE_OF_POOLING_CV dopcv on dopcv.DEGREE_OF_POOLING_ID = sow.DEGREE_OF_POOLING_ID " \
               f"left join USS.dt_logical_amount_units_cv laucv on laucv.logical_amount_units_id = sow.logical_amount_units_id " \
               f"where sow.sow_item_id in ({sow_id_string})"
    db_list = myDB.do_query_get_rows_as_json(db_query)
