# Standard library imports...
from unittest import TestCase
from unittest.mock import patch, Mock
import json
import requests

# Local imports...
from Utils.clarity_constants import ClarityQueues, Container
import Utils.data_gen_tools
from Utils.http_request_tools import HttpTools
import Our_UnitTests.data_gen_mock_data as mock_data
from Utils.open_yaml import open_yaml

class TestDataTools(TestCase):

    def setUp(self):
        """setup"""
        self.data = Utils.data_gen_tools.DataGenTools()
        dict_config = open_yaml()
        self.user_id = dict_config['user_id']

    def test_dg_json_tube(self):
        """ test json when container is Tube """
        response_json = self.data.make_data_gen_json(2, Container.TUBE, 1)
        dict = json.loads(response_json)
        self.assertTrue(dict["sample-container-type"] == Container.TUBE.value)

    def test_dg_json_plate(self):
        """ test json when container is Plate """
        response_json = self.data.make_data_gen_json(2, Container.PLATE, 1)
        dict = json.loads(response_json)
        self.assertTrue(dict["sample-container-type"] == Container.PLATE.value)

    def test_dg_bad_container(self):
        """ test that exception is thrown when container not in Container enum """
        self.assertRaises(Exception, self.data.make_data_gen_json, 2, 'pizza', 1)

    def test_dg_json_number_samples(self):
        """ test for correct number of samples """
        response_json = self.data.make_data_gen_json(2, Container.TUBE, 1)
        dict = json.loads(response_json)
        self.assertTrue(dict["number-samples"] == 2)

    def test_dg_json_seq_product(self):
        """ test for correct number of samples """
        response_json = self.data.make_data_gen_json(2, Container.TUBE, 1)
        dict = json.loads(response_json)
        self.assertTrue(dict["sequencing-product-id"] == 1)

    def test_dg_json_defaults(self):
        """ test that json is correct when defaults are used """
        response_json = self.data.make_data_gen_json(2, Container.TUBE, 1)
        dict = json.loads(response_json)
        self.assertTrue(dict["sequencing-project-manager-id"] == self.user_id)
        self.assertTrue(dict["auto-schedule-sow-items"] == 'Y')
        self.assertTrue(dict["destination-process-type"] == ClarityQueues.APPROVE_SHIPPING.value)

    @patch.object(
        HttpTools,  # object whose attribute I want to patch
        'http_post_json',  # the attribute name I want to patch
        return_value=json.loads(mock_data.JSON_THREE_TUBE_SAMPLES)  # replacement for the patched attribute
    )
    def test_dg_create_samples(self, mock_http):
        artifacts = self.data.create_samples(3, Container.TUBE, 1)
        self.assertTrue(len(artifacts) == 3)
        self.assertEqual(artifacts[0].spids[0], 1665293)
        self.assertEqual(artifacts[0].sample_ids[0], 344209)

    @patch.object(
        requests,  # object whose attribute I want to patch
        'post',  # the attribute name I want to patch
        return_value=(Mock(status_code=201, text=mock_data.JSON_THREE_TUBE_SAMPLES))  # replacement for the patched attribute
    )
    def test_create_illumina_libraries(self, mock_requests):
        # TODO - change this test to use a DG response for plates and add more checks that the info is correct
        artifacts = self.data.create_illumina_libraries(3,Container.PLATE)
        self.assertTrue(len(artifacts) == 3)