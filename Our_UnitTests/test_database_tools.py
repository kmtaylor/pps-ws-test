
import requests

from Utils.clarity_constants import ClarityQueues, Container
from Utils import data_gen_tools
import json
from Utils.util_dbTools import DataBaseTools
from Utils.util_dbTools_Postgres import DataBaseToolsPostgres

#from ws_pythontestscripts.Utils.util_udfTools import udfTools

from Utils.util_jsonInput import jsonInput


#----start here.   convert to unittest class
SP = 1138427
SPNAME= ""
STATUS = ""
ERRS = 0
myTest = DataBaseTools()
myTest.connect(myTest.int_db)
#myTest = DataBaseToolsPostgres()
#myTest.connect()

print("testing get status id of sP")
print("testing using sample id = 171730")

ATTRIBUTE = myTest.get_database_attribute('current_status_id', 'uss.dt_sample',
                                           'sample_id', '171730')
print("current status id = ", ATTRIBUTE)

print("testing get status id of sP")
FIELD = 'Current_status_id'
TABLE = 'uss.dt_sequencing_project'
KEY = 'SEQUENCING_PROJECT_ID'
VALUE = '1138427'

ATTRIBUTE = myTest.get_database_attribute(FIELD, TABLE, KEY, VALUE)
print("the value return is " + ATTRIBUTE)

print("testing get sows  of sP")
ATTRIBUTE = myTest.get_database_attribute_list('*', 'uss.dt_sow_item',
                                               'SEQUENCING_PROJECT_ID', '1138427')
print("the value return is " + str(ATTRIBUTE))

print("testing get statuses of different items")
STATUS = myTest.get_status('sp', SP)
print('sp status=', STATUS)

STATUS = myTest.get_status('sample', 190659)
print('sample status=', STATUS)

STATUS = myTest.get_status('sow', 284894)
print('sow status=', STATUS)

STATUS = myTest.get_status('ap', 1138367)
print('ap status=', STATUS)

STATUS = myTest.get_status('fd', 1138365)
print('fd status=', STATUS)

# STATUS = myTest.get_status('at', 175643)
# print('at status=', STATUS)

# get sp name
SPNAME = str(myTest.get_database_attribute("SEQUENCING_PROJECT_NAME",
                                  "uss.dt_sequencing_project",
                                  "sequencing_project_id", "1138427"))
print("the SP name is ", SPNAME)

# update the status and status comments of sp (string values must be incased in single quotes)
field_Dict = {"current_status_id": '2', "status_comments": "'rjr did this'"}

myTest.update_db_attibute("uss.dt_sequencing_project", field_Dict,
                        "sequencing_project_id", "1138427")

print("")
print("---Number of Errors Found = " + str(ERRS) + " ---")