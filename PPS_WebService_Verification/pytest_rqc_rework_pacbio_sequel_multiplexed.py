import pytest

from PPS_WebService_Verification.pytest_rqc_super_class import TestRqcSuper, RqcActions


class TestRqcReworkPacBioSequelMultiplexed(TestRqcSuper):
    """
    tests for RQC Rework functionality for Illumina libraries with DOP > 1
    """

    @pytest.fixture
    def allowed_lab_actions(self):
        return (RqcActions.MAKE_NEW_LIBRARY.value,
                RqcActions.USE_LIBRARY_IN_NEW_POOL.value,
                RqcActions.RESCHEDULE_POOL.value)

    # For pooled libraries a single pru is created for the pool, so each test uses the same pru lims id
    # with a different library name
    def test_inquiry_response(self, sequel_pool_with_status_awaiting_qa_qc, allowed_lab_actions):
        pru_lims_id = sequel_pool_with_status_awaiting_qa_qc[0].lims_id
        library_name = sequel_pool_with_status_awaiting_qa_qc[0].library_names[0]
        self.check_inquiry_response(pru_lims_id, library_name, allowed_lab_actions)

    def test_no_action_complete(self, pps_uss_db, sequel_pool_with_status_awaiting_qa_qc, allowed_lab_actions):
        pru_lims_id = sequel_pool_with_status_awaiting_qa_qc[0].lims_id
        library_name = sequel_pool_with_status_awaiting_qa_qc[0].library_names[1]
        self.check_no_action_complete(pps_uss_db, pru_lims_id, library_name)

    def test_no_action_needs_attention_and_abandon(self, pps_uss_db, sequel_pool_with_status_awaiting_qa_qc, allowed_lab_actions):
        pru_lims_id = sequel_pool_with_status_awaiting_qa_qc[0].lims_id
        library_name = sequel_pool_with_status_awaiting_qa_qc[0].library_names[2]
        self.check_no_action_needs_attention_and_abandon(pps_uss_db, pru_lims_id, library_name)

    def test_make_new_library(self, sequel_pool_with_status_awaiting_qa_qc):
        pru_lims_id = sequel_pool_with_status_awaiting_qa_qc[0].lims_id
        library_name = sequel_pool_with_status_awaiting_qa_qc[0].library_names[3]
        self.check_make_new_library(pru_lims_id, library_name)

    def test_use_library_in_new_pool(self, sequel_pool_with_status_awaiting_qa_qc):
        pru_lims_id = sequel_pool_with_status_awaiting_qa_qc[0].lims_id
        library_name = sequel_pool_with_status_awaiting_qa_qc[0].library_names[5]
        self.check_use_library_in_new_pool(pru_lims_id, library_name)

    # TODO - create check_reschedule_pacbio_pool to check for binding complex in Sequencing Plate Creation queue
    # def test_reschedule_pool(self, sequel_pool_with_status_awaiting_qa_qc):
    #     pru_lims_id = sequel_pool_with_status_awaiting_qa_qc[0].lims_id
    #     pool_name = sequel_pool_with_status_awaiting_qa_qc[0].pool_name
    #     library_name = sequel_pool_with_status_awaiting_qa_qc[0].library_names[6]
    #     self.check_reschedule_pacbio_pool(pru_lims_id, pool_name, library_name)
