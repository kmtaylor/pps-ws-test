"""
   Tools to support yaml file
"""
import yaml
import os

TEST_ENVIRONMENT = 'pps-int'

def open_yaml():
    """
    :return: yaml_dict: dictionary with all entries in the yaml file

    example:
        import Utils.open_yaml
            dict_config = Utils.open_yaml.open_yaml()
            test_value = dict_config['test_value']
    """
    dirname = os.path.dirname(__file__)
    filename = os.path.join(dirname, f'../config/{TEST_ENVIRONMENT}/config.yaml')
    #with open('/Users/KMTaylor/git/pps-ws-test/config/config.yaml', 'rt') as yaml_file:
    with open(filename, 'rt') as yaml_file:
        config = yaml.safe_load(yaml_file.read())

    return config
