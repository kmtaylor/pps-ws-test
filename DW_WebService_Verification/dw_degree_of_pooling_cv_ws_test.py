# created 11/29/21

# this test runs the Sow Items Types CV webservice and verifies that the data return is as specified
# see the generic specification for these CV webservices:
# https://docs.jgi.doe.gov/display/DW/Controlled+Vocabulary+Web+Services

# The tests included here are:
#
#     GET dw-pc-degree-of-pooling-cv status=200; response contains an array of json objects of resource attributes
#     GET dw-pc-degree-of-pooling-cv/id# status=200; response contains a single json object of resource attributes
#     GET dw-pc-degree-of-pooling-cv?active=true response contains a list of only the active items

import json

from DW_WebService_Verification.cv_ws_super_class import SimpleWsSuperClass


class DopCvWST(SimpleWsSuperClass):
    def __init__(self):
        super().__init__()
        self.wsUrl = self.wsUrlbase + 'dw-pc-degree-of-pooling-cv'
        self.id_key = 'degree-of-pooling-id'
        self.name_key = 'degree-of-pooling'
        self.db_id_field = 'degree_of_pooling_id'
        self.db_fields = 'degree_of_pooling_id, degree_of_pooling, active, aud_date_created, aud_last_modified'
        self.db_table = 'pc.dt_degree_of_pooling_cv'

    # ------------------------------------------------------------------------------------
    # get_all_records_and_verify_schema
    # run the web service without parameters. It verifies the data in response to be
    # consistent with the contract. Prints a report including errors found
    # inputs: none
    # outputs: none
    def get_all_records_and_verify_schema(self):
        response_data = self.myWsTools.runWS(self.wsUrl)

        for record in response_data:
            # print(json.dumps(record, indent=4, sort_keys=True))
            # check the base parameters
            # for degree of pooling, the "name_key" degree of pooling, is an int rather than a string
            # therefore, the base class checking doesn't work for this service, so we put it here instead
            self.myWsTools.checkKey(self.errorMsgs, record, self.id_key, True, int)
            self.myWsTools.checkKey(self.errorMsgs, record, self.name_key, True, int)
            self.myWsTools.checkKey(self.errorMsgs, record, 'active', True, bool)
            self.myWsTools.checkKey(self.errorMsgs, record, 'aud-date-created', True, str, null_ok=True)
            self.myWsTools.checkKey(self.errorMsgs, record, 'aud-last-modified', True, str)

            # uncomment the line below if you want to do a db check for every item
            # returned by the web service
            # self.get_records_with_parameter(record[self.id_key])

    # ------------------------------------------------------------------------------------
    # get_records_with_parameter(self,parm):
    # run the web service with parameters.
    # expects one records returned in response by WS
    # inputs: parameter  (ie.  lc spec id)
    # outputs: prints error report
    def get_records_with_parameter(self, parm):
        self.get_records_with_parameter_base(parm)

    def test_active_records(self):
        self.myWsTools.test_active_records(self.errorMsgs, self.wsUrl)


# ------this is how to run the tests--------------------------
myTest = DopCvWST()
print("----------------------------------------------------")
print("TESTING: get all records and verify schema")
myTest.get_all_records_and_verify_schema()

print("----------------------------------------------------")
print("TESTING: with valid <id> parameters.")
myTest.get_records_with_parameter(1)

#
print("----------------------------------------------------")
print("TESTING: get all active records")
myTest.test_active_records()
print("----------------------------------------------------")

# TODO invalid parameter test
# print("----------------------------------------------------")
# print("TESTING: url with invalid <id> parameter.")

# TODO PUT not allowed
# TODO POST not allowed
# TODO DELETE not allowed

print("----------------------------------------------------")
print("---end---")

if len(myTest.errorMsgs) == 0:
    print("No Errors")
else:
    print("Errors occurred!")
    print("Error messages:")
    print(myTest.errorMsgs)
