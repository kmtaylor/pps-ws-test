from enum import Enum


class SowStatus(Enum):
    COMPLETE = 6
    AWAITING_QA_QC = 12
