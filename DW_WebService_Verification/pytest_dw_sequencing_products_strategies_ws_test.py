# created 3/3/22

"""
# this test runs the Sequencing Products Strategies WS and verifies that the data return is as specified
# see specification:
# https://docs.jgi.doe.gov/display/DW/Sequencing+Product-Strategy+%3A+API+Contract

# the base WS URLs are :
# https://ws-access-dev.jgi.doe.gov
# https://ws-access-int.jgi.doe.gov
# https://ws-access.jgi.doe.gov


# The tests included here are:
#
#     GET dw-pc-sequencing-product-strategies status=200;
#          response contains an array of json objects of resource attributes
#
#     NOTE that for this service, filtering by active and/or specific id has NOT been implemented
#     See comments in DW-5145 for more info
"""

import pytest
import json
from assertpy import assert_that

from Utils.util_dbTools_Postgres import DataBaseToolsPostgres
from Utils.wsTools import wsTools


class TestDwSeqProdStratsWs:
    ws_endpoint = "dw-pc-sequencing-product-strategies"
    myDB = DataBaseToolsPostgres()
    myDB.connect()
    myWsTools = wsTools()

    resource_attributes = (
        {"key": "sequencing-strategy-id", "required": True, "data_type": int},
        {"key": "sequencing-product-id", "required": True, "data_type": int},
        {"key": "default-strategy-for-product", "required": True, "data_type": bool},
        {"key": "active", "required": True, "data_type": bool}
    )

    @pytest.fixture(scope="class")
    def test_url(self, ws_base_url):
        return ws_base_url + self.ws_endpoint

    def test_get_all_records_and_verify_schema(self, test_url):
        response_data = self.myWsTools.runWS(test_url)

        for record in response_data:
            # print(json.dumps(record, indent=4, sort_keys=True))
            self.check_for_required_parameters(record)

    def test_ws_content_matches_db(self, test_url):
        response_data = self.myWsTools.runWS(test_url)

        # get info from the db
        db_columns = "sequencing_product_id, allowed_seq_strat_id, default_strat_for_product, active"

        db_query = f"SELECT {db_columns} " \
                   f"FROM pc.dt_m2m_seq_prod_strats;"
        db_json_result_list = self.myDB.do_query_get_rows_as_json(db_query)
        # print(json.dumps(db_json_result_list, indent=4))

        error_msgs = []
        for row in db_json_result_list:
            db_seq_prod_id = row["sequencing_product_id"]
            db_seq_strat_id = row["allowed_seq_strat_id"]
            # print(db_seq_prod_id)

            record_list = [record for record in response_data
                           if record['sequencing-product-id'] == db_seq_prod_id and
                           record['sequencing-strategy-id'] == db_seq_strat_id]

            assert_that(len(record_list)).is_equal_to(1)

            ws_record = record_list[0]
            json_key_value = 'sequencing-strategy-id'
            self.myWsTools.compare_ws_to_db(error_msgs, json_key_value, ws_record, 'sequencing-strategy-id',
                                            row, 'allowed_seq_strat_id')
            self.myWsTools.compare_ws_to_db(error_msgs, json_key_value, ws_record, 'sequencing-product-id',
                                            row, 'sequencing_product_id')
            self.myWsTools.compare_ws_to_db(error_msgs, json_key_value, ws_record, 'active', row, 'active')
            self.myWsTools.compare_ws_to_db(error_msgs, json_key_value, ws_record, 'default-strategy-for-product',
                                            row, 'default_strat_for_product')

        assert_that(len(error_msgs), description=error_msgs).is_equal_to(0)

    # -------------------------------------------------
    def test_active_records(self, test_url):
        error_msgs = []
        self.myWsTools.test_active_records(error_msgs, test_url)
        assert_that(len(error_msgs), description=error_msgs).is_equal_to(0)

    # -------------------------------------------------------------------------------------------
    # check_for_required_parameters
    #
    def check_for_required_parameters(self, record):
        # check the required parameters
        for index in range(len(self.resource_attributes)):
            att_dict = self.resource_attributes[index]
            self.myWsTools.assert_key(record, **att_dict)
