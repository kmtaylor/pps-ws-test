# created 11/10/21

# the WS URLs are :
# https://ws-access-dev.jgi.doe.gov
# https://ws-access-int.jgi.doe.gov
# https://ws-access.jgi.doe.gov


# this test runs the Sow Item Templates WS and verifies that the data return is as specified
# see
# specification:

# The tests included here are:
#
#     GET dw-pc-sow-item-templates status=200; response contains an array of json objects of resource attributes
#     GET dw-pc-sow-item-templates/id# status=200; response contains json object of resource attributes
#     GET dw-pc-sow-item-templates?sequencing-strategy-id=id# response contains a list of
#           sow item templates for a specific strategy
#     TODO - add tests for POST,PUT,DELETE status=405

import json
import numbers

from Utils.open_yaml import open_yaml
from Utils.util_jsonInput import jsonInput
from Utils.http_request_tools import HttpTools
from Utils.util_dbTools_Postgres import DataBaseToolsPostgres
from Utils.wsTools import wsTools


class SowItemTemplatesWST:
    def __init__(self):
        self.printOn = True
        self.errorCnt = 0
        dict_config = open_yaml()
        self.wsURL = dict_config['ws_url'] + "dw-pc-sow-item-templates"
        self.myJson = jsonInput()
        self.myRequests = HttpTools()
        self.myDB = DataBaseToolsPostgres()
        self.myDB.connect()
        self.myWsTools = wsTools()
        self.errorMsgs = []

    # ------------------------------------------------------------------------------------
    # get_all_records_and_verify_schema
    # run the web service without parameters. It verifies the data in response to be
    # consistent with the contract. Prints a report including errors found
    # inputs: none
    # outputs: none
    def get_all_records_and_verify_schema(self):
        response_data = self.myWsTools.runWS(self.wsURL)

        for record in response_data:
            # print(json.dumps(record, indent=4, sort_keys=True))
            self.check_for_required_parameters(record)

            # uncomment the line below if you want to do a db check for every item
            # returned by the web service
            # self.get_records_with_parameter(record['sow-item-number'])

    # -------------------------------------------------------------------------------------------
    # check_for_required_parameters
    #
    def check_for_required_parameters(self, record):
        # check the base parameters
        self.myWsTools.checkKey(self.errorMsgs, record, 'sow-item-number', True, int)
        self.myWsTools.checkKey(self.errorMsgs, record, 'sequencing-strategy-id', True, int)
        self.myWsTools.checkKey(self.errorMsgs, record, 'sow-item-type-id', True, int)
        # Numbers.number is used for TLA below because some of the values are ints and some are decimals, like 0.6
        self.myWsTools.checkKey(self.errorMsgs, record, 'target-logical-amount', True, numbers.Number, null_ok=True)
        self.myWsTools.checkKey(self.errorMsgs, record, 'logical-amount-units-id', True, int, null_ok=True)
        self.myWsTools.checkKey(self.errorMsgs, record, 'degree-of-pooling-id', True, int, null_ok=True)
        self.myWsTools.checkKey(self.errorMsgs, record, 'library-creation-specs-id', True, int, null_ok=True)
        self.myWsTools.checkKey(self.errorMsgs, record, 'run-mode-id', True, int, null_ok=True)
        self.myWsTools.checkKey(self.errorMsgs, record, 'itag-primer-set-id', True, int, null_ok=True)
        self.myWsTools.checkKey(self.errorMsgs, record, 'exome-capture-probe-set-id', True, int, null_ok=True)
        self.myWsTools.checkKey(self.errorMsgs, record, 'qc-type-id', True, int, null_ok=True)
        self.myWsTools.checkKey(self.errorMsgs, record, 'active', True, bool)

        if record['active']:
            self.myWsTools.checkKey(self.errorMsgs, record, 'library-creation-specs-id', True, int, null_ok=True)


    # ------------------------------------------------------------------------------------
    # get_records_with_parameter(self,parm):
    # run the web service with parameters.
    # expects one records returned in response by WS
    # inputs: parameter  (ie.  lc spec id)
    # outputs: prints error report
    def get_records_with_parameter(self, parm):
        record = self.myWsTools.runWS(self.wsURL, parm)

        # get the info from the db for this id
        db_fields = "sow_item_number, sequencing_strategy_id, sow_item_type_id, " \
                    "target_logical_amount, logical_amount_units_id, degree_of_pooling_id, " \
                    "library_creation_specs_id, run_mode_id, itag_primer_set_id, exome_capture_probe_set_id, " \
                    "qc_type_id, active"

        db_info = self.myDB.get_columns_from_single_row_single_table(db_fields,
                                                        'pc.dt_sow_item_template',
                                                        'sow_item_number',
                                                                     str(parm))

        self.myWsTools.compare_ws_to_db(self.errorMsgs, 'sow-item-number', record, 'sow-item-number', db_info, 'sow_item_number')
        self.myWsTools.compare_ws_to_db(self.errorMsgs, 'sow-item-number', record, 'sequencing-strategy-id', db_info, 'sequencing_strategy_id')
        self.myWsTools.compare_ws_to_db(self.errorMsgs, 'sow-item-number', record, 'sow-item-type-id', db_info, 'sow_item_type_id')
        self.myWsTools.compare_ws_to_db(self.errorMsgs, 'sow-item-number', record, 'target-logical-amount', db_info, 'target_logical_amount')
        self.myWsTools.compare_ws_to_db(self.errorMsgs, 'sow-item-number', record, 'logical-amount-units-id', db_info, 'logical_amount_units_id')
        self.myWsTools.compare_ws_to_db(self.errorMsgs, 'sow-item-number', record, 'degree-of-pooling-id', db_info, 'degree_of_pooling_id')
        self.myWsTools.compare_ws_to_db(self.errorMsgs, 'sow-item-number', record, 'library-creation-specs-id', db_info, 'library_creation_specs_id')
        self.myWsTools.compare_ws_to_db(self.errorMsgs, 'sow-item-number', record, 'run-mode-id', db_info, 'run_mode_id')
        self.myWsTools.compare_ws_to_db(self.errorMsgs, 'sow-item-number', record, 'itag-primer-set-id', db_info, 'itag_primer_set_id')
        self.myWsTools.compare_ws_to_db(self.errorMsgs, 'sow-item-number', record, 'exome-capture-probe-set-id', db_info, 'exome_capture_probe_set_id')
        self.myWsTools.compare_ws_to_db(self.errorMsgs, 'sow-item-number', record, 'qc-type-id', db_info, 'qc_type_id')
        self.myWsTools.compare_ws_to_db(self.errorMsgs, 'sow-item-number', record, 'active', db_info, 'active')

    def get_records_by_sequencing_strategy(self, param):
        ws_records = self.myWsTools.runWS(self.wsURL, 'sequencing-strategy-id', param)

        # get the sow numbers from the db for this sequencing strategy
        db_query = f"SELECT sow_item_number " \
                   f"FROM pc.dt_sow_item_template " \
                   f"where sequencing_strategy_id = {param};"
        column_names, db_rows = self.myDB.do_query_get_all_rows_cols(db_query)

        if not len(ws_records) == len(db_rows):
            error_msg = f'for sequencing strategy {param} the db and ws do not return the same number of templates'
            self.errorMsgs.append(error_msg)

        for row in db_rows:
            ws_contains_info = False
            db_sow_number = int(row[0])
            for ws_template in ws_records:
                if ws_template['sow-item-number'] == db_sow_number:
                    ws_contains_info = True
                    break

            if not ws_contains_info:
                self.errorMsgs.append(f'database shows sow item number {db_sow_number} '
                                      f'associated with sequencing strategy {param} but the webservice does not')


# ------this is how to run the tests--------------------------
myTest = SowItemTemplatesWST()
print("----------------------------------------------------")
print("TESTING: get all records and verify schema")
myTest.get_all_records_and_verify_schema()

print("----------------------------------------------------")
print("TESTING: with valid <id> parameters.")
myTest.get_records_with_parameter(1)  # a regular sow item template
myTest.get_records_with_parameter(77) # a sow item template with a decimal target logical amount - see DW-5107
myTest.get_records_with_parameter(73) # a sow item template with a non null exome capture set id

print("----------------------------------------------------")
print("TESTING: for finding sow templates for sequencing strategies.")
# this test should be run with an FD prod id that has seq products that have alt seq strategies
# per Dana comment in DW-4950, those FD ids currently are: 2, 51, 58, 59, 60, 61, 77, 82
myTest.get_records_by_sequencing_strategy(1)
myTest.get_records_by_sequencing_strategy(60)

# TODO invalid parameter test
# print("----------------------------------------------------")
# print("TESTING: url with invalid <id> parameter.")

# TODO PUT not allowed
# TODO POST not allowed
# TODO DELETE not allowed

print("----------------------------------------------------")
print("---end---")

if len(myTest.errorMsgs) == 0:
    print("No Errors")
else:
    print("Errors occurred!")
    print("Error messages:")
    print(myTest.errorMsgs)
