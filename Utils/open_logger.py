"""
   Tools for logging messages to file and console
"""
import logging
import logging.config
import yaml
import os

TEST_ENVIRONMENT = 'pps-int'

def open_logger(name='dev'):
    """

    :param name:  this is the name of the logger that should be used
        -- see the config.yaml file
        -- default is dev
    :return: logger:

    example:
    import Utils.open_logger
        logger = Utils.open_logger.open_logger()  # will default to 'dev'
        logger.warning("message")
    """
    dirname = os.path.dirname(__file__)
    filename = os.path.join(dirname, f'../config/{TEST_ENVIRONMENT}/config.yaml')
    with open(filename, 'rt') as yaml_file:
        config = yaml.safe_load(yaml_file.read())

    logging.config.dictConfig(config)
    logger = logging.getLogger(name)
    return logger
