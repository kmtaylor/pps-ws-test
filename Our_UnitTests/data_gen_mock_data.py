JSON_THREE_TUBE_SAMPLES = """
        {"sp-ids":[1665292,1665293,1665294],
        "analytes":[{"final-analyte-lims-id":"KOL177654A1PA1","final-analyte-name":"344209",
             "ancestry":{"fd-ids":[1665288],"sp-ids":[1665293],"sample-ids":[344209]}},
             {"final-analyte-lims-id":"KOL177653A1PA1","final-analyte-name":"344208",
             "ancestry":{"fd-ids":[1665286],"sp-ids":[1665292],"sample-ids":[344208]}},
             {"final-analyte-lims-id":"KOL177655A1PA1","final-analyte-name":"344210",
             "ancestry":{"fd-ids":[1665290],"sp-ids":[1665294],"sample-ids":[344210]}}],
        "data-generation-request":{
            "clarity-username":"ide","clarity-password":"ide",
            "analyte-lims-ids":["KOL177653A1PA1","KOL177654A1PA1","KOL177655A1PA1"],
            "destination-process-type":"SM Approve For Shipping"}}
    """

# This list must be kept in sync with the data in JSON_THREE_TUBE_SAMPLES
THREE_TUBES_SAMPLES_IDS = [344208, 344209, 344210]

JSON_THREE_TUBE_LIBRARIES = """
    {"sp-ids":[1665301,1665302,1665303],
    "analytes":[
        {"final-analyte-lims-id":"2-5373313","final-analyte-name":"HXZGY",
            "ancestry":{"fd-ids":[1665295],"sp-ids":[1665301],"sample-ids":[344211],
                    "library-names":["HXZGY"],"library-stocks-lims-ids":["2-5373313"]}},
        {"final-analyte-lims-id":"2-5373314","final-analyte-name":"HXZGZ",
            "ancestry":{"fd-ids":[1665297],"sp-ids":[1665302],"sample-ids":[344212],
                    "library-names":["HXZGZ"],"library-stocks-lims-ids":["2-5373314"]}},
        {"final-analyte-lims-id":"2-5373315","final-analyte-name":"HXZHA",
            "ancestry":{"fd-ids":[1665299],"sp-ids":[1665303],"sample-ids":[344213],
                    "library-names":["HXZHA"],"library-stocks-lims-ids":["2-5373315"]}}],
"data-generation-request":{"clarity-username":"ide","clarity-password":"ide",
    "analyte-lims-ids":["KOL177656A1PA1","KOL177657A1PA1","KOL177658A1PA1"],
    "destination-process-type":"LQ Library qPCR"}}
    """

# the names below should match the library names in JSON_THREE_TUBE_LIBRARIES
THREE_TUBE_LIBRARY_NAMES = ["HXZGY", "HXZGZ", "HXZHA"]

JSON_POOL_10 = """
    {"sp-ids":[1659469,1659470,1659471,1659472,1659473,1659474,1659475,1659476,1659477,1659478],
     "analytes":[{"final-analyte-lims-id":"2-5344219","final-analyte-name":"HXWSC",
         "ancestry":{
             "sp-ids":[1659471,1659470,1659469,1659474,1659473,1659472,1659478,1659477,1659476,1659475],
             "sample-ids":[342750,342749,342748,342753,342752,342751,342757,342756,342755,342754],
             "library-names":["HXWPZ","HXWPY","HXWSB","HXWSA","HXWPP","HXWPS","HXWPU","HXWPT","HXWPX","HXWPW"],
             "library-stocks-lims-ids":["2-5344145","2-5344144","2-5344147","2-5344146","2-5344138","2-5344139","2-5344141","2-5344140","2-5344143","2-5344142"],
             "pool-name":"HXWSC"}}],
     "data-generation-request":{"clarity-username":"alk","clarity-password":"alkalkalk",
          "analyte-lims-ids":["KOL176264A1PA1","KOL176265A1PA1","KOL176266A1PA1","KOL176267A1PA1",
                              "KOL176268A1PA1","KOL176269A1PA1","KOL176270A1PA1","KOL176271A1PA1",
                              "KOL176272A1PA1","KOL176273A1PA1"],
          "destination-process-type":"SQ Sequencing"}}
    """

JSON_POOL_10_SINGLE_SPID = """
    {"sp-ids": [1310953], 
    "analytes": [
    {"final-analyte-lims-id": "2-4454033", "final-analyte-name": "HGZSU", 
    "ancestry": {"fd-ids": [1310949], "sp-ids": [1310953], "sample-ids": [265374], 
    "library-names": ["HGZSU"], "library-stocks-lims-ids": ["2-4454020"]}}, 
    {"final-analyte-lims-id": "2-4454040", "final-analyte-name": "HGZSC", 
    "ancestry":{"fd-ids": [1310949], "sp-ids": [1310953], "sample-ids": [265366], 
    "library-names": ["HGZSC"], "library-stocks-lims-ids": ["2-4454012"]}}, 
    {"final-analyte-lims-id": "2-4454044", "final-analyte-name": "HGZSW", 
    "ancestry":{"fd-ids": [1310949], "sp-ids": [1310953], "sample-ids": [265375], 
    "library-names": ["HGZSW"], "library-stocks-lims-ids": ["2-4454011"]}}, 
    {"final-analyte-lims-id": "2-4454048", "final-analyte-name": "HGZSS", 
    "ancestry": {"fd-ids": [1310949], "sp-ids": [1310953], "sample-ids": [265372], 
    "library-names": ["HGZSS"], "library-stocks-lims-ids": ["2-4454018"]}}, 
    {"final-analyte-lims-id": "2-4454102", "final-analyte-name": "HGZSP", 
    "ancestry": {"fd-ids": [1310949], "sp-ids": [1310953], "sample-ids": [265371], 
    "library-names": ["HGZSP"], "library-stocks-lims-ids": ["2-4454017"]}}, 
    {"final-analyte-lims-id": "2-4454103", "final-analyte-name": "HGZST", 
    "ancestry": {"fd-ids": [1310949], "sp-ids": [1310953], "sample-ids": [265373], 
    "library-names": ["HGZST"], "library-stocks-lims-ids": ["2-4454019"]}}, 
    {"final-analyte-lims-id": "2-4454107", "final-analyte-name": "HGZSH", 
    "ancestry": {"fd-ids": [1310949], "sp-ids": [1310953], "sample-ids": [265368], 
    "library-names": ["HGZSH"], "library-stocks-lims-ids": ["2-4454014"]}}, 
    {"final-analyte-lims-id": "2-4454111", "final-analyte-name": "HGZSG", 
    "ancestry": {"fd-ids": [1310949], "sp-ids": [1310953], "sample-ids": [265367], 
    "library-names": ["HGZSG"], "library-stocks-lims-ids": ["2-4454013"]}}, 
    {"final-analyte-lims-id": "2-4454115", "final-analyte-name": "HGZSO", 
    "ancestry": {"fd-ids": [1310949], "sp-ids": [1310953], "sample-ids": [265370], 
    "library-names": ["HGZSO"], "library-stocks-lims-ids": ["2-4454016"]}}, 
    {"final-analyte-lims-id": "2-4454119", "final-analyte-name": "HGZSN", 
    "ancestry": {"fd-ids": [1310949], "sp-ids": [1310953], "sample-ids": [265369], 
    "library-names": ["HGZSN"], "library-stocks-lims-ids": ["2-4454015"]}}], 
    "data-generation-request": {"clarity-username": "ide", "clarity-password": "ide", 
    "analyte-lims-ids": ["REI88536A1PA1", "REI88536A2PA1", "REI88536A3PA1", "REI88536A4PA1", 
    "REI88536A5PA1", "REI88536A6PA1", "REI88536A7PA1", "REI88536A8PA1", "REI88536A9PA1", "REI88536A10PA1"], 
    "destination-process-type": "RQC"}}
    """