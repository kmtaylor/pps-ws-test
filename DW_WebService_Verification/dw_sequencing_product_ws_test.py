# created 11/10/21

# the WS URLs are :
# https://ws-access-dev.jgi.doe.gov
# https://ws-access-int.jgi.doe.gov
# https://ws-access.jgi.doe.gov


# this test runs the Sequencing Products WS and verifies that the data return is as specified
# see specification:
# https://docs.jgi.doe.gov/display/DW/Sequencing+Product%3A+API+Contract

# The tests included here are:
#
#     GET dw-pc-sequencing-products status=200; response contains an array of json objects of resource attributes
#     GET dw-pc-sequencing-products/id# status=200; response contains a single json object of resource attributes
#     GET dw-pc-sequencing-products?active=true response contains a list of only the active items

import json

from Utils.open_yaml import open_yaml
from Utils.util_jsonInput import jsonInput
from Utils.http_request_tools import HttpTools
from Utils.util_dbTools_Postgres import DataBaseToolsPostgres
from Utils.wsTools import wsTools


class SequencingProductWST:
    def __init__(self):
        self.printOn = True
        self.errorCnt = 0
        dict_config = open_yaml()
        self.wsURL = dict_config['ws_url'] + "dw-pc-sequencing-products"
        self.myJson = jsonInput()
        self.myRequests = HttpTools()
        self.myDB = DataBaseToolsPostgres()
        self.myDB.connect()
        self.myWsTools = wsTools()
        self.errorMsgs = []

    # ------------------------------------------------------------------------------------
    # get_all_records_and_verify_schema
    # run the web service without parameters. It verifies the data in response to be
    # consistent with the contract. Prints a report including errors found
    # inputs: none
    # outputs: none
    def get_all_records_and_verify_schema(self):
        response_data = self.myWsTools.runWS(self.wsURL)

        for record in response_data:
            # print(json.dumps(record, indent=4, sort_keys=True))
            self.check_for_required_parameters(record)

            # uncomment the line below if you want to do a db check for every item
            # returned by the web service
            # self.get_records_with_parameter(record['sequencing-product-id'])

    # -------------------------------------------------------------------------------------------
    # check_for_required_parameters
    #
    def check_for_required_parameters(self, record):
        # check the base parameters
        self.myWsTools.checkKey(self.errorMsgs, record, 'sequencing-product-id', True, int)
        self.myWsTools.checkKey(self.errorMsgs, record, 'sequencing-product-name', True, str)
        self.myWsTools.checkKey(self.errorMsgs, record, 'active', True, bool)
        self.myWsTools.checkKey(self.errorMsgs, record, 'default-sequencing-strategy-id', True, int)
        # TODO see if there is a python date type that will work for these two dates types,
        #  otherwise code some checking?
        self.myWsTools.checkKey(self.errorMsgs, record, 'aud-date-created', True, str)
        self.myWsTools.checkKey(self.errorMsgs, record, 'aud-last-modified', True, str)

    # ------------------------------------------------------------------------------------
    # get_records_with_parameter(self,parm):
    # run the web service with parameters.
    # expects one records returned in response by WS
    # inputs: parameter  (ie.  lc spec id)
    # outputs: prints error report
    def get_records_with_parameter(self, parm):
        record = self.myWsTools.runWS(self.wsURL, parm)

        # get the info sequencing product info from the db for this id
        db_fields = "sequencing_product_id, sequencing_product_name, active, " \
                    "aud_date_created, aud_last_modified"

        db_info = self.myDB.get_columns_from_single_row_single_table(db_fields,
                                                                     'pc.DT_SEQUENCING_PRODUCT',
                                                                     'sequencing_product_id',
                                                                     str(parm))

        # compare values from the ws with values from the db
        json_key_value = 'sequencing-product-id'
        self.myWsTools.compare_ws_to_db(self.errorMsgs, json_key_value, record, 'sequencing-product-id',
                                        db_info, 'sequencing_product_id')
        self.myWsTools.compare_ws_to_db(self.errorMsgs, json_key_value, record, 'sequencing-product-name',
                                        db_info, 'sequencing_product_name')
        self.myWsTools.compare_ws_to_db(self.errorMsgs, json_key_value, record, 'active', db_info, 'active')
        self.myWsTools.compare_ws_to_db(self.errorMsgs, json_key_value, record, 'aud-date-created',
                                        db_info, 'aud_date_created')
        self.myWsTools.compare_ws_to_db(self.errorMsgs, json_key_value, record, 'aud-last-modified',
                                        db_info, 'aud_last_modified')

        # get the info default sequencing strategy info from the db for this id
        db_query = f"SELECT allowed_seq_strat_id FROM pc.dt_m2m_seq_prod_strats " \
                   f"WHERE default_strat_for_product = 'Y' and sequencing_product_id = {parm};"

        column_names, db_info = self.myDB.do_query_get_all_rows_cols(db_query)
        # compare the default seq strategy from ws to that from db
        key = 'default-sequencing-strategy-id'
        ws_value = record[key]
        db_value = db_info[0][0] # [0][0] gets the first value in the first row from the db query

        if ws_value != db_value:
            err_msg = f"for sequencing_product_id {record['sequencing-product-id']} " \
                      f"the ws key {key} shows {record[key]} " \
                      f"while the db shows {db_value}"
            self.errorMsgs.append(err_msg)

    def test_active_records(self):
        self.myWsTools.test_active_records(self.errorMsgs, self.wsURL)


# ------this is how to run the tests--------------------------
myTest = SequencingProductWST()
print("----------------------------------------------------")
print("TESTING: get all records and verify schema")
myTest.get_all_records_and_verify_schema()

print("----------------------------------------------------")
print("TESTING: with valid <id> parameters.")
myTest.get_records_with_parameter(1)  # a regular sequencing product
myTest.get_records_with_parameter(2) # a SP that has a default AND alternative seq strategy
myTest.get_records_with_parameter(15) # a SP that has aud-last-modifed with trailing 0

print("----------------------------------------------------")
print("TESTING: get all active records")
myTest.test_active_records()
print("----------------------------------------------------")

# TODO invalid parameter test
# print("----------------------------------------------------")
# print("TESTING: url with invalid <id> parameter.")

# TODO PUT not allowed
# TODO POST not allowed
# TODO DELETE not allowed

print("----------------------------------------------------")
print("---end---")

if len(myTest.errorMsgs) == 0:
    print("No Errors")
else:
    print("Errors occurred!")
    print("Error messages:")
    print(myTest.errorMsgs)
