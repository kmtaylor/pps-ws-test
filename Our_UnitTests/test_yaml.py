"""
    unit tests for yaml
"""
from unittest import TestCase
import Utils.open_yaml

class TestYamlTools(TestCase):
    """
        unit tests for logger
    """
    def setUp(self):
        """setup"""

    def test_open_yaml(self):
        """ open yaml and look for test value """
        test_data = 'this is a test'
        dict_config = Utils.open_yaml.open_yaml()
        test_value = dict_config['test_value']
        self.assertEqual(test_data, test_value)
