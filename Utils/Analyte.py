"""
   Representation of a Clarity artifact
"""


class Analyte:

    def __init__(self, lims_id, name, spids, sample_ids=[],
                 library_names=[], library_lims_ids=[], pool_name=""):
        self.lims_id = lims_id
        self.name = name
        self.spids = spids
        self.sample_ids = sample_ids
        self.library_names = library_names
        self.library_lims_ids = library_lims_ids
        self.pool_name = pool_name

    def __repr__(self):
        return "<Artifact lims_id:%s name:%s spids:%s sample_ids:%s library_names:%s>" % \
               (self.lims_id, self.name, self.spids, self.sample_ids, self.library_names)

    def __str__(self):
        return "<Artifact lims_id:%s name:%s spids:%s sample_ids:%s library_names%s>" % \
               (self.lims_id, self.name, self.spids, self.sample_ids, self.library_names)

    @staticmethod
    def get_all_lims_ids(analytes):
        lims_ids = []
        for analyte in analytes:
            ids = analyte.lims_id
            lims_ids.append(ids)
        return lims_ids

    @staticmethod
    def get_all_names(analytes):
        names = []
        for analyte in analytes:
            name = analyte.name
            names.append(name)
        return names

    @staticmethod
    def get_all_sample_ids(analytes):
        sample_ids = []
        for analyte in analytes:
            ids = analyte.sample_ids
            # use += rather than append because sample_ids for each analyte is a list
            # += gives us one single final list, rather than a list of lists
            sample_ids += ids
        return sample_ids

    @staticmethod
    def get_all_sp_ids(analytes):
        sp_ids = set()
        for analyte in analytes:
            ids = analyte.spids
            sp_ids.update(ids)
        return list(sp_ids)

    @staticmethod
    def get_all_library_names(analytes):
        library_names = []
        for analyte in analytes:
            ids = analyte.library_names
            # use += rather than append because sample_ids for each analyte is a list
            # += gives us one single final list, rather than a list of lists
            library_names += ids
        return library_names