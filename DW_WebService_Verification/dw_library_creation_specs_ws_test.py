
# created 2/22/21

# the WS URLs are :
# https://ws-access-dev.jgi.doe.gov
# https://ws-access-int.jgi.doe.gov
# https://ws-access.jgi.doe.gov


#this test runs the libraryCreationSpecs WS and verifies that the data return is as specified
# see
# specification:



# The tests included here are:
#
#     GET dw-pc-library-creation-specs status=200; response contains an array of json objects of resource attributes
#     GET dw-pc-library-creation-specs/id# status=200; response contains json object of resource attributes
#     GET dw-pc-library-creation-specs?organization-name=<organization-name> status=200; response contains an array of
#     json objects of resource attributes
#     GET dw-pc-library-creation-specs?organization-name=<organization-name> with invalid or missing <organization-name>;
#     status = 200  with empty list
#     Get dw-pc-library-creation-specs with invalid/incorrect URL; status =404 “error”:“Resource Not Found”
#     POST,PUT,DELETE status=405


import requests
import json

from Utils.open_yaml import open_yaml
from Utils.util_dbTools_Postgres import DataBaseToolsPostgres
from Utils.util_jsonInput import jsonInput
from Utils.http_request_tools import HttpTools
from Utils.util_dbTools import DataBaseTools


class  libraryCreationSpecsWST():
    def __init__(self):
        self.printOn = True
        self.errorCnt = 0
        self.dict_config = open_yaml()
        self.wsURL = self.dict_config['ws_url'] + "dw-pc-library-creation-specs"
        self.myJson = jsonInput()
        self.myRequests = HttpTools()
        self.myDB = DataBaseToolsPostgres()
        self.myDB.connect()



    # -------------------------------------------------------------------------------------------
    # runWS
    #
    def runWS(self,parm="",parmvalue=""):
        wsURL = self.wsURL
        if parmvalue:  # if URL has a query parmeter
            if parm =="/":
                wsURL = self.wsURL + parm + parmvalue
            else:
                wsURL = self.wsURL + "/?" + parm +"=" + parmvalue
        if parm and parmvalue=="":  #if  URL has a parameter (that is not a query)
            wsURL = self.wsURL + "/" + str(parm)

        if self.printOn:
            print("using WS url: ", wsURL)
        status,responseJson = self.myRequests.http_get_json_w_expectedStatus(wsURL,200)
        return responseJson
    # -------------------------------------------------------------------------------------------
    # checkKey -check if key exists
    #
    def checkKey(self,dict, key):

        if key in dict.keys():
            return True
        else:
             return False

    # -------------------------------------------------------------------------------------------
    #checkDBfieldForActive
    # returns True if the record is active

    def checkDBfieldForActive(self,tableName, fieldName, fieldvalue):
        activeValue = self.myDB.get_database_attribute( "active", tableName, fieldName, str(fieldvalue))
        if activeValue == "Y":
            return True
        else:
            return False

    # -------------------------------------------------------------------------------------------
    # checkForRequiredParameters
    #

    def checkForRequiredParameters(self,libraryCreationSpecsRecord):
        verificationErrors = 0
        #print(json.dumps(libraryCreationSpecsRecord, indent=4, sort_keys=True))

        errorMsg = ""
        libraryCreationSpecsId = libraryCreationSpecsRecord["lc-specs-id"]
        if not(libraryCreationSpecsId and  isinstance(libraryCreationSpecsId,int)):
            verificationErrors +=1
            errorMsg += "lc-specs-id  inconsistency,"

        libraryCreationSpecs = libraryCreationSpecsRecord["lc-specs"]
        if not(libraryCreationSpecs and  isinstance(libraryCreationSpecs,str)):
            verificationErrors +=1
            errorMsg += "lc-specs inconsistency,"

        organizationName = libraryCreationSpecsRecord["organization-name"]
        if not (organizationName and isinstance(organizationName, str)):
            verificationErrors += 1
            errorMsg += "organization-name inconsistency,"

        active = libraryCreationSpecsRecord["lc-specs-active"]
        if active:
            if not (isinstance(active, bool)):
                verificationErrors += 1
                errorMsg += "lc-specs-active inconsistency,"
            protocolId = libraryCreationSpecsRecord["protocol-id"]
            if not (protocolId and (isinstance(protocolId, int))):
                verificationErrors += 1
                errorMsg += "protocol-id inconsistency,"

            lcQueues = libraryCreationSpecsRecord["lc-queues"]
            if not (lcQueues and isinstance(lcQueues, list)):
                verificationErrors += 1
                errorMsg += "lc-queues list inconsistency,"
            else:
                for record in lcQueues:
                    # print(json.dumps(record, indent=4, sort_keys=True))

                    lcQueueName = record["lc-queue"]
                    if not (lcQueueName and isinstance(lcQueueName, str)):
                        verificationErrors += 1
                        errorMsg += "lc-queue name inconsistency,"

                    lcQueueId = record["lc-queue-id"]
                    if not (lcQueueId and isinstance(lcQueueId, int)):
                        verificationErrors += 1
                        errorMsg += "lc-queue-id inconsistency,"
                    else:
                        if not(self.checkDBfieldForActive("pc.DT_M2M_LC_QUEUE_SPECS","LIBRARY_CREATION_QUEUE_ID",lcQueueId)):
                            verificationErrors += 1
                            errorMsg += "lc-queue-id not active"

            if self.checkKey(libraryCreationSpecsRecord,"library-protocol"):
                libraryProtocol = libraryCreationSpecsRecord["library-protocol"]
                if not (libraryProtocol and isinstance(libraryProtocol, str)):
                    verificationErrors += 1
                    errorMsg += "library-protocol inconsistency,"
            else:
                verificationErrors += 1
                errorMsg += "library-protocol attribute does not exist,"

        if verificationErrors > 0:
            print("***DB errors for libraryCreationSpecs: ",libraryCreationSpecs, ", " ,
                  libraryCreationSpecsId, ", required parameter(s) inconsistency = ",
                  verificationErrors, " errors. Info: ",errorMsg)
        return verificationErrors,libraryCreationSpecs,errorMsg




    #------------------------------------------------------------------------------------
    # getAllLibraryCreationSpecs_and_verifyData
    # run the web service without parameters. It verifies the data in response to be
    # consistent with the contract. Prints a report including errors found
    # inputs: none
    # outputs: none
    def getAllLibraryCreationSpecs_and_verifyData(self):
        libraryCreationSpecsData = self.runWS()
        lcErrors = 0
        #print(libraryCreationSpecsData)
        activeCount =0
        JGIcount=0
        HAcount = 0
        totalErrors = 0
        libraryCreationSpecsErrorsDict={}

        for record in libraryCreationSpecsData:
            print(json.dumps(record, indent=4, sort_keys=True))
            if record["lc-specs-active"] == True :
                activeCount +=1
            if record["organization-name"] == "JGI":
                JGIcount += 1
            elif record["organization-name"] == "Hudson Alpha":
                HAcount += 1
            lcErrors,libraryCreationSpecs,errMsg = self.checkForRequiredParameters(record)
            totalErrors = lcErrors + totalErrors
            if lcErrors > 0:
                libraryCreationSpecsErrorsDict[libraryCreationSpecs] =errMsg  #save errors for later reporting
        print ("***** library CreationSpecs DB attribute verification Report ! *****")
        print("number of organization-name=JGI = ",JGIcount)
        print("number of organization-name=Hudson Alpha = ", HAcount)
        print("number of ACTIVE library CreationSpecs = ", activeCount)
        print("number of total library CreationSpecs records = ",len(libraryCreationSpecsData))
        # print ("total errors found with DB/contract inconsistency= ", totalErrors)
        # print("****************************************************************")
        # print ("library CreationSpecs  :      ERRORS FOUND:")
        # #print report of this test
        # if totalErrors >0 :
        #     for libraryCreationSpecs in libraryCreationSpecsErrorsDict:
        #         print (libraryCreationSpecs, ":", libraryCreationSpecsErrorsDict[libraryCreationSpecs])
        print("test errors:", totalErrors)


  #------------------------------------------------------------------------------------
    # getlibraryCreationSpecs_with_query_parameter(self,parm,value):
    # run the web service with parameters.
    # expects a list of records returned in response by WS
    # inputs: none
    # outputs: none
    def getlibraryCreationSpecs_with_query_parameter(self,parm,value):
        libraryCreationSpecsData = self.runWS(parm,value)
        JGIcount = 0
        HAcount = 0
        totalErrors = 0
        libraryCreationSpecsErrorsDict = {}
        #print(json.dumps(libraryCreationSpecsData, indent=4, sort_keys=True))
        for record in libraryCreationSpecsData:
            if record["organization-name"] == "JGI":
                JGIcount += 1
            elif record["organization-name"] == "Hudson Alpha":
                HAcount += 1
            lcErrors, libraryCreationSpecs, errMsg = self.checkForRequiredParameters(record)
            totalErrors = lcErrors + totalErrors
            if lcErrors > 0:
                libraryCreationSpecsErrorsDict[libraryCreationSpecs] = errMsg  # save errors for later reporting
        print("number of organization-name=JGI = ",JGIcount)
        print("number of organization-name=Hudson Alpha = ", HAcount)
        print("number of total library CreationSpecs records = ",len(libraryCreationSpecsData))
        print("test errors:", totalErrors)


 #------------------------------------------------------------------------------------
    # getlibraryCreationSpecs_with_parameter(self,parm):
    # run the web service with parameters.
    # expects one records returned in response by WS
    # inputs: parameter  (ie.  lc spec id)
    # outputs: prints error report
    def getlibraryCreationSpecs_with_parameter(self,parm):
        libraryCreationSpecsData = self.runWS(parm)
        totalErrors = 0
        libraryCreationSpecsErrorsDict = {}
        print(json.dumps(libraryCreationSpecsData, indent=4, sort_keys=True))
        lcErrors, libraryCreationSpecs, errMsg = self.checkForRequiredParameters(libraryCreationSpecsData)
        if lcErrors > 0:
                libraryCreationSpecsErrorsDict[libraryCreationSpecs] = errMsg  # save errors for later reporting
        print ("test errors:", lcErrors)

#------this is how to run the tests--------------------------

myTest = libraryCreationSpecsWST()
print ("----------------------------------------------------")
print ("TESTING: library CreationSpecs WS  and verify data ")
myTest.getAllLibraryCreationSpecs_and_verifyData()

print ("----------------------------------------------------")
print ("TESTING: library CreationSpecs WS with parameter organization-name=JGI")
myTest.getlibraryCreationSpecs_with_query_parameter("organization-name","JGI")
print ("----------------------------------------------------")
print ("TESTING: library CreationSpecs WS with parameter organization-name=Hudson Alpha")
myTest.getlibraryCreationSpecs_with_query_parameter("organization-name","Hudson Alpha")

print ("----------------------------------------------------")
print ("library CreationSpecs WS with valid query parameter (organization-name) but invalid value  --expecting no records -------")
print ("TESTING: library CreationSpecs WS with query parameter organization-name=HudsonAlpha")
myTest.getlibraryCreationSpecs_with_query_parameter("organization-name","HudsonAlpha")

print ("----------------------------------------------------")
print ("----Error testing---invalid query parameter -- expecting all records -------")
print ("TESTING: library CreationSpecs WS with invalid query parameter platform")
myTest.getlibraryCreationSpecs_with_query_parameter("lc-specs","PacBio IsoSeq")


print ("----------------------------------------------------")
print ("TESTING: library CreationSpecs WS with valid <id> parameter.")
print ("Expecting only one LC-spec record in the response")
myTest.getlibraryCreationSpecs_with_parameter(5)

print ("----------------------------------------------------")
print ("TESTING: library CreationSpecs WS with invalid <id> parameter.")
print ("Expecting  -- expecting status 404 -------")
saveURL =  myTest.wsURL
myTest.wsURL = "https://ws-access-int.jgi.doe.gov/dw-pc-library-creation-specs/231"
returnedData = myTest.runWS()
myTest.wsURL = saveURL
print(returnedData)

print ("----------------------------------------------------")

print ("----Error testing---incorrect URL -- expecting status 404 -------")
print ("TESTING: library CreationSpecs WS incorrect URL")
saveURL =  myTest.wsURL
myTest.wsURL = "https://ws-access-int.jgi.doe.gov/dw-pc-library-creation-specs/JGI"
libraryCreationSpecsData = myTest.runWS()
print(libraryCreationSpecsData)

print ("----------------------------------------------------")
print("---end---")


#









