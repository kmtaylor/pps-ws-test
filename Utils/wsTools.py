import decimal

from assertpy import soft_assertions, assert_that

from Utils.http_request_tools import HttpTools


class wsTools:
    def __init__(self):
        self.printOn = True
        self.errorCnt = 0
        self.myRequests = HttpTools()

    def runWS(self, wsURL, parm="", parmvalue=""):
        if parmvalue:  # if URL has a query parmeter
            if parm == "/":
                wsURL = wsURL + parm + str(parmvalue)
            else:
                wsURL = f'{wsURL}?{parm}={parmvalue}'
        if parm is not "" and parmvalue == "":  # if  URL has a parameter (that is not a query)
            wsURL = wsURL + "/" + str(parm)

        if self.printOn:
            print("using WS url: ", wsURL)
        status, responseJson = self.myRequests.http_get_json_w_expectedStatus(wsURL, 200)
        return responseJson

    @staticmethod
    def checkKey(error_array, record, key, required, data_type, null_ok=False):
        """
        :param error_array: error messages will be added to this array
        :param record: a single json object to be checked
        :param key: the json key to check
        :param required: a boolean to specify whether this key is required to be in the output
        :param data_type: the type that the value should be, like float, str, int, or
                          numbers.Number - which works for both float and int
        :param null_ok: are null values allowed for this parameter
        :return:
        """
        key_exists = False

        if key in record.keys():
            key_exists = True

        if required and not key_exists:
            error_array.append(f'required key {key} does not exist in record')

        if key_exists:
            value = record[key]
            if not isinstance(value, data_type):
                if value is None:
                    if null_ok:
                        return
                    else:
                        error_array.append(f'key {key} is null which is not expected')
                else:
                    error_array.append(f'key {key} is not of type {data_type}')

    @staticmethod
    def assert_key(record, key, required, data_type, null_ok=False):
        """
        :param record: a single json object to be checked
        :param key: the json key to check
        :param required: a boolean to specify whether this key is required to be in the output
        :param data_type: the type that the value should be, like float, str, int, or
                          numbers.Number - which works for both float and int
        :param null_ok: are null values allowed for this parameter
        :return:
        """
        with soft_assertions():
            if required:
                assert_that(key in record.keys(), description=f'required key {key} is missing').is_true()

            if key in record.keys():
                value = record[key]
                if not null_ok:
                    assert_that(value, description=f'key {key} is null which is not expected').is_not_none()
                if value is not None:
                    assert_that(value, description=f'key {key} is not of type {data_type}').is_type_of(data_type)

    @staticmethod
    def compare_ws_to_db(error_array, id_key, json_record, key, db_record, column_name):
        # for 'active' we have to check True against Y and False against N

        ws_value = json_record[key]
        db_value = db_record[column_name]
        # for some values, the db result will contain values that are class decimal.Decimal
        # then we need to convert the ws_value, which will be of type float to decimal.Decimal
        # because a float with value 0.1 does not evaluate as equal to a decimal with value 0.1
        if isinstance(db_value, decimal.Decimal):
            ws_value = decimal.Decimal(db_value)

        # if the value is a date, we need to convert the datetime value from the db to a str
        # and strip a 'T' out of the web service value
        if 'aud' in column_name:
            ws_value = str(ws_value).replace('T', ' ')
            db_value = str(db_value)
            # the aud_last_modifed in db sometimes have trailing 0 in the milliseconds, like
            # 01-JUL-14 11.48.38.969550, those 0s aren't shown in ws, so strip them before comparing
            if 'aud_last_modified' in column_name:
                while db_value[-1] == '0':
                    db_value = db_value[:-1]

        if ws_value != db_value:
            # for booleans the values don't match exactly, don't add an error message for that
            if ((json_record[key] is True and db_record[column_name] == 'Y') or
                    (json_record[key] is False and db_record[column_name] == 'N')):
                return

            err_msg = f"for {id_key} {json_record[id_key]} the ws key {key} shows {json_record[key]} " \
                      f"while the db record for {column_name} shows {db_record[column_name]}"
            error_array.append(err_msg)

    def test_active_records(self, error_msgs, url):
        records = self.runWS(url, "active", "true")

        if len(records) == 0:
            error_msgs.append('No active records were found')
            return

        for record in records:
            if not record["active"]:
                if len(error_msgs) == 0:
                    error_msgs.append('inactive records were returned when active filtering was on')

                error_msgs.append(record)
                return
