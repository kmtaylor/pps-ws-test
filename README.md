# pps-ws-test
tests for pps webservices
WS URLs and contracts:
https://docs.google.com/spreadsheets/d/1U_qMA_3a22m3AWmHbK2I9AGw9vD3qVtvpOlyh7cuCc4/edit#gid=0 
<br>
You will need to create two config files - see config/config.tmpl<br>
Copy to config/pps-dev/config.yaml and config/pps-int/config.yaml and 
update for each environment

To switch between pps-dev and pps-int environments, you will need to change
the TEST_ENVIRONMENT variable in the open_yaml.py and open_logger.py - NOTE that
not all tests are using these variables yet!  Some still have complete urls
hardcoded.  This should be updated in the future.  And we should look for a 
better way to set the TEST_ENVIRONMENT variable


