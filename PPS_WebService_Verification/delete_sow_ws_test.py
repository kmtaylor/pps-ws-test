
# created 1/26/21

# the WS URLs are :
# https://ws-access-dev.jgi.doe.gov
# https://ws-access-int.jgi.doe.gov
# https://ws-access.jgi.doe.gov

# to see  sow status  in WIP : https://projects-genint.jgi.doe.gov/pmo/sequencing_projects/xxxx
#this test creates new sp and sample, runs it through aliquot creation, sample status is changed
# so that sow can be deleted,  sow is then deleted
#  verify  Database is updated for sow


import requests

from Utils.clarity_constants import Container
from Utils import data_gen_tools
import json
from Utils.util_dbTools import DataBaseTools
from Utils.util_dbTools_Postgres import DataBaseToolsPostgres
from Utils.util_udfTools import udfTools

from Utils.util_jsonInput import jsonInput
from Utils.Analyte import Analyte
from Utils.http_request_tools import HttpTools
from Utils.open_yaml import open_yaml


#name:  deleteSowWST  -  delete sow Web Service Test,
# per requirements doc: https://docs.google.com/document/d/1GPSJAJdMtZ8HwdMm14EAX0tkvASdFBgA-1AAeRJ3nX8/edit


class  deleteSowWST():
    def __init__(self):
        self.printOn = False
        self.myDB = DataBaseToolsPostgres(db='pps')
        self.myDB.connect()
        self.errorCnt = 0

        dict_config = open_yaml()
        self.wsURL = dict_config['ws_url'] + dict_config['delete_sow_url']
        self.clarityServer = dict_config['server_name']

        self.myUDF = udfTools()
        self.dataGen = data_gen_tools.DataGenTools()
        self.myJson = jsonInput()
        self.myRequests = HttpTools()


    # -------------------------------------------------------------------------------------------
    # deleteSows
    # given sow item list , this will delete   sows
    def deleteSows(self,sowItem):
        if self.printOn:
            print("using WS url: ", self.wsURL)
        self.myJson.updateJson(self.myJson.deleteSowJson, "sow-item-ids", sowItem)
        submissionJson = self.myJson.deleteSowJson
        #status,wsResp = self.deleteSowPost(self.wsURL, submissionJson, 200)  # call WS POST,response in dictionary form
        status, wsResp = self.myRequests.http_post_json_w_expectedStatus(self.wsURL, submissionJson, 200)
        if status == 200:
            return True
        return False   #if post did not work return false


    # ------------------------------------------------------------------------------------
    # getSowsViaDBquery
    #
    # get sows associated with SP by doing DB query
    # inputs: SP
    # outputs: sowlist

    def getSowsViaDBquery(self, spid):
        # verify database has been updated with sow item
        sowList = self.myDB.get_database_attribute_list('*', 'uss.dt_sow_item',
                                                        'SEQUENCING_PROJECT_ID', str(spid))
        # convert list from string to list of numbers
        for i in range(0, len(sowList)):
            sowList[i] = int(sowList[i])

        return sowList


    # ------------------------------------------------------------------------------------
    # verifyDB_entityStatus
    # verify that a entity (list) is in the state expected
    # input:
    #         entityType  (i.e.  "sow", "sample", "sp")
    #         entityList  - list of items that you will check status on in DB
    #         expectedStatus - the expected status that entity is in
    def verifyDB_entityStatus(self,entityType, entityList,expectedStatus):
        # verify database has been updated for sample
        for entity in entityList:
            status = self.myDB.get_status(entityType, entity)
            if status != expectedStatus:
                return False
        return True

    # ------------------------------------------------------------------------------------
    # updateSampleStatus
    # update sample to new status
    # inputs:  sample, status (new status to be updated to)

    def updateSampleStatus(self, sample,status):

        statement = "update uss.dt_sample set current_status_id =" + str(status) + " , status_comments = 'updated by " \
                    "delete_sow_ws_test.py' ,status_date = CURRENT_TIMESTAMP where sample_id in (" + str(sample) + ")"
        self.myDB.do_update(statement)

    #------------------------------------------------------------------------------------
    # createSample_and_deleteSowOfExistingSample
    #
    # create new SP and sample using data generator
    # then change  sample status to "Awaiting Collaborator Metadata" (2)
    # then delete sow
    # inputs: none
    # outputs: none
    def createSample_and_deleteSowOfExistingSample(self):
        # create new SP and sample to work with using datagenerator
        artifacts = self.dataGen.create_samples(1, Container.TUBE, 1)
        spid = artifacts[0].spids[0]
        sample_id = artifacts[0].sample_ids[0]
        sample_id_list = Analyte.get_all_sample_ids(artifacts)
        #get sow items (List) of newly created SP
        sowItems = self.getSowsViaDBquery(spid)

        print("New SP: ", spid, " ,New Sample(s): ", sample_id_list, "New Sows: ", sowItems )

        # samples(s)must have "Awaiting Collaborator Metadata" status(2) so you can delete sows
        self.updateSampleStatus(sample_id,2 )

        # delete  sow  by calling WS
        if self.deleteSows(sowItems):
            print("sows deleted: ", sowItems)

            # verify database has sow item (and sample) status has been updated to deleted
            if (self.verifyDB_entityStatus( "sow", sowItems, "Deleted")):
                print("Verified! Sow(s) have deleted status in DB")
                if (self.verifyDB_entityStatus( "sample", sample_id_list, "Deleted")) == True:
                    print("Verified! sample(s) have deleted status in DB")
                else:
                    print("**ERROR** sample(s) not deleted from DB")
            else:
                print("**ERROR** Sow(s) not deleted from DB")

        else:
            print("**ERROR** deleteSow WS not successful")

        # commenting this out for now, the deleteSPandChildren(spid) function does not exist in the
        # postgres db utils class, and I'm not a fan of leaving things in Clarity with weird statuses
        # delete SP and children
        # self.myDB.deleteSPandChildren(spid)

        return





#------Call to run test --------------------------
print ("End to end testing deleteSow WS ")
myTest = deleteSowWST()

myTest.createSample_and_deleteSowOfExistingSample()



#---- tests to do --------------------











