"""
test library-sequencing-status changes: pps-library-sequencing-status/check-complete

per the contract, which is here:
https://docs.jgi.doe.gov/display/PPS/Library+Sequencing+Status+Service+Contract

"This service accepts a list of library-names and returns the subset of the same that
have completed sequencing in clarity.  A library is considered to have completed sequencing
if neither the library nor its descendant analytes are queued/in-progress in any of the
clarity workflows."

"""
from assertpy import assert_that
import pytest
import json


class TestLibSeqStatusWS:
    ws_endpoint = "pps-library-sequencing-status/check-complete"
    success_status = 200
    error_status = 422

    library_seq_status_json = {
        "library-names": ["GXSAU"],
        "submitted-by-cid": 13
    }

    @pytest.fixture(scope="class")
    def ws_url(self, ws_base_url):
        return ws_base_url + self.ws_endpoint

    def test_lib_status_correct_for_one_completed_library(
            self, ws_url, illumina_pool_pru_artifact, user_id, http_tools):
        """
        test new library-sequencing-status
        process:
            create a new library that has gone all the way through sequencing
            execute web service
            verify
        """

        library_name = illumina_pool_pru_artifact[0].library_names[0]

        # submit json for webservice
        submission_json = self.library_seq_status_json
        submission_json['library-names'] = [library_name]
        submission_json['submitted-by-cid'] = user_id

        submission_json = json.dumps(submission_json)

        status_code, response_json = http_tools.http_post_json_w_status(ws_url, submission_json)
        returned_libs = response_json['sequencing-completed-library-names']

        # error message to show if returned status code is not equal to the code we expect for success
        error_msg = f"POST to {ws_url} with json\n {submission_json} did not return expected success status"
        assert_that(status_code, description=error_msg).is_equal_to(self.success_status)

        error_msg = f"library {library_name} is not shown in Library Sequencing Complete web service response"
        assert_that(returned_libs, description=error_msg).contains(library_name)

    def test_lib_status_correct_for_eight_completed_libraries(
            self, ws_url, illumina_pool_pru_artifact, user_id, http_tools):
        """
        test new library-sequencing-status
        process:
            create 8 libraries in a pool that has gone all the way through sequencing
            execute web service
            verify
        """

        # generate libraries
        library_names = illumina_pool_pru_artifact[0].library_names

        # submit json for webservice
        submission_json = self.library_seq_status_json
        submission_json['library-names'] = library_names
        submission_json['submitted-by-cid'] = user_id

        submission_json = json.dumps(submission_json)

        status_code, response_json = http_tools.http_post_json_w_status(ws_url, submission_json)
        returned_libs = response_json['sequencing-completed-library-names']

        # error message to show if returned status code is not equal to the code we expect for success
        error_msg = f"POST to {ws_url} with json\n {submission_json} did not return expected success status"
        assert_that(status_code, description=error_msg).is_equal_to(self.success_status)

        error_msg = f"all library names are not shown in Library Sequencing Complete web service response"
        assert_that(returned_libs, description=error_msg).is_equal_to(library_names)

    def test_lib_status_error_response_for_invalid_library_names(self, ws_url, user_id, http_tools):
        """
        test library-sequencing-status w. invalid libraries
        process:
            submit request w. invalid library names
        """

        # bad library names
        bad_lib_1 = 'xxxxx'
        bad_lib_2 = 'yyyyy'

        # update json with invalid data
        submission = self.library_seq_status_json
        submission['library-names'] = [bad_lib_1, bad_lib_2]
        submission['submitted-by-cid'] = user_id

        submission_json = json.dumps(submission)

        # submit request
        status_code, response_json = http_tools.http_post_json_w_status(ws_url, submission_json)

        # error message to show if returned status code is not the error code we are expecting
        error_msg = f"POST to {ws_url} with json\n {submission_json} did not return expected error status"
        assert_that(status_code, description=error_msg).is_equal_to(self.error_status)

        error_msg = f"POST to {ws_url} with json\n {submission_json} did not return expected error message"
        assert_that(response_json, description=error_msg).contains('errors')
        assert_that(response_json['errors'][0], description=error_msg).contains('message')
        assert_that(response_json['errors'][0]['message'], description=error_msg).contains(bad_lib_1)
        assert_that(response_json['errors'][0]['message'], description=error_msg).contains(bad_lib_2)
