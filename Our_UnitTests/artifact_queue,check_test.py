
# created 4/7/21



# verifying that the UDF functions that return the workflow that an entity is "QUEUED" in
# works correctly.  This demonstrates that given an entity, the function returns a list (of lists)
# of workflow names that the entity is currently queued in
#


import requests
from Utils.util_udfTools import udfTools
from Utils.open_yaml import open_yaml

class  artifactQueueTest():
    def __init__(self):
        self.myUDF = udfTools()



#------Call to run test --------------------------
print ("testing - get queue of artifact ")
myTest = artifactQueueTest()
print ("queues of 270662")
print(myTest.myUDF.getQueuesOfEntity("270662"))
print ("queues of 174446")
print(myTest.myUDF.getQueuesOfEntity("174446"))
print ("queues of 259861")
print(myTest.myUDF.getQueuesOfEntity("259861"))
print ("queues of 266637")
print(myTest.myUDF.getQueuesOfEntity("266637"))
print ("queues of 259020")
print(myTest.myUDF.getQueuesOfEntity("259020"))
print ("queues of HGSBY")
print(myTest.myUDF.getQueuesOfEntity("HGSBY"))
print ("queues of HGPUP")
print(myTest.myUDF.getQueuesOfEntity("HGPUP"))
print ("queues of 231132")
print(myTest.myUDF.getQueuesOfEntity("231132"))
print ("queues of HHNZB")
print(myTest.myUDF.getQueuesOfEntity("HHNZB"))
print ("queues of HGTHO")
print(myTest.myUDF.getQueuesOfEntity("HGTHO"))
print ("queues of lb HGTHO")
print(myTest.myUDF.getQueuesOfEntity("lb HGTHO"))
print ("queues of aq 252508")
print(myTest.myUDF.getQueuesOfEntity("aq 252508"))


















